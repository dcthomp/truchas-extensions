//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/truchas/Metadata.h"

#include "smtk/resource/Properties.h"
#include "smtk/resource/Resource.h"

#include <iostream>

namespace
{
typedef smtk::resource::String SMTKStringType;
typedef smtk::resource::Integer SMTKIntType;

const SMTKStringType METADATA_FORMAT_KEY = "truchas.format";
}

namespace smtk
{
namespace simulation
{
namespace truchas
{

// Static constants
const std::string Metadata::PROJECT_TYPENAME = "truchas";
const std::string Metadata::PROJECT_FILE_EXTENSION = ".project.smtk";
const std::string Metadata::PROPERTY_PREFIX = Metadata::PROJECT_TYPENAME + ".";
const std::string Metadata::HEAT_TRANSFER_MESH_ROLE = "heat-transfer-mesh";
const std::string Metadata::INDUCTION_HEATING_MESH_ROLE = "induction-heating-mesh";

#ifndef WORKFLOWS_SOURCE_DIR
#error "WORKFLOWS_SOURCE_DIR not defined."
#endif
std::string Metadata::WORKFLOWS_DIRECTORY = WORKFLOWS_SOURCE_DIR;

bool Metadata::getFromResource(smtk::resource::ConstResourcePtr resource)
{
  bool isTruchas = resource->properties().contains<SMTKIntType>(METADATA_FORMAT_KEY);
  if (!isTruchas)
  {
    std::cerr << __FILE__ << ":" << __LINE__ << " Resource missing truchas-formatted metadata."
              << std::endl;
    return false;
  }
  this->Format = resource->properties().at<SMTKIntType>(METADATA_FORMAT_KEY);
  if ((this->Format < 1) || (this->Format > METADATA_FORMAT_NUMBER))
  {
    std::cerr << __FILE__ << ":" << __LINE__ << " Unsupported truchas.format " << this->Format
              << std::endl;
    return false;
  }

  const std::string htKey = Metadata::PROPERTY_PREFIX + Metadata::HEAT_TRANSFER_MESH_ROLE;
  this->NativeHeatTransferMeshLocation = resource->properties().at<SMTKStringType>(htKey);
  const std::string ihKey = Metadata::PROPERTY_PREFIX + Metadata::INDUCTION_HEATING_MESH_ROLE;
  this->NativeInductionHeatingMeshLocation = resource->properties().at<SMTKStringType>(ihKey);

  return true;
}

void Metadata::putToResource(smtk::resource::ResourcePtr resource)
{
  // Store instance data as POD strings.
  // ALWAYS call erase() before insert(), a lesson learned the hard way...
  resource->properties().erase<SMTKIntType>(METADATA_FORMAT_KEY);
  resource->properties().insert<SMTKIntType>(METADATA_FORMAT_KEY, this->Format);

  const std::string htKey = Metadata::PROPERTY_PREFIX + Metadata::HEAT_TRANSFER_MESH_ROLE;
  resource->properties().erase<SMTKStringType>(htKey);
  bool htRet =
    resource->properties().insert<SMTKStringType>(htKey, this->NativeHeatTransferMeshLocation);
  assert(htRet);

  const std::string ihKey = Metadata::PROPERTY_PREFIX + Metadata::INDUCTION_HEATING_MESH_ROLE;
  resource->properties().erase<SMTKStringType>(ihKey);
  bool ihRet =
    resource->properties().insert<SMTKStringType>(ihKey, this->NativeInductionHeatingMeshLocation);
  assert(ihRet);
}

void to_json(nlohmann::json& j, const Metadata& mdata)
{
  j = nlohmann::json{ "native-files",
    { { "heat-transfer", mdata.NativeHeatTransferMeshLocation },
      { "induction-heating", mdata.NativeInductionHeatingMeshLocation } } };
}

void from_json(const nlohmann::json& j, Metadata& mdata)
{
  j.at("native-files").at("heat-transfer").get_to(mdata.NativeHeatTransferMeshLocation);
  j.at("native-files").at("induction-heating").get_to(mdata.NativeInductionHeatingMeshLocation);
}
}
}
}
