//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtkTruchasMaterialPriorityView.h"

// Plugin includes
#include "smtk/simulation/truchas/qt/qtMaterialPriorityModel.h"
#include "ui_smtkTruchasMaterialPriorityView.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Categories.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/utility/Queries.h"
#include "smtk/view/Configuration.h"
#include "smtk/view/Information.h"

// Qt includes
#include <QCoreApplication>
#include <QDebug>
#include <QIcon>
#include <QLabel>
#include <QString>
#include <QVBoxLayout>

// Std includes
#include <algorithm>
#include <cassert>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#define ALERT_ICON_PATH ":/icons/attribute/errorAlert.png"

class smtkTruchasMaterialPriorityViewInternals : public Ui::smtkTruchasMaterialPriorityView
{
public:
  // Internal member data
  QWidget* m_widget;
  qtMaterialPriorityModel* m_priorityModel;
  bool m_notConfirmed; // user has not confirmed

  smtk::attribute::WeakAttributePtr m_attribute;
  smtk::attribute::WeakGroupItemPtr m_trackInterfacesGroup;
  smtk::attribute::WeakGroupItemPtr m_materialPriorityGroup;

  void initialize(smtk::attribute::AttributePtr att, QWidget* widget)
  {
    this->setupUi(widget);
    m_widget = widget;
    m_notConfirmed = false;

    m_attribute = att;

    // Find the track_interfaces and material_priority GroupItem instances
    smtk::attribute::GroupItemPtr trackIntGroup = att->findGroup("track_interfaces");
    m_trackInterfacesGroup = trackIntGroup;
    smtk::attribute::GroupItemPtr priorityGroup =
      trackIntGroup->findAs<smtk::attribute::GroupItem>("material_priority");
    m_materialPriorityGroup = priorityGroup;

    m_trackCheckbox->setChecked(trackIntGroup->isEnabled());
    m_assignWidget->setVisible(trackIntGroup->isEnabled());

    // Set confirm button from item state
    bool confirmed = trackIntGroup->find("confirmed")->isEnabled();
    m_notConfirmed = !confirmed;
    m_confirmButton->setVisible(m_notConfirmed);

    // Initialize alert icon
    m_iconLabel->setText("");
    auto icon = QIcon(ALERT_ICON_PATH);
    m_iconLabel->setPixmap(icon.pixmap(16));
    m_iconLabel->setMinimumWidth(24);
    m_iconLabel->setVisible(m_notConfirmed);

    // Initialize priority model
    m_priorityModel = new qtMaterialPriorityModel(widget, priorityGroup);
    m_priorityView->setModel(m_priorityModel);
    m_priorityView->setColumnHidden(0, true);
    m_priorityView->setColumnHidden(1, true);
  }

  // Finds or creates material priority attribute
  smtk::attribute::AttributePtr getPriorityAttribute(smtk::view::ConfigurationPtr view,
    smtk::attribute::ResourcePtr attResource, bool& isCreated, std::string& msg)
  {
    isCreated = false;
    // Use sstream to generate warning message if not successful
    std::stringstream ss;

    // This view uses the same syntax as instanced views, though we only support a single attribute
    int index = view->details().findChild("InstancedAttributes");
    if (index < 0)
    {
      ss << "WARNING: View " << view->name() << " has no InstancedAttributes element.";
      msg = ss.str();
      return nullptr;
    }
    smtk::view::Configuration::Component& comp = view->details().child(index);
    std::size_t i, n = comp.numberOfChildren();
    if (n == 0)
    {
      ss << "WARNING: View " << view->name() << " InstancedAttributes element is empty.";
      msg = ss.str();
      return nullptr;
    }
    else if (n > 1)
    {
      ss << "WARNING: View " << view->name() << " InstancedAttributes has more than one child.";
      msg = ss.str();
      return nullptr;
    }

    smtk::view::Configuration::Component attComp = comp.child(0);
    if (attComp.name() != "Att")
    {
      ss << "WARNING: View " << view->name() << " InstancedAttributes element missing Att child.";
      msg = ss.str();
      return nullptr;
    }

    std::string attName = "material-priority"; // default attribute name
    attComp.attribute("Name", attName);

    // See if the attribute exists and if not then create it
    smtk::attribute::AttributePtr att = attResource->findAttribute(attName);
    if (!att)
    {
      std::string defName;
      if (!attComp.attribute("Type", defName))
      {
        ss << "WARNING: View " << view->name() << " Att element missing \"Type\" attribute.";
        msg = ss.str();
        return nullptr;
      }
      smtk::attribute::DefinitionPtr attDef = attResource->findDefinition(defName);
      if (!attDef)
      {
        ss << "WARNING: View " << view->name() << " using definition " << defName
           << " not found in attribute resuource.";
        msg = ss.str();
        return nullptr;
      }

      // (else)
      att = attResource->createAttribute(attName, attDef);
      isCreated = true;
    }

    return att;
  } // getPriorityAttribute()

  // Generates set of all material attributes that require priority:
  //   - all material.real attributes with fluid phase and used in body or inflow
  //   - material.void if used in any body or inflow
  //   - material.solid if any of the materials in the set includes a solid phase
  void getMaterialAttributes(std::set<smtk::attribute::AttributePtr>& materialAtts) const
  {
    materialAtts.clear();

    auto att = m_attribute.lock();
    if (att == nullptr)
    {
      return;
    }
    auto attResource = att->attributeResource();
    bool insertSolid = false; // track whether to include material.solid

    // Start with all material.real attributes assigned to a body
    auto bodyAtts = attResource->findAttributes("body");
    for (auto bodyAtt : bodyAtts)
    {
      bool hasSolid = false;
      auto matAtt = this->_getReferencedMaterialIfFluid(bodyAtt, hasSolid);
      insertSolid = insertSolid || hasSolid;
      if (matAtt != nullptr)
      {
        materialAtts.insert(matAtt);
      }
    }

    // Add material.real attributes assigned to material_inflow BC
    auto bcAtts = attResource->findAttributes("ff.boundary.inflow");
    for (auto bcAtt : bcAtts)
    {
      bool hasSolid = false;
      auto matAtt = this->_getReferencedMaterialIfFluid(bcAtt, hasSolid);
      insertSolid = insertSolid || hasSolid;
      if (matAtt != nullptr)
      {
        materialAtts.insert(matAtt);
      }
    }

    if (insertSolid)
    {
      auto solidAtts = attResource->findAttributes("material.solid");
      materialAtts.insert(solidAtts[0]);
    }
  } // getMaterialAttributes()

  // Generate label, either material or material:phase
  QString materialLabel(smtk::attribute::AttributePtr matAtt) const
  {
    QString matLabel = QString::fromStdString(matAtt->name());

    // For solid, use attribute label
    if (matAtt->type() == "material.solid")
    {
      return QString("SOLID");
    }

    if (matAtt->type() == "material.void")
    {
      return QString("VOID");
    }

    // For single-phase, use attribute label
    auto phasesGroup = matAtt->findGroup("phases");
    assert(phasesGroup != nullptr);
    if (phasesGroup->numberOfGroups() == 1)
    {
      return matLabel;
    }

    // For multiple phases, find the fluid phase
    for (std::size_t i = 0; i < phasesGroup->numberOfGroups(); ++i)
    {
      auto fluidItem = phasesGroup->find(i, "fluid");
      if (fluidItem->isEnabled())
      {
        QString fluidLabel = QString::fromStdString(fluidItem->name());
        return QString("%1:%2").arg(matLabel, fluidLabel);
      }
    }

    qWarning() << "No fluid phase for attribute" << matAtt->name().c_str();
    return matLabel;
  } // materialLabel()

  void setPriorityConfirmed(bool state)
  {
    auto trackIntGroup = m_trackInterfacesGroup.lock();
    if (!trackIntGroup)
    {
      return;
    }
    auto confirmedItem = trackIntGroup->find("confirmed");
    confirmedItem->setIsEnabled(state);

    m_notConfirmed = !state;
    m_iconLabel->setVisible(m_notConfirmed);
    m_confirmButton->setVisible(m_notConfirmed);
  }

  // Gets material attribute referenced by either body or material inflow attribute.
  // Can use the same logic because the 2 attribute types are structured the same
  smtk::attribute::AttributePtr _getReferencedMaterialIfFluid(
    smtk::attribute::AttributePtr att, bool& hasSolid) const
  {
    int version = att->definition()->version();
    smtk::attribute::ComponentItemPtr refItem = nullptr;
    if ((version == 0) || (version == 2))
    {
      refItem = att->findComponent("material");
    }
    else if (version == 1)
    {
      auto typeItem = att->findString("material-type");
      if (!typeItem->isSet())
      {
        return smtk::attribute::AttributePtr();
      }
      else if (typeItem->value() == "void")
      {
        auto attResource = att->attributeResource();
        auto voidAtt = attResource->findAttributes("material.void")[0];
        hasSolid = false;
        return voidAtt;
      }
      // (else)
      auto item = typeItem->findChild("material", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
      refItem = std::dynamic_pointer_cast<smtk::attribute::ComponentItem>(item);
    }

#ifndef NDEBUG
    if (refItem == nullptr)
    {
      qWarning() << "refItem is null," << __FILE__ << "line" << __LINE__;
      hasSolid = false;
      return smtk::attribute::AttributePtr();
    }
#endif

    return this->_getMaterialIfFluid(refItem, hasSolid);
  }

  // Gets target of reference item and returns if it is a material attribute with fluid phase.
  // Set hasSolid flag (only) if material also has solid phase.
  smtk::attribute::AttributePtr _getMaterialIfFluid(
    smtk::attribute::ConstComponentItemPtr refItem, bool& hasSolid) const
  {
    if (!refItem->isSet())
    {
      return nullptr;
    }

    auto matAtt = std::dynamic_pointer_cast<smtk::attribute::Attribute>(refItem->value());
    if (matAtt == nullptr)
    {
      return nullptr;
    }

    auto matType = matAtt->type();
    if (matType == "material.solid")
    {
      hasSolid = true;
      return nullptr;
    }

    if (matType == "material.void")
    {
      return matAtt;
    }

#ifndef NDEBUG
    if (matType != "material.real")
    {
      qWarning() << "unexpected attribute type" << matType.c_str() << "(expected material.real)";
      return nullptr;
    }
#endif

    // Check phases
    bool hasFluid = false;
    auto phasesItem = matAtt->findGroup("phases");
    for (std::size_t i = 0; i < phasesItem->numberOfGroups(); ++i)
    {
      auto fluidItem = phasesItem->find(i, "fluid");
      bool isFluid = fluidItem->isEnabled();
      hasFluid |= isFluid;
      hasSolid |= !isFluid;
    }

    if (hasFluid)
    {
      return matAtt;
    }

    // (else)
    return nullptr;
  } // _getMaterialIfFluid()
};

qtBaseView* smtkTruchasMaterialPriorityView::createViewWidget(const smtk::view::Information& info)
{
  smtkTruchasMaterialPriorityView* view = new smtkTruchasMaterialPriorityView(info);
  view->buildUI();
  return view;
}

smtkTruchasMaterialPriorityView::smtkTruchasMaterialPriorityView(
  const smtk::view::Information& info)
  : qtBaseAttributeView(info)
{
  m_internals = new smtkTruchasMaterialPriorityViewInternals();
}

smtkTruchasMaterialPriorityView::~smtkTruchasMaterialPriorityView()
{
  delete m_internals;
}

bool smtkTruchasMaterialPriorityView::isEmpty() const
{
  // Return true if "Fluid Flow" is not part of
  // the current active categories
  auto attRes = this->attributeResource();
  if (!(attRes && attRes->activeCategoriesEnabled()))
  {
    return false; // there is no category filtering
  }
  return (attRes->activeCategories().find("Fluid Flow") == attRes->activeCategories().end());
}

bool smtkTruchasMaterialPriorityView::isValid() const
{
  if (m_internals->m_notConfirmed)
  {
    return false;
  }

  // Get current set of applicable material atts and validate against model
  std::set<smtk::attribute::AttributePtr> materialAtts;
  m_internals->getMaterialAttributes(materialAtts);
  bool valid = m_internals->m_priorityModel->isValid(materialAtts);

  // Set not-confirmed flag to avoid redundancy.
  if (!valid)
  {
    m_internals->m_notConfirmed = true;
  }
  return valid;
}

void smtkTruchasMaterialPriorityView::createWidget()
{
  smtk::view::ConfigurationPtr view = this->configuration();
  if (!view)
  {
    return;
  }

  this->Widget = new QFrame(this->parentWidget());
  const smtk::attribute::ResourcePtr attResource = this->attributeResource();
  std::string msg;
  bool isCreated = false;
  auto att = m_internals->getPriorityAttribute(view, attResource, isCreated, msg);
  if (att == nullptr)
  {
    // Display label indicating that the definition is missing
    auto label = new QLabel(this->Widget);
    label->setText(msg.c_str());
    auto layout = new QVBoxLayout();
    layout->addWidget(label);
    layout->setAlignment(label, Qt::AlignHCenter);
    this->Widget->setLayout(layout);
  }
  else
  {
    if (isCreated)
    {
      this->attributeCreated(att);
    }
    m_internals->initialize(att, this->Widget);
    this->setupConnections();
  }
}

void smtkTruchasMaterialPriorityView::onPriorityConfirmed()
{
  m_internals->setPriorityConfirmed(true);
  emit qtBaseView::modified();
}

void smtkTruchasMaterialPriorityView::onPriorityModified(
  smtk::attribute::ItemPtr priorityGroup, bool needsConfirm)
{
  if (needsConfirm)
  {
    m_internals->setPriorityConfirmed(false);
  }
  emit qtBaseAttributeView::modified(priorityGroup);
}

void smtkTruchasMaterialPriorityView::onTrackingStateChanged(int state)
{
  auto att = m_internals->m_attribute.lock();
  if (!att)
  {
    return;
  }
  bool checked = static_cast<bool>(state);
  m_internals->m_assignWidget->setVisible(checked);

  std::string itemName = m_internals->m_trackInterfacesGroup.lock()->name();
  std::vector<std::string> nameList = { itemName };
  emit this->attributeChanged(att, nameList);
}

void smtkTruchasMaterialPriorityView::setupConnections()
{
  // Make sure all events have been handled before connecting signals & slots
  QCoreApplication::processEvents();

  QObject::connect(m_internals->m_confirmButton, &QPushButton::clicked, this,
    &smtkTruchasMaterialPriorityView::onPriorityConfirmed);
  QObject::connect(m_internals->m_trackCheckbox, &QCheckBox::stateChanged, this,
    &smtkTruchasMaterialPriorityView::onTrackingStateChanged);
  QObject::connect(m_internals->m_priorityModel, &qtMaterialPriorityModel::modified, this,
    &smtkTruchasMaterialPriorityView::onPriorityModified);
  // &qtBaseAttributeView::modified);
}

void smtkTruchasMaterialPriorityView::updateUI()
{
  auto att = m_internals->m_attribute.lock();
  if (att == nullptr)
  {
    return;
  }

  // Get all attributes to be included in material priority
  std::set<smtk::attribute::AttributePtr> materialAtts;
  m_internals->getMaterialAttributes(materialAtts);

  // Update priority model and get the set of unassigned atts
  m_internals->m_priorityModel->update(materialAtts);
}

#undef ALERT_ICON_PATH
