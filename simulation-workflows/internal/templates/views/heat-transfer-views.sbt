<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Views>
    <View Type="Group" Title="Heat Transfer" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="HT Surface Conditions" />
        <View Title="HT Solver" />
        <View Title="HT Sources" />
      </Views>
    </View>

    <View Type="Group" Title="HT Surface Conditions" Label="Boundary Conditions" TabPosition="North" TabIcon="false">
      <Views>
        <View Title="HT Boundary Conditions"/>
        <View Title="HT Interface Conditions"/>
        <View Title="HT Functions" />
      </Views>
    </View>

    <View Type="Attribute" Title="HT Boundary Conditions" Label="Boundary" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="ht.boundary"/>
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="HT Interface Conditions" Label="Interface" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="ht.interface"/>
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="HT Functions" Label="Functions" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="fn.ht" />
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="HT Solver" Label="Solver">
      <InstancedAttributes>
        <Att Name="Standard Solver" Type="ht.solver.standard" />
        <Att Name="Second Solver" Type="ht.solver.second" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="HT Sources" Label="Sources" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="ht.source" />
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeSystem>
