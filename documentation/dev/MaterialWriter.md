# Truchas MaterialWriter Class
The `MaterialWriter` class is a mixin to the `TruchasWriter` instance and is used to write the MATERIAL, MATERIAL_SYSTEM, and PHASE namelists to the output stream.

## Material Attribute Definition
Each material created by the user is represented as a CMB attribute of type `material.real` in the attribute resource specifying the simulation. The definition for the `material.real` attribute is located in the file at `simulation-workflows/internal/templates/material.sbt`. In brief, the material attribute definition ("AttDef") contains three top-level CMB "group items":

* `shared-properties` is a group item specifying properties that are common across all phases of a material. Each shared-property item (density, specific heat, conductivity, etc.) can be optionally enabled by the user. (By default all shared-property items are disabled.)
* `phases` is an "extensible" group item, which stores an ordered list of elements, with each element storing the properties for one phase (density, specific heat, conductivity, etc.). In the modelbuilder user interface, any properties enabled in the `shared-property` group are disabled for the `phases` elements.
* `transitions` is an "extensible" group item, with each element storing the transition data between two material phases (latent heat, transition temperatures). When phases are added to or removed from a given material, the modelbuilder user interface also updates the transition elements, so that the number of transition elements is always one less than the number of phase elements.

## Entry point: MaterialWriter._write_physical_materials()
The entry point for writing materials is the `MaterialWriter._write_physical_materials()` method, which is called by the `TruchasWriter` code. The `_write_physical_materials()` method starts by retrieving the list of `material.real` attributes from the attribute resource. For each attribute, the method:

1. Calls an internal method to check the attribute validity (whether or not its contents are consistent with their corresponding definitions in the template file).
2. Writes a MATERIAL_SYSTEM namelist by calling the `_write_material_system_namelist()` method.
3. Calls the `_get_shared_properties()` method to build a dictionary of `<keyword, CMB-item>` for shared properties that are enabled. This dictionary is used in the next step.
4. Traverses each element in the `phases` group item and for each element: (i) writes the PHASE namelist by calling the `_write_phase_namelist` method, then (ii) writes the MATERIAL namelist by calling the `_write_material_namelist` method.

## A note about phase names
There is some additional logic in the `_write_physical_materials()` method for writing the name of the first phase of each material. Each phase stores a "name" item, but for single-phase materials, the phase name is set to the same name as the material attribute itself.


## MATERIAL_SYSTEM namelist
The MATERIAL_SYSTEM namelist for each material attribute is written by the `MaterialWriter._write_material_system_namelist()` method.

1. The method begins by calling `BaseWriter._start_namelist()`, which writes the namelist title (in this case, "`&MATERIAL_SYSTEM`").
2. The method then gets the attribute name and writes that to the namelist using the CardFormat.write_value() method. (The code also truncates the name to 32 characters.) The `CardFormat` class provides low-level methods for formatting and writing data.
3. If the material is single-phase, the method writes the `phases` keyword using the attribute name, then finishes the namelist by calling the `BaseWriter._finish_namelist()` method and returns.
4. If there is more than one phase, the method proceeds to build four python lists with variable names `phase_list`, `lo_temps`, `hi_temps`, and `latent_heats`, respectively. The contents of these lists will be written to the MATERIAL_SYSTEM namelist.
5. The `phase_list` is generated by traversing each element in the `phases` group item, finding the `name` item, and adding its value to the list.
6. Similarly, the other three lists are generated by traversing each element in the `transitions` group item, finding the appropriate item, and getting its value. (The names of the items making up the phase and transition group items are defined in the `material.sbt` template file.)
7. A `smooting_radius` value is optionally set by checking for this value in each element in the transition group. Because only one `smoothing_radius` keyword is used in the `MATERIAL_SYSTEM` namelist, the first value found is used.
8. After the four lists are completed, the method calls the `CardFormat.write_value()` static method for each list to write the contents into the namelist.


## PHASE namelist
The PHASE namelists are written by the `MaterialWriter._write_phase_namelist()` method. This method is called separately for each phase in the `phases` group item contained by each `material.real` attribute. In other words, the `_write_phase_namelist()` method writes one PHASE namelist each time it is called.

The basic logic in this method traverses a list of CardFormat instances called the PHASE_FORMAT_LIST. Each CardFormat instance stores the namelist variable for one material property and the "item_path", which specifies where to find the property item in the phases group item. All of the CardFormat instances in the PHASE_FORMAT_LIST also pass the keyword argument `as_property` to `True`, indicating that the value should be written in the Truchas property format. Some of the CardFormat instances also set the keyword argument `is_custom` to `True` to indicate that special logic is used to write that namelist variable.

The `_write_phase_namelist()` method starts the PHASE namelist and writes the name variable that is passed in. The logic then traverses each CardFormat instance in the PHASE_FORMAT_LIST, and for each one, finds the corresponding `card_item` variable, which is an smtk.attribute.Item instance that stores the data to write. To do this:

1. The logic first checks if the shared property of that name has been enabled by the user. This is done by checking the `shared_props` dictionary, which is passed into the method. If the property is found in the `shared_props` dictionary, it is used.
2. If the property is not in the `shared_props` dictionary, the logic then checks the card's `is_custom` flag. Special logic is used for the `viscosity` property, because it is only written if its parent `fluid` item is enabled. Special logic is also used for the electrical properties, which are only written when induction heating is included in the analysis; this is the case when a class variable `INDUCTION_HEATING` is included `CardFormat.Conditions`, which is a Python set that is configured upstream in the export logic. (Note: the `CardFormat.Conditions` logic will likely be obviated in the future.)
3. If neither step 1 or 2 are used, then the `card_item` is retrieved from the `phases_group` item passed into the function.

Once the `card_item` is found, the `CardFormat.write_item()` method is used to write the property to the namelist.


## MATERIAL namelist
The MATERIAL namelists are written by the `MaterialWriter._write_material_namelist()` method. This method is called separately for each phase in the `phases` group item contained by each `material.real` attribute. In other words, the `_write_material_namelist()` method writes one MATERIAL namelist each time it is called. The logic for writing the namelist are as follows:

* `material_name` : The method writes the name passed into the method.
* `material_number` : The material_number (integer) is assigned to each phase of each material as traversed by the method. The material numbers are assigned in sequence and the assignments stored in the `material_number_dict` for later use when writing BODY namelists.
* `density` : The method writes the value `1.0` for each phase.
* `immobile` : The method finds the `fluid` item and checks whether or not it is enabled to determine which value to write.
* background material: The method first checks if the `background_material_id` value has been set by previous logic. If not, the current material attribute is assigned as the background. If the current material is the background, the method writes the namelist item `material_feature = background`.


## Appendix: Member Data
The `MaterialWriter` does not have any explicit member data, but as a mixin, it can access member data of its owning `TruchasWriter` class. Member data that are used this way are:

* `self.background_material_id` : internal id of the material attribute assigned as the background material. Note that if no background material is assigned by the user, the first material written will be set as the background material.
* `self.material_number_dict` : a dictionary that stores the mapping of each material phase item to the material number written to the Truchas input file. The dictionary contents are created when writing the MATERIAL namelist; the results are used when writing BODY namelists.
* `self.out` : the file object to which output are written.
* `self.sim_atts` : the *attribute resource*  that stores the simulation specifications, including material properties, boundary conditions, convergence criteria, etc.
* `self.skip_void_material` : a boolean indicating whether or not to write a void material to the Truchas input file.


## Appendix: Member Functions
Other member functions in `MaterialWriter` are:

* `_get_shared_properties()` : generates a dictionary mapping those Truchas property names (e.g., density, specific_heat) that are set to be shared among all material phases.
* `_write_void_material()` : writes a MATERIAL namelist for the `material.void` attribute.


Methods implemented in `BaseWriter` include:

* `self._check_attributes()` : an internal method that validates whether or not the contents of a given attribute are consistent with its definition in the template.
* `self._finish_namelist()` : writes the forward-slash character designating the end of a namelist.
* `self._material_key()` : generates a string unique for a given phase of a material attribute. The string is used for storing the material number in the  `material_number_dict`.
* `self._start_namelist()` : writes the namelist title with the ampersand prefix.
