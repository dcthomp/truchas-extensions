//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_plugin_pqTruchasDisplayToolBar_h
#define smtk_simulation_truchas_plugin_pqTruchasDisplayToolBar_h

// Provides a paraview toolbar with icons for "custom display options".
// The first implementation provides 2 icons for displaying only surface
// entities or only volume entities.

#include "smtk/PublicPointerDefs.h"
#include "smtk/model/EntityTypeBits.h"
#include "smtk/resource/Component.h"

#include <QToolBar>

class pqRepresentation;
class pqSMTKResourceRepresentation;

class pqTruchasDisplayToolBar : public QToolBar
{
  Q_OBJECT
  using Superclass = QToolBar;

public:
  pqTruchasDisplayToolBar(QWidget* parent = nullptr);
  ~pqTruchasDisplayToolBar() override;

protected slots:
  void showSurfaces();
  void showVolumes();
  void toggleEMMeshVisibility();

protected:
  smtk::resource::ResourcePtr getResource(pqRepresentation* rep) const;
  void showEntities(smtk::model::BitFlags flags) const;

  // Visitor function to set component visibility
  void displayVisitor(const smtk::resource::ComponentPtr& comp, pqSMTKResourceRepresentation* rep,
    smtk::model::BitFlags flags) const;

private:
  Q_DISABLE_COPY(pqTruchasDisplayToolBar);
};

#endif
