//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqTruchasProjectMenu.h"

#include "pqTruchasProjectCloseBehavior.h"
#include "pqTruchasProjectLoader.h"
#include "pqTruchasProjectOpenBehavior.h"
#include "pqTruchasProjectSaveBehavior.h"
#include "pqTruchasRecentProjectsMenu.h"
#include "pqTruchasProjectExportBehavior.h"
#include "pqTruchasProjectImportModelBehavior.h"
#include "pqTruchasProjectNewBehavior.h"
#include "smtk/simulation/truchas/utility/ProjectUtils.h"

#include "pqApplicationCore.h"

#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"
#include "smtk/project/ResourceContainer.h"

#include <QAction>
#include <QDebug>
#include <QMenu>
#include <QString>
#include <QtGlobal> // qWarning()

//-----------------------------------------------------------------------------
pqTruchasProjectMenu::pqTruchasProjectMenu(QObject* parent)
  : Superclass(parent)
{
  this->startup();
}

//-----------------------------------------------------------------------------
pqTruchasProjectMenu::~pqTruchasProjectMenu()
{
  this->shutdown();
}

//-----------------------------------------------------------------------------
bool pqTruchasProjectMenu::startup()
{
  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "cannot initialize Project menu because pqCore is not found";
    return false;
  }

  // Access/create the singleton instances of our behaviors.
  auto newProjectBehavior = pqTruchasProjectNewBehavior::instance(this);
  auto openProjectBehavior = pqTruchasProjectOpenBehavior::instance(this);
  auto saveProjectBehavior = pqTruchasProjectSaveBehavior::instance(this);
  // auto saveAsProjectBehavior = pqTruchasProjectSaveAsBehavior::instance(this);
  auto closeProjectBehavior = pqTruchasProjectCloseBehavior::instance(this);
  auto exportProjectBehavior = pqTruchasProjectExportBehavior::instance(this);
  auto importModelBehavior = pqTruchasProjectImportModelBehavior::instance(this);

  // Initialize "New Project" action
  m_newProjectAction = new QAction(tr("New Project..."), this);
  auto newProjectReaction = new pqTruchasProjectNewReaction(m_newProjectAction);
  QObject::connect(newProjectBehavior, &pqTruchasProjectNewBehavior::projectCreated, this,
    &pqTruchasProjectMenu::onProjectOpened);

  // Initialize "Open Project" action
  m_openProjectAction = new QAction(tr("Open Project..."), this);
  auto openProjectReaction = new pqTruchasProjectOpenReaction(m_openProjectAction);

  // Initialize "Recent Projects" menu
  m_recentProjectsAction = new QAction(tr("Recent Projects"), this);
  QMenu* menu = new QMenu();
  m_recentProjectsAction->setMenu(menu);
  m_recentProjectsMenu = new pqTruchasRecentProjectsMenu(menu, menu);

  // Initialize "Import Model" action
  m_importModelAction = new QAction(tr("Add Induction Heating Model..."), this);
  m_importModelAction->setEnabled(false);
  auto importModelReaction = new pqTruchasProjectImportModelReaction(m_importModelAction);

  // Initialize "Save Project" action
  m_saveProjectAction = new QAction(tr("Save Project"), this);
  auto saveProjectReaction = new pqTruchasProjectSaveReaction(m_saveProjectAction);

  // Initialize "Save As Project" action
  // m_saveAsProjectAction = new QAction(tr("Save As Project..."), this);
  // auto saveAsProjectReaction = new pqTruchasProjectSaveAsReaction(m_saveAsProjectAction);

  // Initialize "Export Project" action
  m_exportProjectAction = new QAction(tr("Export Project"), this);
  auto exportProjectReaction = new pqTruchasProjectExportReaction(m_exportProjectAction);

  // Insert separator
  QAction* sep = new QAction("", this);
  sep->setSeparator(true);

  // Initialize "Close Project" action
  m_closeProjectAction = new QAction(tr("Close Project"), this);
  auto closeProjectReaction = new pqTruchasProjectCloseReaction(m_closeProjectAction);
  QObject::connect(closeProjectReaction, &pqTruchasProjectCloseReaction::projectClosed, this,
    &pqTruchasProjectMenu::onProjectClosed);

  // For now, presume that there is no project loaded at startup
  this->onProjectClosed();

  // Connect to project loader
  auto projectLoader = pqTruchasProjectLoader::instance();
  QObject::connect(projectLoader, &pqTruchasProjectLoader::projectOpened, this,
    &pqTruchasProjectMenu::onProjectOpened);

  return true;
} // startup()

//-----------------------------------------------------------------------------
void pqTruchasProjectMenu::shutdown()
{
  // Reserved for future use.
}

//-----------------------------------------------------------------------------
void pqTruchasProjectMenu::onProjectOpened(smtk::project::ProjectPtr project)
{
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);
  m_exportProjectAction->setEnabled(true);

  smtk::simulation::truchas::ProjectUtils projectUtils;
  auto ihModel = projectUtils.getByRole<smtk::model::Resource>(project, "induction-heating");
  m_importModelAction->setEnabled(ihModel == nullptr);
}

//-----------------------------------------------------------------------------
void pqTruchasProjectMenu::onProjectClosed()
{
  m_newProjectAction->setEnabled(true);
  m_openProjectAction->setEnabled(true);
  m_recentProjectsAction->setEnabled(true);
  m_saveProjectAction->setEnabled(false);
  // m_saveAsProjectAction->setEnabled(false);
  m_closeProjectAction->setEnabled(false);
  m_exportProjectAction->setEnabled(false);
  m_importModelAction->setEnabled(false);
}
