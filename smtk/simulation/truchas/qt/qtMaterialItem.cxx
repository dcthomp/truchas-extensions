//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtMaterialItem.h"

#include "smtk/simulation/truchas/qt/ctkCollapsibleButton.h"
#include "smtk/simulation/truchas/qt/qtMaterialAttribute.h"
#include "smtk/simulation/truchas/qt/qtMaterialMultiPhaseMoveButtons.h"
#include "smtk/simulation/truchas/qt/qtValidators.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/SearchStyle.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/qt/qtAttributeItemInfo.h"
#include "smtk/extension/qt/qtBaseAttributeView.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/simulation/UserData.h"

#include <QAbstractButton>
#include <QButtonGroup>
#include <QCheckBox>
#include <QColor>
#include <QDebug>
#include <QFrame>
#include <QHeaderView>
#include <QLabel>
#include <QMargins>
#include <QMessageBox>
#include <QPushButton>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QTextStream>
#include <QTimer>
#include <QVBoxLayout>

#include <map>
#include <memory>

#define ALERT_ICON_PATH ":/icons/attribute/errorAlert.png"

namespace
{
// Table widget columns
const int select_column = 0;
const int status_column = 1;
const int data_column = 2;
const int move_column = 3;

// User data keys
const std::string collapsed_state_key = "_collapsed_state_";
} // namespace

namespace smtk
{
namespace simulation
{
// UserDataSet class is for storing collapsed state of
// each row in multiphase table widget.
class UserDataSet : public smtk::simulation::UserData
{
public:
  static smtk::simulation::UserDataPtr New()
  {
    return smtk::simulation::UserDataPtr(new UserDataSet());
  }

  void insert(std::size_t value) { m_set.insert(value); }

  void erase(std::size_t value) { m_set.erase(value); }

  bool contains(std::size_t value) { return m_set.count(value); }

  virtual ~UserDataSet() {}

protected:
  UserDataSet() {}
  std::set<std::size_t> m_set;
};

} // namespace simulation
} // namespace smtk

typedef smtk::shared_ptr<smtk::simulation::UserDataSet> UserDataSetPtr;
typedef smtk::extension::qtAttributeItemInfo qtItemInfo;

class qtMaterialItemInternals
{
public:
  qtMaterialAttribute* m_qAttribute;
  smtk::attribute::GroupItemPtr m_phasesItem;
  smtk::attribute::GroupItemPtr m_transitionsItem;
  QTableWidget* m_tableWidget;
  QWidget* m_dataWidget;
  std::map<std::string, smtk::extension::qtAttributeItemInfo> m_itemViewMap;
  QButtonGroup* m_checkboxGroup; // for phase-select checkboxes

  // Include remove button so that we can show/hide for one vs. multiple phases
  QPushButton* m_removeSelectedButton;

  qtMaterialItemInternals() {}

  UserDataSetPtr getUserDataSet(smtk::attribute::ItemPtr item)
  {
    auto userData = item->userData(collapsed_state_key);
    if (userData == nullptr)
    {
      userData = smtk::simulation::UserDataSet::New();
      item->setUserData(collapsed_state_key, userData);
    }
    return smtk::dynamic_pointer_cast<smtk::simulation::UserDataSet>(userData);
  }

  // Functions to store & retrieve the collapsed state for rows in the widget table
  void saveCollapsedState(smtk::attribute::ItemPtr item, std::size_t element, bool state)
  {
    auto userDataSet = this->getUserDataSet(item);
    if (state)
    {
      userDataSet->insert(element);
    }
    else
    {
      userDataSet->erase(element);
    }
  }

  bool getCollapsedState(smtk::attribute::ItemPtr item, std::size_t element)
  {
    auto userDataSet = this->getUserDataSet(item);
    return userDataSet->contains(element);
  }
};

qtMaterialItem::qtMaterialItem(const qtItemInfo& phasesInfo,
  const qtItemInfo& transitionsInfo,
  qtMaterialAttribute* qAttribute)
  : smtk::extension::qtItem(phasesInfo)
{
  this->Internals = new qtMaterialItemInternals;
  this->Internals->m_qAttribute = qAttribute;
  m_itemInfo.createNewDictionary(this->Internals->m_itemViewMap);
  auto att = qAttribute->attribute();
  this->Internals->m_phasesItem = att->findGroup("phases");
  this->Internals->m_transitionsItem = att->findGroup("transitions");
  this->Internals->m_tableWidget = nullptr;
  this->Internals->m_dataWidget = nullptr;
  this->Internals->m_checkboxGroup = new QButtonGroup(this);
  this->Internals->m_checkboxGroup->setExclusive(false);

  // This is a hack to add ItemViews from transitionsInfo
  qtItemInfo tempInfo = transitionsInfo;
  tempInfo.createNewDictionary(this->Internals->m_itemViewMap);

  this->createWidget();
}

qtMaterialItem::~qtMaterialItem()
{
  delete this->Internals;
}

void qtMaterialItem::createWidget()
{
  auto parentWidget = this->Internals->m_qAttribute->widget();
  m_widget = new QFrame(parentWidget);
  m_widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
  auto layout = new QVBoxLayout();
  m_widget->setLayout(layout);

  std::size_t phasesCount = 0;
  std::size_t transitionsCount = 0;

  // Check some validity conditions for the material attribute
  QString errorMessage;

  // Check that phases and transitions items are defined
  if (this->Internals->m_phasesItem == nullptr)
  {
    errorMessage = "Error: no \"phases\" item";
  }
  else if (this->Internals->m_transitionsItem == nullptr)
  {
    errorMessage = "Error: no \"transitions\" item";
  }
  else
  {
    // Check that number of phases is 1 more than number of transitions
    phasesCount = this->Internals->m_phasesItem->numberOfGroups();
    transitionsCount = this->Internals->m_transitionsItem->numberOfGroups();
    if (phasesCount > 0 && phasesCount != transitionsCount + 1)
    {
      QTextStream qs(&errorMessage);
      qs << "Error: invalid number of transitions should be 1 less than number of phases"
         << ", phases: " << phasesCount << ", transitions: " << transitionsCount;
    }
  }

  if (!errorMessage.isEmpty())
  {
    QLabel* label = new QLabel(errorMessage, m_widget);
    layout->addWidget(label);
    return;
  }

  // Control panel
  QWidget* controlWidget = new QWidget;
  QHBoxLayout* controlLayout = new QHBoxLayout;
  QPushButton* addPhaseButton = new QPushButton("Add Phase");
  this->Internals->m_removeSelectedButton = new QPushButton("Remove Selected");
  this->Internals->m_removeSelectedButton->setEnabled(false);
  controlWidget->setLayout(controlLayout);
  controlLayout->addWidget(addPhaseButton);
  controlLayout->addWidget(this->Internals->m_removeSelectedButton);
  controlLayout->addStretch(); // left-aligns buttons
  QObject::connect(addPhaseButton, &QPushButton::clicked, this, &qtMaterialItem::addPhase);
  QObject::connect(this->Internals->m_removeSelectedButton,
    &QPushButton::clicked,
    this,
    &qtMaterialItem::removeSelected);

  void (QButtonGroup::*clicked)(QAbstractButton*) = &QButtonGroup::buttonClicked;
  QObject::connect(
    this->Internals->m_checkboxGroup, clicked, this, &qtMaterialItem::onPhaseSelected);

  layout->addWidget(controlWidget);

  this->Internals->m_dataWidget = this->createDataWidget();

  layout->addWidget(this->Internals->m_dataWidget);
}

void qtMaterialItem::updateItemData()
{
  this->recreateDataWidget();
}

QWidget* qtMaterialItem::createElementWidget(smtk::attribute::GroupItemPtr groupItem,
  std::size_t element,
  const QString& labelString)
{
  // qDebug() << "createElementWidget" << element;
  QWidget* frame = nullptr;
  int verticalPadding = 6; // for esthetics (multiple-phase only)
  bool isSinglePhase = groupItem->name() == "phases" && groupItem->numberOfGroups() == 1;
  if (isSinglePhase)
  {
    // Display single-phase material with simple frame
    frame = new QFrame();
    this->Internals->m_removeSelectedButton->hide();
  }
  else
  {
    this->Internals->m_removeSelectedButton->show();

    // Display multiple-phase material with collapsible frames
    auto collapsibleFrame = new ctkCollapsibleButton();
    collapsibleFrame->setVerticalPadding(verticalPadding); // set this *before* text
    collapsibleFrame->setContentsFrameShape(QFrame::StyledPanel);
    collapsibleFrame->setCollapsedHeight(0);
    collapsibleFrame->setText(labelString);
    collapsibleFrame->setMinimumWidth(400); // cosmetic hack

    // Check if this element was previously in a "collapsed" row
    bool isCollapsed = this->Internals->getCollapsedState(groupItem, element);
    collapsibleFrame->setCollapsed(isCollapsed);

    QObject::connect(collapsibleFrame,
      &ctkCollapsibleButton::contentsCollapsed,
      this,
      &qtMaterialItem::onElementCollapsed);

    // Alternate row colors
    QPalette pal = collapsibleFrame->palette();
    QString hex = groupItem->name() == "phases" ? "#a0a0a0" : "#808080";
    pal.setColor(QPalette::Button, QColor(hex));
    collapsibleFrame->setAutoFillBackground(true);
    collapsibleFrame->setPalette(pal);
    collapsibleFrame->update();

    frame = collapsibleFrame;
  }

  auto layout = new QVBoxLayout(frame);
  // Adjust margin to match vertical padding - dont know why this is needed
  auto margins = layout->contentsMargins();
  int top = margins.top();
  margins.setTop(top + verticalPadding);
  layout->setContentsMargins(margins);

  const std::size_t numItems = groupItem->numberOfItemsPerGroup();
  for (std::size_t i = 0; i < numItems; i++)
  {
    auto item = groupItem->item(element, static_cast<int>(i));
    if (isSinglePhase && item->name() == "name")
    {
      // Omit phase name for single-phase materials
      continue;
    }

    smtk::extension::qtItem* childItem = nullptr;

    // Check for custom item view
    auto it = this->Internals->m_itemViewMap.find(item->name());
    if (it != this->Internals->m_itemViewMap.end())
    {
      auto info = it->second;
      info.setParentWidget(m_widget);
      info.setItem(item);
      childItem = m_itemInfo.uiManager()->createItem(info);
    }
    else
    {
      smtk::view::Configuration::Component comp; // create a default style (empty component)
      qtItemInfo info(item, comp, m_widget, m_itemInfo.baseView());
      childItem = m_itemInfo.uiManager()->createItem(info);
    }

    if (childItem)
    {
      this->addChildItem(childItem);
      layout->addWidget(childItem->widget());

      if (!isSinglePhase && item->name() == "name")
      {
        QObject::connect(childItem, &qtItem::modified, [frame, element, childItem]() {
          auto* dataFrame = static_cast<ctkCollapsibleButton*>(frame);
          QString title;
          QTextStream qs(&title);
          qs << "Phase" << element + 1 << ": "
             << childItem->itemAs<smtk::attribute::StringItem>()->value().c_str();
          dataFrame->setText(title);
        });
      } // if ("name")
      QObject::connect(childItem, &qtItem::modified, this, &qtMaterialItem::onItemModified);
    } // if (childItem)
  }   // for (i)

  return frame;
}

void qtMaterialItem::updateTableItemSizes()
{
  this->Internals->m_tableWidget->resizeRowsToContents();
  this->Internals->m_tableWidget->resizeColumnsToContents();
  this->Internals->m_tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
}

void qtMaterialItem::setTableRowFromPhase(int row, size_t element)
{
  QString title;
  QTextStream qs(&title);
  auto nameItem =
    this->Internals->m_phasesItem->findAs<smtk::attribute::StringItem>(element, "name");
  qs << "Phase" << element + 1;
  if (nameItem->isSet())
  {
    qs << ": " << nameItem->value().c_str();
  }
  auto* phaseWidget = this->createElementWidget(this->Internals->m_phasesItem, element, title);
  this->setTableRowWidgets(row, phaseWidget);
}

void qtMaterialItem::setTableRowFromTransition(int row, size_t element)
{
  QString title = "Phase Change";
  auto* transitionWidget =
    this->createElementWidget(this->Internals->m_transitionsItem, element, title);
  this->setTableRowWidgets(row, transitionWidget);
}

void qtMaterialItem::setTableRowWidgets(int row, QWidget* widget)
{
  if (row >= this->Internals->m_tableWidget->rowCount())
  {
    this->Internals->m_tableWidget->insertRow(row);
  }

  auto* selectBoxWidget = new QWidget();
  auto* selectBoxLayout = new QVBoxLayout(selectBoxWidget);
  auto* selectBox = new QCheckBox();
  this->Internals->m_checkboxGroup->addButton(selectBox);
  selectBoxLayout->addWidget(selectBox);
  selectBoxLayout->setAlignment(Qt::AlignCenter);
  selectBoxLayout->setContentsMargins(0, 0, 0, 0);

  auto* moveButton = new qtMaterialMultiPhaseMoveButtons(nullptr,
    row,
    static_cast<size_t>(row) == 2 * (this->Internals->m_phasesItem->numberOfGroups() - 1));

  this->Internals->m_tableWidget->setCellWidget(row, select_column, selectBoxWidget);
  this->Internals->m_tableWidget->setCellWidget(row, data_column, widget);
  this->Internals->m_tableWidget->setCellWidget(row, move_column, moveButton);

  if (row % 2 != 0)
  {
    selectBoxWidget->setVisible(false);
    selectBoxWidget->setEnabled(false);
    selectBox->setVisible(false);
    selectBox->setEnabled(false);
  }

  this->checkStatus(row);
  this->updateTableItemSizes();

  QObject::connect(static_cast<ctkCollapsibleButton*>(widget),
    &ctkCollapsibleButton::contentsCollapsed,
    this,
    &qtMaterialItem::updateTableItemSizes);
  QObject::connect(
    moveButton, &qtMaterialMultiPhaseMoveButtons::moveUp, this, &qtMaterialItem::movePhaseUp);
  QObject::connect(
    moveButton, &qtMaterialMultiPhaseMoveButtons::moveDown, this, &qtMaterialItem::movePhaseDown);
}

void qtMaterialItem::swapPhase(size_t phaseA, size_t phaseB)
{
  if (phaseA == phaseB)
  {
    return;
  }
  if (phaseA > phaseB)
  {
    std::swap(phaseA, phaseB);
  }
  int rowA = 2 * static_cast<int>(phaseA);
  auto* phaseATableWidget =
    static_cast<ctkCollapsibleButton*>(this->Internals->m_tableWidget->cellWidget(rowA, 2));
  int rowB = 2 * static_cast<int>(phaseB);
  auto* phaseBTableWidget =
    static_cast<ctkCollapsibleButton*>(this->Internals->m_tableWidget->cellWidget(rowB, 2));
  bool phaseACollapsed = phaseATableWidget->collapsed();
  bool phaseBCollapsed = phaseBTableWidget->collapsed();

  this->Internals->m_phasesItem->rotate(phaseA, phaseB);
  if (phaseB - phaseA > 1)
  {
    this->Internals->m_phasesItem->rotate(phaseB - 1, phaseA);
  }

  setTableRowFromPhase(2 * static_cast<int>(phaseA), phaseA);
  setTableRowFromPhase(2 * static_cast<int>(phaseB), phaseB);

  // setRowCollapsed(2 * static_cast<int>(phaseB), phaseACollapsed);
  // setRowCollapsed(2 * static_cast<int>(phaseA), phaseBCollapsed);
}

void qtMaterialItem::setTransitionToDefault(int row)
{
  assert(row % 2 != 0);
  auto transitionsItem = this->Internals->m_transitionsItem;
  size_t element = static_cast<size_t>(row) / 2;
  for (size_t i = 0; i < transitionsItem->numberOfItemsPerGroup(); i++)
  {
    auto item = transitionsItem->item(element, i);
    std::static_pointer_cast<smtk::attribute::ValueItem>(item)->setToDefault();
  }
  this->setTableRowFromTransition(row, element);
}

void qtMaterialItem::setTransitionsToDefault(const std::vector<int>& rows)
{
  for (int row : rows)
  {
    if (row >= 0 && row < this->Internals->m_tableWidget->rowCount())
    {
      setTransitionToDefault(row);
    }
  }
}

QWidget* qtMaterialItem::createDataWidget()
{
  int phasesCount = static_cast<int>(this->Internals->m_phasesItem->numberOfGroups());
  if (phasesCount == 1)
  {
    QString title;
    QTextStream qs(&title);

    // Make sure that "name" item, which is not displayed, is nonetheless valid
    auto nameItem = this->Internals->m_phasesItem->findAs<smtk::attribute::StringItem>(0, "name");
    if (!nameItem->isValid())
    {
      auto att = nameItem->attribute();
      nameItem->setValue(att->name());
    }

    return this->createElementWidget(this->Internals->m_phasesItem, 0, title);
  }
  else
  {
    auto& tableWidget = this->Internals->m_tableWidget;
    if (tableWidget != nullptr)
    {
      tableWidget->setRowCount(0);
    }
    else
    {
      tableWidget = new QTableWidget;
      tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
      tableWidget->setFocusPolicy(Qt::NoFocus);
      tableWidget->setSelectionMode(QAbstractItemView::NoSelection);

      tableWidget->setColumnCount(4);
      tableWidget->setHorizontalHeaderLabels({ "Select", "Status", "Phase/Transition", "Move" });
      tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    }

    int row = 0;
    this->setTableRowFromPhase(row, 0);
    row++;
    for (std::size_t i = 0; i < this->Internals->m_transitionsItem->numberOfGroups(); ++i)
    {
      this->setTableRowFromTransition(row, i);
      row++;
      this->setTableRowFromPhase(row, i + 1);
      row++;
    }

    return tableWidget;
  }
}

void qtMaterialItem::recreateDataWidget()
{
  if (this->Internals->m_dataWidget != nullptr)
  {

    delete this->Internals->m_dataWidget;
    this->Internals->m_dataWidget = nullptr;
    this->Internals->m_tableWidget = nullptr;
    this->Internals->m_dataWidget = this->createDataWidget();
    this->m_widget->layout()->addWidget(this->Internals->m_dataWidget);
  }
}

void qtMaterialItem::movePhaseDown(int row)
{
  assert(row % 2 == 0);
  size_t phase = static_cast<size_t>(row) / 2;
  this->swapPhase(phase, phase + 1);
  std::vector<int> affectedTransitions;
  affectedTransitions.push_back(row - 1);
  affectedTransitions.push_back(row + 1);
  affectedTransitions.push_back(row + 3);
  setTransitionsToDefault(affectedTransitions);
}

void qtMaterialItem::movePhaseUp(int row)
{
  assert(row % 2 == 0);
  size_t phase = static_cast<size_t>(row) / 2;
  this->swapPhase(phase, phase - 1);
  std::vector<int> affectedTransitions;
  affectedTransitions.push_back(row + 1);
  affectedTransitions.push_back(row - 1);
  affectedTransitions.push_back(row - 3);
  setTransitionsToDefault(affectedTransitions);
}

void qtMaterialItem::removeSelected()
{
  auto* table = this->Internals->m_tableWidget;
  std::vector<int> selectedList;
  for (int row = 0; row < table->rowCount(); row += 2)
  {
    auto* checkBox =
      static_cast<QCheckBox*>(table->cellWidget(row, 0)->layout()->itemAt(0)->widget());
    if (checkBox->isChecked())
    {
      selectedList.push_back(row / 2);
    }
  }
  if (selectedList.empty())
  {
    return;
  }

  // We currently support removing one phase at a time
  int phaseNumber = selectedList[0];
  emit this->removePhase(phaseNumber);
}

void qtMaterialItem::setRowCollapsed(int row, bool collapsed)
{
  auto* collapsibleButton =
    static_cast<ctkCollapsibleButton*>(this->Internals->m_tableWidget->cellWidget(row, 2));
  collapsibleButton->setCollapsed(collapsed);
}

void qtMaterialItem::onElementCollapsed(bool state)
{
  auto* frame = dynamic_cast<ctkCollapsibleButton*>(this->sender());
  assert(frame);

  // Figure out which row the sender is in and save its state
  int senderRow = -1;
  auto* tableWidget = this->Internals->m_tableWidget;
  for (int row = 0; row < tableWidget->rowCount(); ++row)
  {
    if (tableWidget->cellWidget(row, data_column) == frame)
    {
      senderRow = row;
      break;
    }
  }
  assert(senderRow >= 0);

  smtk::attribute::GroupItemPtr groupItem =
    senderRow % 2 ? this->Internals->m_transitionsItem : this->Internals->m_phasesItem;
  std::size_t element = senderRow / 2;
  this->Internals->saveCollapsedState(groupItem, element, state);
}

void qtMaterialItem::onItemModified()
{
  auto qItem = qobject_cast<smtk::extension::qtItem*>(this->sender());
  if (qItem == nullptr)
  {
    return;
  }

  // For single phase, can simply relay the modified signal
  if (this->Internals->m_phasesItem->numberOfGroups() == 1)
  {
    emit this->modified();
    return;
  }

  // For multi-phase, figure out which row changed
  auto item = qItem->item();
  int changedRow = -1;
  for (int row = 0; row < this->Internals->m_tableWidget->rowCount(); ++row)
  {
    auto groupItem = row % 2 ? this->Internals->m_transitionsItem : this->Internals->m_phasesItem;
    std::size_t element = static_cast<std::size_t>(row) / 2;
    auto foundItem = groupItem->find(element, item->name(), smtk::attribute::RECURSIVE_ACTIVE);
    if (foundItem == item)
    {
      changedRow = row;
      break;
    }
  } // for (row)

  if (changedRow < 0)
  {
    qWarning() << "Failed to find row where change was made";
    return;
  }

  // (else) Update status_column
  this->checkStatus(changedRow);

  // For reasons not currently understood, we need to force a redraw of
  // the full cell widget (collapsible frame) when the changed item contains
  // other items (group item and discrete value items with children).
  bool hasChildren = false;
  auto valItem = std::dynamic_pointer_cast<smtk::attribute::ValueItem>(item);
  hasChildren = (valItem != nullptr) && (valItem->numberOfChildrenItems() > 0);
  hasChildren |= item->type() == smtk::attribute::Item::GroupType;
  if (hasChildren)
  {
    auto cellWidget = this->Internals->m_tableWidget->cellWidget(changedRow, data_column);
    auto cbtn = dynamic_cast<ctkCollapsibleButton*>(cellWidget);
    // And the only way we know to reliably redraw the collapsible frame is to
    // collapse then re-open it.
    cbtn->setCollapsed(true);
    // And we need single shot to make sure the first event isn't skipped.
    QTimer::singleShot(1, [cbtn]() { cbtn->setCollapsed(false); });
  }

  emit this->modified();
}

void qtMaterialItem::onPhaseSelected(QAbstractButton* button)
{
  // Enable or disable the remove-phase button based on whether phase is selected or not
  bool isChecked = button->isChecked();
  this->Internals->m_removeSelectedButton->setEnabled(isChecked);

  if (!isChecked)
  {
    return;
  }

  // If checked, then uncheck all other buttons
  for (auto b : this->Internals->m_checkboxGroup->buttons())
  {
    b->setChecked(b == button);
  }
}

void qtMaterialItem::checkStatus(int row)
{
  smtk::attribute::ResourcePtr attResource = this->attributeResource();
  std::size_t element = static_cast<std::size_t>(row) / 2;
  bool isValid = false;
  if (row % 2)
  {
    isValid = isTransitionElementValid(this->Internals->m_transitionsItem, element);
  }
  else
  {
    isValid = isPhaseElementValid(this->Internals->m_phasesItem, element);
  }

  QTableWidgetItem* statusItem =
    isValid ? nullptr : new QTableWidgetItem(QIcon(ALERT_ICON_PATH), "", smtk_USER_DATA_TYPE);
  this->Internals->m_tableWidget->setItem(row, status_column, statusItem);
}

#undef ALERT_ICON_PATH
