//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/Resource.h"
#include "smtk/io/AttributeReader.h"
#include "smtk/io/Logger.h"

#include "smtk/simulation/truchas/qt/qtInductionCoilsModel.h"

#include <QApplication>
#include <QDir>
#include <QHeaderView>
#include <QString>
#include <QTableView>

#include <cassert>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cout << "\n"
              << "Load attribute resource and display induction-heating attribute\n"
              << "with custom view of the coils model data."
              << "\n"
              << "Usage: qtCoilsModelPreview attribute_filename  [output_filename]"
              << "\n"
              << std::endl;
    return -1;
  }

  QApplication app(argc, argv);

  // Instantiate and load attribute resource
  smtk::attribute::ResourcePtr attResource = smtk::attribute::Resource::create();
  char* inputPath = argv[1];
  smtk::io::AttributeReader reader;
  smtk::io::Logger inputLogger;
  bool err = reader.read(attResource, inputPath, true, inputLogger);
  if (err)
  {
    std::cout << "Error loading attribute file -- exiting"
              << "\n";
    std::cout << inputLogger.convertToString() << std::endl;
    return -2;
  }

  // Get induction-heating attribute and initialize coils model
  std::string attName = "induction-heating";
  auto att = attResource->findAttribute(attName);
  assert(att != nullptr);

  auto coilsModel = new qtInductionCoilsModel(nullptr, att);
  assert(coilsModel != nullptr);

  auto tableView = new QTableView();
  tableView->setModel(coilsModel);
  tableView->resize(800, 400);
  tableView->show();

  int retval = app.exec();

  // Release resources

  return retval;
}
