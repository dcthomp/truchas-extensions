//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtkTruchasCoilsView.h"

#include "smtk/simulation/truchas/qt/qtDragSelfFilter.h"
#include "smtk/simulation/truchas/qt/qtInductionCoilsModel.h"
#include "smtk/simulation/truchas/qt/qtInductionSourceModel.h"

#include "ui_smtkTruchasCoilsView.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Categories.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Resource.h"

#include <QCoreApplication>
#include <QDebug>
#include <QHeaderView>
#include <QItemSelectionModel>
#include <QLabel>
#include <QMessageBox>
#include <QTableView>
#include <QVBoxLayout>
#include <Qt>

class smtkTruchasCoilsViewInternals : public Ui::smtkTruchasCoilsView
{
public:
  // Internal member data
  QWidget* m_widget;

  smtk::attribute::AttributePtr m_attribute;
  smtk::attribute::GroupItemPtr m_coilsItem;
  smtk::attribute::GroupItemPtr m_sourceItem;

  qtInductionCoilsModel* m_coilsModel;
  qtInductionSourceModel* m_sourceModel;

  void initialize(smtk::attribute::ResourcePtr attResource, QWidget* widget)
  {
    this->setupUi(widget);
    m_widget = widget;

    m_attribute = attResource->findAttribute("induction-heating");
    assert(m_attribute);
    m_sourceItem = m_attribute->findGroup("source");
    assert(m_sourceItem);
    m_coilsItem = m_attribute->findGroup("coils");
    assert(m_coilsItem);

    m_coilsModel = new qtInductionCoilsModel(widget, m_attribute);
    m_sourceModel = new qtInductionSourceModel(widget, m_attribute);

    m_coilsTableView->setModel(m_coilsModel);
    m_sourceTableView->setModel(m_sourceModel);

    m_coilsTableView->setDragEnabled(false);
    m_coilsTableView->setDropIndicatorShown(true);
    m_coilsTableView->setAcceptDrops(true);
    m_coilsTableView->setDefaultDropAction(Qt::IgnoreAction);
    m_coilsTableView->setDragDropMode(QTableView::DragDrop);
    m_coilsTableView->setDragDropOverwriteMode(true);

    m_sourceTableView->setDragEnabled(false);
    m_sourceTableView->setDropIndicatorShown(true);
    m_sourceTableView->setAcceptDrops(true);
    m_sourceTableView->setDefaultDropAction(Qt::IgnoreAction);
    m_sourceTableView->setDragDropMode(QTableView::DragDrop);
    m_sourceTableView->setDragDropOverwriteMode(true);

    // Adjust source table's vertical header to make it easier to select.
    m_sourceTableView->verticalHeader()->setMinimumWidth(40);
    m_sourceTableView->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);

    // Make sure all events have been handled before connecting signals & slots
    QCoreApplication::processEvents();

    // Connect slots using lambda functions
    QObject::connect(m_addCoilButton, &QPushButton::clicked, [this] { this->addCoil(); });
    QObject::connect(m_addTimeButton, &QPushButton::clicked, [this] { this->addTime(); });
    QObject::connect(m_coilsTableView->selectionModel(), &QItemSelectionModel::selectionChanged,
      [this] { this->coilsSelectionChanged(); });
    QObject::connect(m_sourceTableView->selectionModel(), &QItemSelectionModel::selectionChanged,
      [this] { this->sourceSelectionChanged(); });
    QObject::connect(m_sourceModel, &QAbstractItemModel::dataChanged,
      [this](const QModelIndex& topLeft, const QModelIndex& bottomRight) {
        this->sourceDataChanged(topLeft, bottomRight);
      });

    // Drag & drop events occur on the source and coils models directly, via dropMimeData().
    // The source model can process them immediately, but the coils model
    // events must be pushed back here, because *both* models must be updated together.
    QObject::connect(m_coilsModel, &qtInductionCoilsModel::requestRotate,
      [this](int fromRow, int toRow) { this->rotateCoils(fromRow, toRow); });

    QObject::connect(
      m_removeCoilButton, &QPushButton::clicked, [this] { this->removeSelectedCoil(); });
    QObject::connect(
      m_removeTimeButton, &QPushButton::clicked, [this] { this->removeSelectedTime(); });

    // Up/Down buttons not used
    m_coilUpButton->hide();
    m_coilDownButton->hide();
    m_timeUpButton->hide();
    m_timeDownButton->hide();
    // QObject::connect(m_coilUpButton, &QPushButton::clicked, [this] { this->notImplemented(); });
    // QObject::connect(m_coilDownButton, &QPushButton::clicked, [this] { this->notImplemented(); });
    // QObject::connect(m_timeUpButton, &QPushButton::clicked, [this] { this->notImplemented(); });
    // QObject::connect(m_timeDownButton, &QPushButton::clicked, [this] { this->notImplemented(); });

    QObject::connect(m_sortRowsButton, &QPushButton::clicked, [this] { this->sortTimeRows(); });

    this->timeDataChanged();
  }

  void sourceDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight)
  {
    // Respond to dataChanged signal in model to determine if time-data changed
    // Since time is in the first column, see if change occurred there
    if (topLeft.column() == 0)
    {
      this->timeDataChanged();
    }

    // (else)
  }

  void timeDataChanged()
  {
    // Respond to row add/remove from source table
    // Updates enable state for sort button

    // If 0 or 1 row, nothing to sort
    if (m_sourceModel->rowCount() <= 1)
    {
      m_sortRowsButton->setEnabled(false);
      return;
    }

    // Check if source times are monotonic
    // Only enable dort button if ALL time items are set
    // Start with index 1 because index 0 is implied to be minus infinity
    auto firstItem = m_sourceItem->findAs<smtk::attribute::DoubleItem>(1, "time");
    if (!firstItem->isSet())
    {
      m_sortRowsButton->setEnabled(false);
      return;
    }

    double previousTime = firstItem->value();
    for (std::size_t i = 2; i < m_sourceItem->numberOfGroups(); ++i)
    {
      auto nextItem = m_sourceItem->findAs<smtk::attribute::DoubleItem>(i, "time");
      if (!nextItem->isSet())
      {
        m_sortRowsButton->setEnabled(false);
        return;
      }
      double nextTime = nextItem->value();
      if (nextTime < previousTime)
      {
        // Definitely NOT montonically increasing
        m_sortRowsButton->setEnabled(true);
        return;
      }
      previousTime = nextTime;
    }

    // (else) monotonically increasing
    m_sortRowsButton->setEnabled(false);
  }

  // (slot)
  void rotateCoils(int fromRow, int toRow)
  {
    // Updates both source and coil tables together.
    int fromGroupPosition = fromRow;
    int toGroupPosition = toRow;
    if (toRow < 0)
    {
      toRow = m_coilsModel->rowCount();
      toGroupPosition = toRow - 1;
    }

    // Check that the move is supported by Qt's logic
    if (!m_coilsModel->beginRotateCoil(fromRow, toRow))
    {
      return;
    }

    int fromCol = fromRow + 2;
    int toCol = toRow + 2;
    if (!m_sourceModel->beginRotateCoil(fromCol, toCol))
    {
      qWarning() << "Invalid source table move from column" << fromCol << "to" << toCol;
      m_coilsModel->endRotateCoil();
      return;
    }

    m_coilsItem->rotate(fromGroupPosition, toGroupPosition);
    m_coilsModel->endRotateCoil();
    m_sourceModel->endRotateCoil();

    // Make sure coil remains selected in each view
    if (toRow == m_coilsModel->rowCount())
    {
      toRow -= 1;
      toCol -= 1;
    }

    QModelIndex coilsFirstIndex = m_coilsModel->index(toRow, 0);
    int coilsLastColumn = m_coilsModel->columnCount() - 1;
    QModelIndex coilsLastIndex = m_coilsModel->index(toRow, coilsLastColumn);
    QItemSelection coilsSelection(coilsFirstIndex, coilsLastIndex);
    m_coilsTableView->selectionModel()->select(coilsSelection, QItemSelectionModel::ClearAndSelect);

    QModelIndex sourceFirstIndex = m_sourceModel->index(0, toCol);
    int sourceLastRow = m_sourceModel->rowCount() - 1;
    QModelIndex sourceLastIndex = m_sourceModel->index(sourceLastRow, toCol);
    QItemSelection sourceSelection(sourceFirstIndex, sourceLastIndex);
    m_sourceTableView->selectionModel()->select(
      sourceSelection, QItemSelectionModel::ClearAndSelect);
  }

  // Return row number if a single row is selected,
  // otherwise returns -1
  int selectedRowNumber(const QItemSelectionModel* selectionModel)
  {
    auto modelIndexList = selectionModel->selectedIndexes();
    if (modelIndexList.empty())
    {
      return -1;
    }

    // Note that we cannot check that the number of indexes matches
    // the model column count, because some items might be disabled.
    // So instead...

    // First check if all indexes are from the same row.
    int row = -1;
    for (auto& modelIndex : modelIndexList)
    {
      int indexRow = modelIndex.row();
      if (row == -1) // first index
      {
        // First modelIndex
        row = indexRow;
      }
      else if (indexRow != row)
      {
        // Different row numbers
        return -1;
      }
    } // for (modelIndex)

    // Confirm that the entire row is selected.
    return selectionModel->isRowSelected(row, QModelIndex()) ? row : -1;
  }

  void addCoil()
  {
    qDebug() << "Adding Coil";
    // Notify models before changing source data
    m_coilsModel->beginAddCoil();
    m_sourceModel->beginAddCoil();

    // Add subgroup to coils
    bool addGroupOK = m_coilsItem->appendGroup();
    if (!addGroupOK)
    {
      qWarning() << "Coils GroupItem.appendGroup() failed";
      m_coilsModel->endAddCoil();
      m_sourceModel->endAddCoil();
      return;
    }

    // Initialize new coil's "current" item's number of values
    int numTimes = static_cast<int>(m_sourceItem->numberOfGroups());

    int coilIndex = static_cast<int>(m_coilsItem->numberOfGroups()) - 1;
    auto currentItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(coilIndex, "current");
    currentItem->setNumberOfValues(numTimes);

    // Notify models after changes complete
    m_coilsModel->endAddCoil();
    m_sourceModel->endAddCoil();

    // Select first column of new row
    auto firstIndex = m_coilsModel->index(coilIndex, 0);
    m_coilsTableView->selectionModel()->select(firstIndex, QItemSelectionModel::ClearAndSelect);
  }

  void addTime()
  {
    qDebug() << "Adding Time Entry";
    // Notify models before changing source data
    m_coilsModel->beginAddTime();
    m_sourceModel->beginAddTime();

    // Add subgroup to source item
    bool addGroupOK = m_sourceItem->appendGroup();
    if (!addGroupOK)
    {
      qWarning() << "Source GroupItem.appendGroup() failed";
      m_coilsModel->endAddTime();
      m_sourceModel->endAddTime();
      return;
    }

    // Update each coil's "current" data
    auto numCoils = m_coilsItem->numberOfGroups();
    for (int i = 0; i < numCoils; ++i)
    {
      auto currentItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(i, "current");
      bool addCurrentOK = currentItem->appendValue(currentItem->defaultValue());
      if (!addCurrentOK)
      {
        qWarning() << "Problem updating coil" << i << "current item";
      }
    }

    // Notify models after changes complete
    m_coilsModel->endAddTime();
    m_sourceModel->endAddTime();

    this->timeDataChanged();

    // Select first column of new row
    int row = m_sourceModel->rowCount() - 1;
    auto timeIndex = m_sourceModel->index(row, 0);
    m_sourceTableView->selectionModel()->select(timeIndex, QItemSelectionModel::ClearAndSelect);
  }

  void removeSelectedCoil()
  {
    auto selModel = m_coilsTableView->selectionModel();
    int row = this->selectedRowNumber(selModel);
    if (row == -1)
    {
      qWarning() << "Abort -- single coil (row) is not selected";
      return;
    }

    // Notify models before changing source data
    m_coilsModel->beginRemoveCoil(row);
    m_sourceModel->beginRemoveCoil(row);

    bool removeGroupOK = m_coilsItem->removeGroup(row);
    if (!removeGroupOK)
    {
      qWarning() << "Coils GroupItem.removeGroup(" << row << ") failed";
    }

    // Notify models after changes complete
    m_coilsModel->endRemoveCoil();
    m_sourceModel->endRemoveCoil();
  }

  void removeSelectedTime()
  {
    auto selModel = m_sourceTableView->selectionModel();
    int row = this->selectedRowNumber(selModel);
    if (row == -1)
    {
      qWarning() << "Abort -- single time (row) is not selected";
      return;
    }

    // Notify models before changing source data
    m_coilsModel->beginRemoveTime(row);
    m_sourceModel->beginRemoveTime(row);

    // Update the source item
    bool removeGroupOK = m_sourceItem->removeGroup(row);
    if (!removeGroupOK)
    {
      qWarning() << "Source GroupItem.removeGroup(" << row << ") failed";
    }

    // Update each coil's "current" data
    auto numCoils = m_coilsItem->numberOfGroups();
    for (int i = 0; i < numCoils; ++i)
    {
      auto currentItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(i, "current");
      bool removeCurrentOK = currentItem->removeValue(row);
      if (!removeCurrentOK)
      {
        qWarning() << "Problem updating coil" << i << "current item";
      }
    }

    // Notify models after changes complete
    m_coilsModel->endRemoveTime();
    m_sourceModel->endRemoveTime();
  }

  void sortTimeRows()
  {
    // Use bubble sort to arrange m_sourceItem subgroups by their "time" item
    for (std::size_t i = 1; i < m_sourceItem->numberOfGroups(); ++i)
    {
      double left = m_sourceItem->findAs<smtk::attribute::DoubleItem>(i - 1, "time")->value();
      double right = m_sourceItem->findAs<smtk::attribute::DoubleItem>(i, "time")->value();
      if (right < left)
      {
        m_sourceModel->rotateLeft(i);
      }
    }
    m_sortRowsButton->setEnabled(false);
  }

  void coilsSelectionChanged()
  {
    // qDebug() << "coilSelectionChanged";
    auto selModel = m_coilsTableView->selectionModel();
    int row = this->selectedRowNumber(selModel);

    bool rowSelected = row != -1;
    m_removeCoilButton->setEnabled(rowSelected);
    bool upEnable = rowSelected && row > 0;
    m_coilUpButton->setEnabled(upEnable);
    bool downEnable = rowSelected && row < selModel->model()->rowCount() - 1;
    m_coilDownButton->setEnabled(downEnable);

    // Experimental:
    // If coil is selected, then select the corresponding column in source table
    if (rowSelected)
    {
      int sourceColumn = row + 2;
      auto topIndex = m_sourceModel->index(0, sourceColumn);
      int lastRow = m_sourceModel->rowCount() - 1;
      auto bottomIndex = m_sourceModel->index(lastRow, sourceColumn);
      QItemSelection sourceSelection(topIndex, bottomIndex);
      m_sourceTableView->selectionModel()->select(
        sourceSelection, QItemSelectionModel::ClearAndSelect);
    }

    // If single coil is selected, enable drag
    bool enableCoilsDrag = rowSelected && m_coilsModel->rowCount() > 1;
    m_coilsTableView->setDragEnabled(enableCoilsDrag);
  }

  void sourceSelectionChanged()
  {
    // qDebug() << "sourceSelectionChanged";
    auto selModel = m_sourceTableView->selectionModel();
    int row = this->selectedRowNumber(selModel);

    bool rowSelected = row != -1;
    m_removeTimeButton->setEnabled(rowSelected);
    bool upEnable = rowSelected && row > 0;
    m_timeUpButton->setEnabled(upEnable);
    bool downEnable = rowSelected && row < selModel->model()->rowCount() - 1;
    m_timeDownButton->setEnabled(downEnable);

    // If single row is selected, enable drag
    bool enableSourceDrag = rowSelected && m_sourceModel->rowCount() > 1;
    m_sourceTableView->setDragEnabled(enableSourceDrag);
  }

  void notImplemented()
  {
    QMessageBox::information(
      m_widget, "Not Implemented", "Sorry, this action is not yet implemented");
  }

}; // Internals

qtBaseView* smtkTruchasCoilsView::createViewWidget(const smtk::view::Information& info)
{
  smtkTruchasCoilsView* view = new smtkTruchasCoilsView(info);
  view->buildUI();
  return view;
}

smtkTruchasCoilsView::smtkTruchasCoilsView(const smtk::view::Information& info)
  : qtBaseAttributeView(info)
{
  this->Internals = new smtkTruchasCoilsViewInternals();
}

smtkTruchasCoilsView::~smtkTruchasCoilsView()
{
  delete this->Internals;
}

bool smtkTruchasCoilsView::isEmpty() const
{
  // Return true if "Induction Heating" is not part of
  // the current active categories
  auto attRes = this->attributeResource();
  if (!(attRes && attRes->activeCategoriesEnabled()))
  {
    return false; // there is no category filtering
  }
  return (attRes->activeCategories().find("Induction Heating") == attRes->activeCategories().end());
}

void smtkTruchasCoilsView::createWidget()
{
  const smtk::attribute::ResourcePtr attResource = this->attributeResource();

  // Check if "induction-heating" attribute is present
  const std::string attIdentifier("induction-heating");
  auto att = attResource->findAttribute(attIdentifier);
  if (att == nullptr)
  {
    auto defn = attResource->findDefinition(attIdentifier);
    if (defn)
    {
      att = attResource->createAttribute(attIdentifier, defn);
    }
    else
    {
      qWarning() << "Missing" << QString::fromStdString(attIdentifier) << "definition";
    }
  }

  this->Widget = new QFrame(this->parentWidget());
  if (att == nullptr)
  {
    // Display label indicating that the definition is missing
    auto label = new QLabel(this->Widget);
    label->setText("(No \"induction-heating\" definition)");
    auto layout = new QVBoxLayout();
    layout->addWidget(label);
    layout->setAlignment(label, Qt::AlignHCenter);
    this->Widget->setLayout(layout);
  }
  else
  {
    this->Internals->initialize(attResource, this->Widget);

    // Install drag event filters, which prevent dragging from
    // one QTableView into the other.
    auto coilsDragFilter = new qtDragSelfFilter(this->Internals->m_coilsTableView);
    this->Internals->m_coilsTableView->viewport()->installEventFilter(coilsDragFilter);

    auto sourceDragFilter = new qtDragSelfFilter(this->Internals->m_sourceTableView);
    this->Internals->m_sourceTableView->installEventFilter(sourceDragFilter);
  }
}
