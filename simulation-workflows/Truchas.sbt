<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">

  <!-- Category & Analysis specifications -->
  <Categories>
    <Cat>Genre</Cat>
    <Cat>Truchas</Cat>

    <Cat>Enclosure Radiation</Cat>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Solid Mechanics</Cat>
    <Cat>Viscous Flow</Cat>
    <Cat>Void Material</Cat>
  </Categories>

  <Analyses Exclusive="true">
    <Analysis Type="Genre">
      <Cat>Genre</Cat>
    </Analysis>
    <Analysis Type="Truchas">
      <Cat>Truchas</Cat>
    </Analysis>
    <Analysis Type="Heat Transfer" BaseType="Truchas">
      <Cat>Heat Transfer</Cat>
    </Analysis>
    <Analysis Type="Enclosure Radiation" BaseType="Heat Transfer">
      <Cat>Enclosure Radiation</Cat>
    </Analysis>
    <Analysis Type="Induction Heating" BaseType="Heat Transfer">
      <Cat>Induction Heating</Cat>
    </Analysis>
    <Analysis Type="Fluid Flow" BaseType="Truchas">
      <Cat>Fluid Flow</Cat>
    </Analysis>
    <Analysis Type="Viscous Flow" BaseType="Fluid Flow">
      <Cat>Viscous Flow</Cat>
    </Analysis>
    <!-- <Analysis Type="Solid Mechanics" BaseType="Truchas">
      <Cat>Solid Mechanics</Cat>
    </Analysis> -->
    <Analysis Type="Include Void Material" BaseType="Truchas">
      <Cat>Void Material</Cat>
    </Analysis>
  </Analyses>

  <Prerequisites>
    <Rule Type="emissivity">
      <Def>enclosure</Def>
    </Rule>
  </Prerequisites>

  <!-- Attribute definitions -->
  <Includes>
    <!-- Note: order is important, e.g., put expressions first -->
    <File>internal/templates/functions.sbt</File>
    <File>internal/templates/material.sbt</File>
    <File>internal/templates/thermal-surface-condition.sbt</File>
    <File>internal/templates/flow_bc.sbt</File>
    <File>internal/templates/flow-solver.sbt</File>
    <File>internal/templates/numerics-solver.sbt</File>
    <File>internal/templates/body-source-probe.sbt</File>
    <File>internal/templates/enclosure-radiation.sbt</File>
    <File>internal/templates/other.sbt</File>
    <File>internal/templates/initial-conditions.sbt</File>
    <File>internal/templates/induction-heating.sbt</File>

    <File>internal/templates/views/fluid-flow-views.sbt</File>
    <File>internal/templates/views/heat-transfer-views.sbt</File>
  </Includes>

  <Definitions>
    <AttDef Type="description" Label="Description" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="description" Label="text:" MultipleLines="true">
          <BriefDescription>Text added to top of input file</BriefDescription>
          <Categories>
            <Cat>Enclosure Radiation</Cat>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
            <Cat>Void Material</Cat>
          </Categories>
          <DefaultValue>* Truchas simulation</DefaultValue>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Attributes>
    <Att Name="Element Blocks to Ignore (Genre)" Type="ignore-blocks" />
    <Att Name="induction-heating" Type="induction-heating" />
    <Att Name="material-priority" Type="material-priority" />
    <Att Name="|Solid" Type="material.solid" />
    <Att Name="|Void" Type="material.void" />
  </Attributes>

  <!-- Attribute exclusion specifications -->
  <Exclusions>
    <!-- HT BCs are either exterior (boundary) or interface -->
    <Rule>
      <Def>ht.boundary</Def>
      <Def>ht.interface</Def>
    </Rule>

    <!-- HT temperature BCs are exclusive to other exterior BCs -->
    <!-- HT flux BCs are exclusive to htc and radiation BCs-->
    <Rule>
      <Def>ht.boundary.temperature</Def>
      <Def>ht.boundary.flux</Def>
      <Def>ht.boundary.htc</Def>
    </Rule>
    <Rule>
      <Def>ht.boundary.temperature</Def>
      <Def>ht.boundary.flux</Def>
      <Def>ht.boundary.radiation</Def>
    </Rule>
  </Exclusions>

  <!-- View specifications -->
  <Views>
    <View Type="Group" Title="TopLevel" TopLevel="true" TabPosition="North"
      FilterByAdvanceLevel="true" FilterByCategory="false">
      <Views>
        <View Title="Analysis" />
        <View Title="Materials" />
        <View Title="Modules" />
        <View Title="Bodies" />
        <View Title="Outputs" />
      </Views>
    </View>

    <View Type="Group" Title="Analysis" Style="Tiled">
      <Views>
        <View Title="SelectModules" />
        <View Title="Description" />
        <View Title="Mesh" />
        <View Title="General" />
        <View Title="Global Constants" />
      </Views>
    </View>

    <View Type="Instanced" Name="Description">
      <InstancedAttributes>
        <Att Name="description" Type="description" />
      </InstancedAttributes>
    </View>

    <View Type="Analysis" Name="SelectModules" Label="Select Modules"
      AnalysisAttributeName="analysis" AnalysisAttributeType="analysis">
    </View>

    <View Type="Group" Name="Modules" Label="Modules" TabPosition="North">
      <Views>
        <View Title="Enclosure Radiation" />
        <View Title="Fluid Flow" />         <!-- In views/fluid-flow-views.sbt -->
        <View Title="Heat Transfer" />      <!-- In views/heat-transfer-views.sbt -->
        <View Title="Induction Heating" />
        <View Title="Solid Mechanics" />
      </Views>
    </View>

    <View Type="Group" Name="Enclosure Radiation" TabPosition="North">
      <Views>
        <View Title="Enclosures" />
        <View Title="Emissivity" />
        <View Title="Ignore Blocks" />
        <View Title="Enclosure Radiation Functions" />
      </Views>
    </View>
    <View Type="Attribute" Title="Enclosures" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="enclosure" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Emissivity"
      DisplaySearchBox="false"
      RequireAllAssociated="true"
      AvailableLabel="Unassigned"
      CurrentLabel="Assigned"
      AssociationTitle="All enclosure surfaces must be assigned an emissivity value">
      <AttributeTypes>
        <Att Type="emissivity" />
      </AttributeTypes>
    </View>
    <View Type="Associations" Title="Ignore Blocks" Label="Ignore Blocks">
      <AttributeTypes>
        <Att Type="ignore-blocks" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Enclosure Radiation Functions" Label="Functions"  DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="fn.er" />
      </AttributeTypes>
    </View>

    <View Type="Group" Name="Induction Heating" TabPosition="North">
      <Views>
        <View Title="Electromagnetics" />
        <View Title="EM Sources" />
      </Views>
    </View>
    <View Type="Instanced" Title="Electromagnetics">
      <InstancedAttributes>
        <Att Name="electromagnetics" Type="electromagnetics" />
      </InstancedAttributes>
    </View>
    <!-- Using custom view for coils and EM source. -->
    <View Type="smtkTruchasCoilsView" Name="EM Sources">
      <InstancedAttributes>
        <!-- Dont change Name or Type; they are for reference only -->
        <Att Name="induction-heating" Type="induction-heating" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Name="Solid Mechanics">
      <InstancedAttributes>
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Materials" Label="Materials" TabPosition="North">
      <Views>
        <View Title="CustomMaterials" />
        <View Title="Material Functions" />
      </Views>
    </View>

    <!-- Custom view for materials. -->
    <View Type="smtkTruchasMaterialsView" Name="CustomMaterials" Label="Specification">
      <AttributeTypes>
        <!-- Note that smtkTruchasMaterialsView only supports ONE attribute type -->
        <Att Type="material.real">
          <ItemViews>
            <View Path="/shared-properties/enthalpy/specific-enthalpy" ExpressionOnly="true" />
            <View Path="/phases/enthalpy/specific-enthalpy" ExpressionOnly="true" />
            <View Path="/transitions/transition-spec/solid-fraction-table" ExpressionOnly="true" />
            <View Path="/phases/fluid/density-delta" ExpressionOnly="true" />
          </ItemViews>
        </Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Material Functions" Label="Functions" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="fn.material" />
        <Att Type="fn.solid-fraction" />
      </AttributeTypes>
    </View>

    <View Type="Group" Title="Bodies" TabPosition="North">
      <Views>
        <View Title="Body Assignment" />
        <View Title="Initial Condition Functions" />
      </Views>
    </View>
    <View Type="Attribute" Title="Body Assignment"
      DisplaySearchBox="false"
      RequireAllAssociated="true"
      AvailableLabel="Unassigned"
      CurrentLabel="Assigned"
      AssociationTitle="All element blocks must be assigned to a body">
      <AttributeTypes>
        <Att Type="body" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Initial Condition Functions" Label="Functions" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="fn.initial-condition" />
      </AttributeTypes>
    </View>

     <View Type="Group" Title="Outputs" Style="tiled">
      <Views>
        <View Title="Time" />
        <View Title="Probes" />
      </Views>
    </View>
    <View Type="Instanced" Title="Time">
      <InstancedAttributes>
        <Att Name="outputs-att" Type="outputs" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Probes" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="probe" />
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="Mesh" Label="Heat Transfer Mesh">
      <InstancedAttributes>
        <Att Name="Mesh" Type="mesh" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="General" Label="Numerics">
      <InstancedAttributes>
        <Att Name="numerics-att" Type="numerics" />
        <Att Name="simulation-control-att" Type="simulation-control" />
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Globals" Style="Tiled">
      <Views>
        <View Title="Global Constants" />
      </Views>
    </View>

    <View Type="Instanced" Title="Global Constants">
      <InstancedAttributes>
        <Att Name="physics" Type="physics" />
        <Att Name="physical-constants" Type="physical-constants" />
      </InstancedAttributes>
    </View>

  </Views>

</SMTK_AttributeResource>
