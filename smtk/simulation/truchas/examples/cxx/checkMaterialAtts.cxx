//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

// Test program to load attribute resource file and test each material
// attribute for validity.

#include "smtk/simulation/truchas/utility/MaterialAttributeUtils.h"

#include "smtk/attribute/Analyses.h"
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/operators/Read.h"
#include "smtk/io/AttributeReader.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Operation.h"

#include <set>
#include <string>
#include <vector>


int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cout << "\n"
              << "Load attribute resource and check material attributes.\n"
              << "\n"
              << "Usage: TestMaterialAttValidity attribute_filename"
              << "\n"
              << std::endl;
    return -1;
  }

  auto readOp = smtk::attribute::Read::create();
  readOp->parameters()->findFile("filename")->setValue(argv[1]);
  auto opResult = readOp->operate();
  if (opResult->findInt("outcome")->value() !=
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    std::cout << "Failed to read attribute resource file " << argv[1] << std::endl;
    return 1;
  }

  auto attResource = std::dynamic_pointer_cast<smtk::attribute::Resource>(
    opResult->findResource("resource")->value());

  std::vector<smtk::attribute::AttributePtr> attList = attResource->findAttributes("material.real");
  std::cout << "Attribute List Size " << attList.size() << std::endl;

  // Get categories from analysis attribute (if any)
  std::set<std::string> categories;
  auto analysisAtt = attResource->findAttribute("analysis");
  if (analysisAtt != nullptr)
  {
    categories = attResource->analyses().getAnalysisAttributeCategories(analysisAtt);
  }

  smtk::simulation::truchas::MaterialAttributeUtils utils;
  std::string reason;
  for (auto att : attList)
  {
    utils.configureCopiedAttribute(att);
    bool isValid = categories.empty() ? utils.isValid(att) : utils.isValid(att, categories, reason);
    std::string status = isValid ? "yes" : "NO";
    std::cout << att->name() << " is valid? " << status << std::endl;
  }

  return 0;
}
