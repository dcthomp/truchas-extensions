//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_qt_qtTruchasViewRegistrar_h
#define smtk_simulation_truchas_qt_qtTruchasViewRegistrar_h

#include "smtk/simulation/truchas/qt/Exports.h"

#include "smtk/PublicPointerDefs.h"

namespace smtk
{
namespace extension
{
class SMTKTRUCHASQTEXT_EXPORT qtTruchasViewRegistrar
{
public:
  static void registerTo(const smtk::view::ManagerPtr&);
  static void unregisterFrom(const smtk::view::ManagerPtr&);
};
}
}

#endif
