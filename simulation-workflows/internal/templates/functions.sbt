<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Heat Transfer</Cat>
    <Cat>Enclosure Radiation</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Fluid Flow</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>
  <!-- Attribute styles-->
  <Styles>
    <Att Type="fn.er">
      <Style>
        <ItemViews>
          <View Path="/type/tabular-data" ImportFromFile="true" FileFormat="double"></View>
        </ItemViews>
      </Style>
    </Att>
    <Att Type="fn.ff">
      <Style>
        <ItemViews>
          <View Path="/type/tabular-data" ImportFromFile="true" FileFormat="double"></View>
        </ItemViews>
      </Style>
    </Att>
    <Att Type="fn.ht">
      <Style>
        <ItemViews>
          <View Path="/type/tabular-data" ImportFromFile="true" FileFormat="double"></View>
        </ItemViews>
      </Style>
    </Att>
    <Att Type="fn.initial-condition.temperature">
      <Style>
        <ItemViews>
          <View Path="/type/tabular-data" ImportFromFile="true" FileFormat="double"></View>
        </ItemViews>
      </Style>
    </Att>
    <Att Type="fn.material">
      <Style>
        <ItemViews>
          <View Path="/type/tabular-data" ImportFromFile="true" FileFormat="double"></View>
        </ItemViews>
      </Style>
    </Att>
    <Att Type="fn.solid-fraction">
      <ItemViews>
        <View Path="/tabular-data" ImportFromFile="true" FileFormat="double"></View>
      </ItemViews>
    </Att>
  </Styles>
  <!-- Attribute definitions-->
  <Definitions>
    <AttDef Type="fn.material" BaseType="" Abstract="true" Version="0"></AttDef>
    <AttDef Type="fn.ff" BaseType="" Abstract="true" Version="0">
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
    </AttDef>
    <AttDef Type="fn.ht" BaseType="" Abstract="true" Version="0">
      <Categories>
        <Cat>Heat Transfer</Cat>
      </Categories>
    </AttDef>
    <AttDef Type="fn.initial-condition" BaseType="" Abstract="true" Version="0"></AttDef>
    <AttDef Type="fn.er" BaseType="" Abstract="true" Version="0">
      <Categories>
        <Cat>Enclosure Radiation</Cat>
      </Categories>
    </AttDef>
    <AttDef Type="fn.material.conductivity" BaseType="fn.material" Label="Conductivity" RootName="ConductivityFn" Version="0">
      <Categories>
        <Cat>Heat Transfer</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Conductivity" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1"></Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.material.density-delta" BaseType="fn.material" Label="Density Delta" RootName="DensityDeviationFn" Version="0">
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Density Deviation" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1"></Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.material.electrical-conductivity" BaseType="fn.material" Label="Electrical Conductivity" RootName="ElectricalConductivityFn" Version="0">
      <Categories>
        <Cat>Induction Heating</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Electrical Conductivity" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1"></Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.material.specific-enthalpy" BaseType="fn.material" Label="Specific Enthalpy" RootName="SpecificEnthalpyFn" Version="0">
      <Categories>
        <Cat>Heat Transfer</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Specific Enthalpy" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1"></Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.material.specific-heat" BaseType="fn.material" Label="Specific Heat" RootName="SpecificHeatFn" Version="0">
      <Categories>
        <Cat>Heat Transfer</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Specific Heat" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1"></Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.material.viscosity" BaseType="fn.material" Label="Viscosity" RootName="ViscosityFn" Version="0">
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Viscosity" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1"></Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.solid-fraction" BaseType="" Label="Solid Fraction" RootName="SolidFractionFn">
      <Categories>
        <Cat>Heat Transfer</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="tabular-data" Label="Data" Extensible="true" NumberOfRequiredGroups="2">
          <ItemDefinitions>
            <Double Name="X" Label="Temperature" NumberOfRequiredValues="1"></Double>
            <Double Name="Value" Label="Solid Fraction [0.0-1.0]" NumberOfRequiredValues="1">
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ff.pressure" Label="Pressure" BaseType="fn.ff" RootName="PressureFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Pressure" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ff.velocity" BaseType="fn.ff" Label="Velocity" RootName="VelocityFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Time" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Velocity" NumberOfRequiredValues="3"></Double>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ht.temperature" Label="Temperature" BaseType="fn.ht" RootName="TemperatureFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Temperature" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ht.flux" Label="Flux" BaseType="fn.ht" RootName="FluxFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Flux" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ht.htc" Label="HTC" BaseType="fn.ht" RootName="HTCFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="HTC" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ht.ambient-temperature" Label="Ambient Temp" BaseType="fn.ht" RootName="AmbientTemperatureFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Ambient Temp" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ht.emissivity" Label="Emissivity" BaseType="fn.ht" RootName="EmissivityFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Emissivity" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.initial-condition.temperature" BaseType="fn.initial-condition" Label="Temperature" RootName="InitialTempFn" Version="0">
      <Categories>
        <Cat>Fluid Flow</Cat> 
        <Cat>Heat Transfer</Cat> 
        <Cat>Solid Mechanics</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="x">1</Value>
                <Value Enum="y">2</Value>
                <Value Enum="z">3</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Temperature" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (x,y,z)" NumberOfRequiredValues="3">
              <ComponentLabels>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(x, y, z)" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (x,y,z)" NumberOfRequiredValues="3">
                  <ComponentLabels>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.er.ambient-temperature" Label="Ambient Temp" BaseType="fn.er" RootName="AmbientTemperatureFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Ambient Temp" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.er.emissivity" Label="Emissivity" BaseType="fn.er" RootName="EmissivityFn">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Int Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">1</Value>
                <Value Enum="x">2</Value>
                <Value Enum="y">3</Value>
                <Value Enum="z">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="2">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Emissivity" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <ComponentLabels>
                <Label>t</Label>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents (t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
