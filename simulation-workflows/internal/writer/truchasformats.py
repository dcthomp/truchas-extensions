# ---------------------------------------------------------------------
#
# Dictionary of formatters for Truchas output lines
# Arguments are: (keyword, attribute type, item path, **kwargs)
#
# ---------------------------------------------------------------------
import os
print('loading', os.path.basename(__file__))

from .cardformat import CardFormat as card
from .namelist import Namelist as namelist

# Predefined strings for "if_condition" arguments
# ---------------------------------------------------------------------
ENCLOSURE_RADIATION = 'Enclosure Radiation'
FLOW_ANALYSIS = 'Fluid Flow'
INDUCTION_HEATING = 'Induction Heating'
THERMAL_ANALYSIS = 'Heat Transfer'

ONLY_THERMAL_ANALYSIS = 'only-thermal-analysis'
INVISCID_FLOW = 'inviscid_flow'
VISCOUS_FLOW = 'viscous-flow'
BC_INFLOW = 'bc-inflow'


class key_card(card):
    """Shorthand for cards where keyword matches the item name and path."""

    def __init__(self, keyword, **kwargs):
        super(key_card, self).__init__(keyword, item_path=keyword, **kwargs)


# Please order the namelists alphabetically in this table
# ---------------------------------------------------------------------
format_table = {
    'ALTMESH': [
        card('altmesh_file', item_path='model', is_custom=True),
        card('altmesh_coordinate_scale_factor',
             item_path='coordinate-scale-factor'),
    ],
    'BODY': [
        card('surface_name', literal_value='from mesh file'),
        card('mesh_material_number', use_model_entities_for_value=True),
        card('material_name', is_custom=True),
        # temp for versions 0 and 1, Todo update CardFormat to check version
        card('temperature', item_path='temperature',
             expression_keyword='temperature_function'),
        card('temperature', item_path='material/temperature',
             expression_keyword='temperature_function'),
        card('velocity', item_path='velocity', if_condition=FLOW_ANALYSIS),
        card('velocity', item_path='material/velocity',
             if_condition=FLOW_ANALYSIS)
    ],
    'DIFFUSION_SOLVER': [
        card('stepping_method', literal_value='Adaptive BDF2',
             if_condition=ONLY_THERMAL_ANALYSIS),
        card('stepping_method', literal_value='Non-adaptive BDF1',
             if_condition=FLOW_ANALYSIS),

        card('abs_enthalpy_tol', item_path='abs-enthalpy-tol',
             if_condition=ONLY_THERMAL_ANALYSIS),
        card('rel_enthalpy_tol', item_path='rel-enthalpy-tol',
             if_condition=ONLY_THERMAL_ANALYSIS),
        card('abs_temp_tol', item_path='abs-temperature-tol',
             if_condition=ONLY_THERMAL_ANALYSIS),
        card('rel_temp_tol', item_path='rel-temperature-tol',
             if_condition=ONLY_THERMAL_ANALYSIS),

        card('residual_atol', item_path='residual-atol',
             if_condition=FLOW_ANALYSIS),
        card('residual_rtol', item_path='residual-rtol',
             if_condition=FLOW_ANALYSIS),
        card('nlk_preconditioner', item_path='nlk-preconditioner',
             literal_value='Hypre_AMG'),
        card('max_nlk_itr', item_path='max-nlk-itr'),
        card('max_nlk_vec', item_path='max-nlk-vec'),
        card('nlk_tol', item_path='nlk-tol'),
        card('nlk_vec_tol', item_path='nlk-vec-tol'),
        card('verbose_stepping', item_path='verbose-stepping'),
        card('pc_amg_cycles', item_path='pc-amg-cycles'),
        card('cond_vfrac_threshold', item_path='cond-vfrac-threshold',
             if_condition=FLOW_ANALYSIS)
    ],
    'DS_SOURCE': [
        card('equation', literal_value='temperature'),
        card('cell_set_ids', use_model_entities_for_value=True),
        card('source_constant', item_path='source',
             expression_keyword='source_function')
    ],
    'ELECTROMAGNETICS': [
        card('em_domain_type', item_path='em-domain-type'),
        card('symmetry_axis', item_path='symmetry-axis'),
        # Note: item_path for time & frequency are actuall item names under 'source' group item
        card('source_times', att_type='induction-heating', item_path='time'),
        card('source_frequency', att_type='induction-heating',
             item_path='frequency'),
        # card('uniform_source', att_type='em-source', item_path='source-type/constant-uniform-source'),
        card('cg_stopping_tolerance', item_path='cg-stopping-tolerance'),
        card('maximum_cg_iterations', item_path='maximum-cg-iterations'),
        card('num_etasq', item_path='num-etasq'),
        card('steps_per_cycle', item_path='steps-per-cycle'),
        card('ss_stopping_tolerance', item_path='ss-stopping-tolerance'),
        card('maximum_source_cycles', item_path='maximum-source-cycles'),
        card('output_level', item_path='output-level')
    ],
    'ENCLOSURE_RADIATION': [
        card('name', use_name_for_value=True),
        card('enclosure_file', item_path='enclosure_file'),
        # card('coord_scale_factor', att_type='mesh', item_path='coordinate-scale-factor'),
        card('skip_geometry_check', item_path='skip_geometry_check', as_boolean=True),
        card('ambient_constant', item_path='ambient-temperature',
             expression_keyword='ambient_function'),
        card('error_tolerance', item_path='error_tolerance'),
        card('precon_method', item_path='precon_method'),
        card('precon_iter', item_path='precon_iter'),
        card('precon_coupling_method', item_path='precon_coupling_method')
    ],
    'ENCLOSURE_SURFACE': [
        # name and enclosure_name handled in custom method
        card('face_block_ids', use_model_entities_for_value=True),
        card('emissivity_constant', item_path='emissivity',
             expression_keyword='emissivity_function')
    ],
    'FLOW': [
        card('inviscid', use_condition_for_boolean=INVISCID_FLOW),
        key_card('courant_number'),
        key_card('viscous_number'),
        card('viscous_implicitness', is_custom=True, att_type='viscous-linear-solver',
             item_path='implicit-time-stepping/viscous_implicitness'),
        key_card('track_interfaces', as_boolean=True,
                 att_type='material-priority'),
        card('material_priority', att_type='material-priority',
             item_path='track_interfaces/material_priority'),
        key_card('vol_track_subcycles'),
        key_card('nested_dissection'),
        key_card('vol_frac_cutoff'),
        key_card('fischer_dim'),
        key_card('fluid_frac_threshold'),
        key_card('min_face_fraction'),
        key_card('void_collapse', as_boolean=True),
        card('void_collapse_relaxation',
             item_path='void_collapse/void_collapse_relaxation'),
        key_card('wisp_redistribution', as_boolean=True),
        card('wisp_cutoff', item_path='wisp_redistribution/wisp_cutoff'),
        card('wisp_neighbor_cutoff',
             item_path='wisp_redistribution/wisp_neighbor_cutoff'),
    ],
    'FLOW_BC': [
        card('name', use_name_for_value=True),
        card('face_set_ids', use_model_entities_for_value=True),
        card('type', is_custom=True),
        key_card('pressure', expression_keyword='pressure_func'),
        key_card('velocity', expression_keyword='velocity_func'),
        key_card('dsigma'),
        card('inflow_material', item_path='material', is_custom=True),
        key_card('inflow_temperature'),
    ],
    # FLOW_PRESSURE_SOLVER and FLOW_VISCOUS_SOLVER inserted below
    'FUNCTION': [
        card('name', use_name_for_value=True),
        card('type', item_path='type')
    ],
    'INDUCTION_COIL': [
        # Note that item_path is actually the item name under the extensible "coils" group item
        card('nturns', item_path='nturns'),
        card('center', item_path='center'),
        card('radius', item_path='radius'),
        card('length', item_path='length'),
        card('current', item_path='current')
    ],
    'LINEAR_SOLVER': [
        card('name', item_path='', use_name_for_value=True),
        card('method', item_path='method'),
        card('preconditioning_method', item_path='preconditioning-method'),
        card('preconditioning_steps', item_path='preconditioning-steps'),
        card('relaxation_parameter', item_path='relaxation-parameter'),
        card('stopping_criterion', item_path='stopping-criterion'),
        card('convergence_criterion', item_path='convergence-criterion'),
        card('krylov_vectors', item_path='krylov-vectors'),
        card('output_mode', item_path='output-mode')
    ],
    # Writer for MATERIAL namelist is in materialwriter.py
    # Writer for MATERIAL_SYSTEM namelist is in materialwriter.py
    'MESH': [
        card('mesh_file', is_custom=True),
        card('coordinate_scale_factor', item_path='coordinate-scale-factor'),
        card('exodus_block_modulus', item_path='exodus-block-modulus'),
        card('interface_side_sets', is_custom=True)
    ],
    'NUMERICS': [
        card('dt_init', att_type='numerics', item_path='dt_init'),
        card('dt_grow', att_type='numerics', item_path='dt_grow'),
        card('dt_max', att_type='numerics', item_path='dt_max'),
        card('dt_min', att_type='numerics', item_path='dt_min'),
        card('t', att_type='outputs', item_path='start-time'),
    ],
    'OUTPUTS': [
        # Writer has custom code for output time lists
        card('output_t', item_path='output-times/time'),
        card('output_dt', item_path='output-times/time')
    ],
    # Writer for PHASE namelist is in materialwriter.py
    'PHYSICAL_CONSTANTS': [
        card('absolute_zero', item_path='absolute-zero'),
        card('stefan_boltzmann', item_path='stefan-boltzmann')
    ],
    'PHYSICS': [
        card('body_force_density', att_type='physics', item_path='body_force_density',
             if_condition=FLOW_ANALYSIS),
        card('electromagnetics', use_condition_for_boolean=INDUCTION_HEATING),
        card('flow', use_condition_for_boolean=FLOW_ANALYSIS),
        card('heat_transport', use_condition_for_boolean=THERMAL_ANALYSIS),
    ],
    'PROBE': [
        card('data', item_path='data'),
        card('data_file', item_path='data-file'),
        card('coord', item_path='coord'),
        card('coord_scale_factor', item_path='coord-scale-factor'),
        card('description', item_path='description'),
        card('digits', item_path='digits')
    ],
    'SIMULATION_CONTROL': [
        card('phase_start_times', item_path='simulation-control/phase-start-times'),
        card('phase_init_dt_factor',
             item_path='simulation-control/phase-init-dt-factor')
    ],
    'THERMAL_BC': [],
    'VFUNCTION': [
        card('name', use_name_for_value=True),
        card('type', item_path='type')
    ],
}

# Flow solver namelists are almost the same
flow_solver_namelist = [
    key_card('krylov_method'),
    card('krylov_dim', item_path='krylov_method/krylov_dim'),
    key_card('conv_rate_tol'),
    key_card('abs_tol'),
    key_card('rel_tol'),
    key_card('max_ds_iter'),
    card('max_amg_iter', item_path='max_pcg_iter'),
    card('print_level', item_path='verbose-output', is_custom=True),
    # card('cg_use_two_norm', item_path='hypre/cg_use_two_norm'),
    card('amg_strong_threshold', item_path='hypre/amg_strong_threshold'),
    card('amg_max_levels', item_path='hypre/amg_max_levels'),
    card('amg_coarsen_method', item_path='hypre/amg_coarsen_method'),
    card('amg_smoothing_sweeps', item_path='hypre/amg_smoothing_sweeps'),
    card('amg_smoothing_method', item_path='hypre/amg_smoothing_method'),
    card('amg_interp_method', item_path='hypre/amg_interp_method'),
]
format_table['FLOW_PRESSURE_SOLVER'] = flow_solver_namelist
format_table['FLOW_VISCOUS_SOLVER'] = flow_solver_namelist


# This list sets the order that namelists are written to the Truchas input file
# ---------------------------------------------------------------------
namelist_sequence = [
    namelist('MESH', att_type='mesh', custom_method='_write_mesh'),
    namelist('ALTMESH', if_condition=INDUCTION_HEATING, att_type='electromagnetics',
             custom_method='_write_mesh'),

    namelist('PHYSICS', custom_method='_write_physics'),
    namelist('DIFFUSION_SOLVER',
             if_condition=ONLY_THERMAL_ANALYSIS,
             att_type='ht.solver',
             att_version=0,
             base_item_path='analysis/thermal'),
    namelist('DIFFUSION_SOLVER',
             if_condition=[THERMAL_ANALYSIS, FLOW_ANALYSIS],
             att_type='ht.solver',
             att_version=0,
             base_item_path='analysis/thermal-plus-fluid/thermal-solver'),
    namelist('DIFFUSION_SOLVER',
             att_type='ht.solver.standard',
             att_version=1),
    namelist('DIFFUSION_SOLVER',
             att_type='ht.solver.second',
             att_version=1),
    namelist('NUMERICS', att_type='numerics'),
    namelist('OUTPUTS', att_type='outputs', custom_method='_write_outputs'),
    namelist('PHYSICAL_CONSTANTS', att_type='physical-constants'),

    # Writer for MATERIAL/MATERIAL_SYSTEM/PHASE namelists is in materialwriter.py
    namelist('MATERIAL', custom_method='_write_materials',
             separator='MATERIALS'),
    namelist('FUNCTION', att_type='fn.material',
             custom_method='_write_function'),
    namelist('ENCLOSURE_RADIATION', att_type='enclosure', separator='ENCLOSURE RADIATION',
             custom_method='_write_enclosure'),
    namelist('FUNCTION', att_type='fn.er', custom_method='_write_function'),

    namelist('ELECTROMAGNETICS', if_condition=INDUCTION_HEATING, att_type='electromagnetics',
             custom_method='_write_electromagnetics', separator='INDUCTION HEATING'),
    namelist('INDUCTION_COIL', if_condition=INDUCTION_HEATING, att_type='induction-heating',
             custom_method='_write_induction_coils'),
    namelist(
        'THERMAL_BC',
        att_type='ht.boundary',
        custom_method='_write_thermal_condition',
        separator='BOUNDARY CONDITIONS'),
    namelist(
        'THERMAL_BC',
        att_type='ht.interface',
        custom_method='_write_thermal_condition',
        separator='INTERFACE CONDITIONS'),
    namelist('FUNCTION', att_type='fn.ht', custom_method='_write_function'),

    namelist('FLOW', att_type='flow-numerics', separator='FLOW',
             custom_method='_write_flow_solver'),
    namelist('FLOW_PRESSURE_SOLVER', att_type='pressure-linear-solver',
             custom_method='_write_flow_solver'),
    namelist('FLOW_VISCOUS_SOLVER', att_type='viscous-linear-solver',
             base_item_path='implicit-time-stepping', custom_method='_write_flow_solver'),
    namelist('FLOW_BC', att_type='ff.boundary',
             custom_method='_write_flow_bc'),
    namelist('FUNCTION', att_type='fn.ff.temperature',
             custom_method='_write_function'),
    namelist('VFUNCTION', att_type='fn.ff.velocity',
             custom_method='_write_vector_function'),

    namelist('BODY', att_type='body',
             custom_method='_write_body', separator='BODIES'),
    namelist('FUNCTION', att_type='fn.initial-condition',
             custom_method='_write_function'),
    namelist('DS_SOURCE', att_type='ht.source'),
    namelist('PROBE', att_type='probe', separator='PROBES'),
    namelist(
        'SIMULATION_CONTROL',
        att_type='simulation-control',
        custom_method='_write_simcontrol')
]

test_sequence = [
    namelist('MATERIAL', custom_method='_write_materials',
             separator='MATERIALS'),
    namelist('FUNCTION', att_type='fn.material',
             custom_method='_write_function')
]
