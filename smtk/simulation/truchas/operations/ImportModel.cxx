//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/operations/ImportModel.h"

// Project includes
#include "smtk/simulation/truchas/operations/ImportModel_xml.h"
#include "smtk/simulation/truchas/Metadata.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Project.h"

#include <boost/filesystem.hpp>

#include <iostream>
#include <string>

namespace
{
const int OP_SUCCESS = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

// Macro for returning on error
#define errorMacro(msg)                                                                            \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), msg);                                                              \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

} // namespace

namespace smtk
{
namespace simulation
{
namespace truchas
{

smtk::operation::Operation::Result ImportModel::operateInternal()
{
  auto& logger = this->log();

  // Get the project
  auto refItem = this->parameters()->associations();
  auto project = refItem->valueAs<smtk::project::Project>();
  if (project == nullptr)
  {
    errorMacro("No project associated to thie ImportModel operator.");
  }

  Metadata mdata;
  mdata.getFromResource(project);

  // Get copy flag
  bool copyNativeFiles = this->parameters()->find("copy-file")->isEnabled();
  boost::filesystem::path copyPath; // file system path for native model copy
  if (copyNativeFiles)
  {
    // Make sure assets folder exists
    boost::filesystem::path smtkPath(project->location());
    boost::filesystem::path projectPath = smtkPath.parent_path();
    boost::filesystem::path assetsPath = projectPath / "assets";

    if (!boost::filesystem::exists(assetsPath))
    {
      bool created = boost::filesystem::create_directories(assetsPath);
      if (!created)
      {
        errorMacro("Failed to create assets directory: " << assetsPath.string() << ".");
      }
    }

    auto fileItem = this->parameters()->findFile("induction-heating-mesh");
    if (!fileItem->isSet())
    {
      errorMacro("Error: the induction-heating-mesh file item is not set");
    }
    std::string location = fileItem->value();
    boost::filesystem::path nativePath(location);
    if (!boost::filesystem::exists(nativePath))
    {
      errorMacro("Error: import file not found " << location);
    }

    // Copy the mesh file to project directory
    copyPath = assetsPath / nativePath.filename();
    boost::filesystem::copy_file(nativePath, copyPath);
    mdata.NativeInductionHeatingMeshLocation = copyPath.string();
  } // if (copyNativeFiles)

  // Import the model resource
  auto importOp = this->manager()->create("smtk::session::vtk::Import");
  if (importOp == nullptr)
  {
    errorMacro("Failed to create smtk::session::vtk::Import operator.");
  }
  importOp->parameters()->findFile("filename")->setValue(mdata.NativeInductionHeatingMeshLocation);
  auto importOpResult = importOp->operate();
  int outcome = importOpResult->findInt("outcome")->value(0);
  if (outcome != OP_SUCCESS)
  {
    smtkErrorMacro(logger, "Error importing file " << mdata.NativeInductionHeatingMeshLocation);
    // Remove the copied asset
    if (!copyPath.empty())
    {
      boost::filesystem::remove(copyPath);
    }
    mdata.NativeInductionHeatingMeshLocation = "";
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }
  auto modelResource = importOpResult->findResource("resource")->value(0);
  if (!project->resources().add(modelResource, Metadata::INDUCTION_HEATING_MESH_ROLE))
  {
    smtkErrorMacro(logger, "Failed to add model to project.");
    if (!copyPath.empty())
    {
      boost::filesystem::remove(copyPath);
    }
    mdata.NativeInductionHeatingMeshLocation = "";
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Update meta data and we're done
  mdata.putToResource(project);
  return this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
}

smtk::operation::Operation::Specification ImportModel::createSpecification()
{
  Specification spec = this->smtk::operation::XMLOperation::createSpecification();
  return spec;
}

const char* ImportModel::xmlDescription() const
{
  return ImportModel_xml;
}
}
}
}
