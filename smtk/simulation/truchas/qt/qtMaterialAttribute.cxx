//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/qt/qtMaterialAttribute.h"

// Truchas Extension headers
#include "smtk/simulation/truchas/qt/qtMaterialItem.h"
#include "smtk/simulation/truchas/qt/qtSharedPropertiesItem.h"
#include "smtk/simulation/truchas/utility/MaterialAttributeUtils.h"

// SMTK headers
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/SearchStyle.h"
#include "smtk/extension/qt/qtBaseAttributeView.h"
#include "smtk/extension/qt/qtBaseView.h"
#include "smtk/extension/qt/qtGroupItem.h"
#include "smtk/extension/qt/qtInputsItem.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/view/Configuration.h"

#include <QDebug>
#include <QFrame>
#include <QLabel>
#include <QList>
#include <QPointer>
#include <QSpacerItem>
#include <QVBoxLayout>

#include <map>
#include <set>
#include <string>

namespace att = smtk::attribute;
namespace ext = smtk::extension;

class qtMaterialAttributeInternals
{
public:
  // Member data
  att::WeakAttributePtr m_attribute;
  QPointer<QWidget> m_parentWidget;
  QList<ext::qtItem*> m_items;
  ext::qtBaseAttributeView* m_view;
  smtk::view::Configuration::Component m_attComp;
  std::map<std::string, ext::qtAttributeItemInfo> m_itemViewMap;
  smtk::simulation::truchas::MaterialAttributeUtils m_utils;

  qtMaterialItem* m_materialItem;
  qtSharedPropertiesItem* m_sharedPropertiesItem;

  // Methods
  qtMaterialAttributeInternals(att::AttributePtr myAttribute,
    const smtk::view::Configuration::Component& comp, QWidget* p, ext::qtBaseAttributeView* myView)
    : m_parentWidget(p)
    , m_attribute(myAttribute)
    , m_view(myView)
    , m_attComp(comp)
    , m_materialItem(nullptr)
    , m_sharedPropertiesItem(nullptr)
  {
    // Check for ItemViews
    int sindex = comp.findChild("ItemViews");
    if (sindex == -1)
    {
      return;
    }
    auto iviews = m_attComp.child(sindex);
    ext::qtAttributeItemInfo::buildFromComponent(iviews, m_view, m_itemViewMap);
  }

  ~qtMaterialAttributeInternals() {}
};

qtMaterialAttribute::qtMaterialAttribute(att::AttributePtr myAttribute,
  const smtk::view::Configuration::Component& comp, QWidget* p, ext::qtBaseView* myView)
{
  auto theView = dynamic_cast<ext::qtBaseAttributeView*>(myView);
  m_internals = new qtMaterialAttributeInternals(myAttribute, comp, p, theView);
  m_widget = NULL;
  m_useSelectionManager = false;
  this->createWidget();
  m_isEmpty = true;
}

qtMaterialAttribute::~qtMaterialAttribute()
{
  // First Clear all the items
  for (int i = 0; i < m_internals->m_items.count(); i++)
  {
    delete m_internals->m_items.value(i);
  }

  m_internals->m_items.clear();
  if (m_widget)
  {
    delete m_widget;
  }

  delete m_internals;
}

void qtMaterialAttribute::createWidget()
{
  // Initially there are no items being displayed
  m_isEmpty = true;
  auto att = this->attribute();
  if ((att == nullptr) || ((att->numberOfItems() == 0) && (att->associations() == nullptr)))
  {
    return;
  }

  int numShowItems = 0;
  std::size_t i, n = att->numberOfItems();
  if (m_internals->m_view)
  {
    for (i = 0; i < n; i++)
    {
      if (m_internals->m_view->displayItem(att->item(static_cast<int>(i))))
      {
        numShowItems++;
      }
    }
    // also check associations
    if (m_internals->m_view->displayItem(att->associations()))
    {
      numShowItems++;
    }
  }
  else // show everything
  {
    numShowItems = static_cast<int>(att->associations() ? n + 1 : n);
  }
  if (numShowItems == 0)
  {
    return;
  }
  m_isEmpty = false;
  QFrame* attFrame = new QFrame(this->parentWidget());
  attFrame->setFrameShape(QFrame::Box);
  m_widget = attFrame;

  QVBoxLayout* layout = new QVBoxLayout(m_widget);
  layout->setMargin(3);
  m_widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
}

void qtMaterialAttribute::addItem(ext::qtItem* child)
{
  if (!m_internals->m_items.contains(child))
  {
    m_internals->m_items.append(child);
    QObject::connect(child, &ext::qtItem::modified, this, &qtMaterialAttribute::onItemModified);
  }
}

QList<ext::qtItem*>& qtMaterialAttribute::items() const
{
  return m_internals->m_items;
}

void qtMaterialAttribute::showAdvanceLevelOverlay(bool show)
{
  for (int i = 0; i < m_internals->m_items.count(); i++)
  {
    m_internals->m_items.value(i)->showAdvanceLevelOverlay(show);
  }
}

void qtMaterialAttribute::createBasicLayout(bool includeAssociations)
{
  // Initially we have not displayed any items
  m_isEmpty = true;
  // If there is no main widget there is nothing to show
  if (!m_widget)
  {
    return;
  }
  QLayout* layout = m_widget->layout();
  att::AttributePtr att = this->attribute();
  auto uiManager = m_internals->m_view->uiManager();

  // Create widget for invariant items (same for all phases)
  auto invariantGroup = att->findGroup("invariant-properties");
  if (invariantGroup != nullptr) // not in older versions
  {
    // Add widget to contain invariant items, mostly for vertical
    // alignment with rest of widgets in the panel (in lieu of group
    // item widget).
    QWidget* marginWidget = new QWidget(m_widget);
    QLayout* marginLayout = new QVBoxLayout(marginWidget);
    for (std::size_t i = 0; i < invariantGroup->numberOfItemsPerGroup(); ++i)
    {
      auto item = invariantGroup->item(i);
      smtk::view::Configuration::Component comp;
      ext::qtAttributeItemInfo info(item, comp, m_widget, m_internals->m_view);
      auto invarQItem = ext::qtInputsItem::createItemWidget(info);
      this->addItem(invarQItem);
      QWidget* widget = invarQItem->widget();
      if (widget != nullptr)
      {
        marginLayout->addWidget(widget);
      }
    }
    marginWidget->setLayout(marginLayout);
    layout->addWidget(marginWidget);
  }

  std::size_t phaseCount = 0;
  // Get "phases" and "transitions" group items,
  // which are displayed after everything else
  att::GroupItemPtr phasesItem = att->findGroup("phases");
  if (phasesItem == nullptr)
  {
    QString msg("Cannot display phase/transition table; missing \"phases\" item");
    qWarning() << msg;
    auto* label = new QLabel(msg, m_widget);
    layout->addWidget(label);
    return;
  }
  phaseCount = phasesItem->numberOfGroups();

  att::GroupItemPtr transitionsItem = att->findGroup("transitions");
  if (transitionsItem == nullptr)
  {
    QString msg("Cannot display phase/transition table; missing \"transitions\" item");
    qWarning() << msg;
    auto label = new QLabel(msg, m_widget);
    layout->addWidget(label);
    return;
  }

  qtSharedPropertiesItem* qSharedProperties = nullptr;
  qtMaterialItem* qMaterial = nullptr;

  // Handle shared-properties group item
  att::GroupItemPtr sharedPropertiesItem = att->findGroup("shared-properties");
  if (sharedPropertiesItem == nullptr)
  {
    QString msg("Missing \"shared-properties\" item");
    qWarning() << msg;
    auto* label = new QLabel(msg, m_widget);
    layout->addWidget(label);
  }
  else
  {
    auto sharedIter = m_internals->m_itemViewMap.find(sharedPropertiesItem->name());
    if (sharedIter == m_internals->m_itemViewMap.end())
    {
      smtk::view::Configuration::Component comp;
      ext::qtAttributeItemInfo sharedInfo(
        sharedPropertiesItem, comp, m_widget, m_internals->m_view);
      qSharedProperties = new qtSharedPropertiesItem(sharedInfo, this);
    }
    else
    {
      auto sharedInfo = sharedIter->second;
      auto item = std::static_pointer_cast<att::Item>(sharedPropertiesItem);
      sharedInfo.setItem(item);
      sharedInfo.setParentWidget(m_widget);
      qSharedProperties = new qtSharedPropertiesItem(sharedInfo, this);
    }

    if (qSharedProperties && qSharedProperties->widget())
    {
      m_isEmpty = false;
      layout->addWidget(qSharedProperties->widget());
      this->addItem(qSharedProperties);
      QObject::connect(
        qSharedProperties, &ext::qtItem::modified, this, &qtMaterialAttribute::onItemModified);
      qSharedProperties->onPhaseCountChanged(); // sets visibility
    }
  }

  // Handle phase/transition sequence
  qtItemInfo phasesInfo;
  auto it = m_internals->m_itemViewMap.find(phasesItem->name());
  if (it != m_internals->m_itemViewMap.end())
  {
    phasesInfo = it->second;
    phasesInfo.setParentWidget(m_widget);
    // Cast to base item before calling setItem (otherwise compiler error)
    auto item = std::static_pointer_cast<att::Item>(phasesItem);
    phasesInfo.setItem(item);
  }
  else
  {
    smtk::view::Configuration::Component comp;
    phasesInfo = ext::qtAttributeItemInfo(phasesItem, comp, m_widget, m_internals->m_view);
  }

  qtItemInfo transitionsInfo;
  it = m_internals->m_itemViewMap.find(transitionsItem->name());
  if (it != m_internals->m_itemViewMap.end())
  {
    transitionsInfo = it->second;
    transitionsInfo.setParentWidget(m_widget);
    // Cast to base item before calling setItem (otherwise compiler error)
    auto item = std::static_pointer_cast<att::Item>(transitionsItem);
    transitionsInfo.setItem(item);
  }
  else
  {
    smtk::view::Configuration::Component comp;
    transitionsInfo = ext::qtAttributeItemInfo(phasesItem, comp, m_widget, m_internals->m_view);
  }

  qMaterial = new qtMaterialItem(phasesInfo, transitionsInfo, this);
  if (qMaterial && qMaterial->widget())
  {
    m_isEmpty = false;
    layout->addWidget(qMaterial->widget());
    this->addItem(qMaterial);
  }

  if (qMaterial && qSharedProperties)
  {
    QObject::connect(qSharedProperties, &qtSharedPropertiesItem::itemEnabledStateChanged, this,
      &qtMaterialAttribute::onSharedStateChanged);
  }
  m_internals->m_materialItem = qMaterial;
  m_internals->m_sharedPropertiesItem = qSharedProperties;

  QObject::connect(
    m_internals->m_materialItem, &qtMaterialItem::addPhase, this, &qtMaterialAttribute::onAddPhase);
  QObject::connect(m_internals->m_materialItem, &qtMaterialItem::removePhase, this,
    &qtMaterialAttribute::onRemovePhase);

  if (phaseCount == 1)
  {
    // Add spacer item to push all fields up
    QVBoxLayout* boxLayout = dynamic_cast<QVBoxLayout*>(layout);
    boxLayout->addStretch(1);
  }

  m_isEmpty = false;
}

att::AttributePtr qtMaterialAttribute::attribute()
{
  return m_internals->m_attribute.lock();
}

QWidget* qtMaterialAttribute::parentWidget()
{
  return m_internals->m_parentWidget;
}

void qtMaterialAttribute::onAddPhase()
{
  auto att = m_internals->m_attribute.lock();
  m_internals->m_utils.addPhase(att);
  this->updateItemData();
}

void qtMaterialAttribute::onRemovePhase(int phaseNumber)
{
  auto att = m_internals->m_attribute.lock();
  m_internals->m_utils.removePhase(att, phaseNumber);
  this->updateItemData();
}

void qtMaterialAttribute::updateItemData()
{
  m_internals->m_materialItem->updateItemData();
  m_internals->m_sharedPropertiesItem->updateItemData();

  // Check if last layout item is a spacer
  QSpacerItem* spacer = nullptr;
  QVBoxLayout* layout = dynamic_cast<QVBoxLayout*>(m_widget->layout());
  int lastIndex = layout->count() - 1;
  if (lastIndex >= 0)
  {
    spacer = dynamic_cast<QSpacerItem*>(layout->itemAt(lastIndex));
  }

  // Include spacer for single-phase case
  // (Is there an easier way to do this in Qt?)
  auto phasesGroup = this->attribute()->findGroup("phases");
  if (phasesGroup->numberOfGroups() == 1 && spacer == nullptr)
  {
    layout->addStretch(1);
  }
  else if (phasesGroup->numberOfGroups() > 1 && spacer != nullptr)
  {
    layout->removeItem(spacer);
    delete spacer;
  }

  // For notifying view(s)
  emit this->modified();
}

/* Slot for properly emitting signals when an attribute's item is modified */
void qtMaterialAttribute::onItemModified()
{
  // are we here due to a signal?
  QObject* sobject = this->sender();
  if (sobject == nullptr)
  {
    return;
  }
  auto iobject = qobject_cast<ext::qtItem*>(sobject);
  if (iobject == nullptr)
  {
    return;
  }

  emit this->itemModified(iobject);
}

void qtMaterialAttribute::onSharedStateChanged(QString itemName, bool isEnabled)
{
  // Get attribute and shared item
  auto att = m_internals->m_attribute.lock();
  auto sharedGroup = att->findGroup("shared-properties");
  auto sharedItem = sharedGroup->find(itemName.toStdString());

  // Update attribute and view
  m_internals->m_utils.enableSharedItem(sharedItem, isEnabled);
  m_internals->m_materialItem->updateItemData();
}

bool qtMaterialAttribute::isEmpty() const
{
  return m_isEmpty;
}
