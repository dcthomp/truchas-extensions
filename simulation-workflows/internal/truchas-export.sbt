<SMTK_AttributeResource Version="4">
  <Definitions>
    <AttDef Type="export" BaseType="operation" Label="Export to Truchas" Version="2">
      <BriefDescription>
        Write Truchas input file.
      </BriefDescription>
      <DetailedDescription>
        Using the specified model and simulation attribute resources, this
        operation will write a Truchas input file to the specified location.
      </DetailedDescription>
      <ItemDefinitions>
        <String Name="analysis-code" Label="Analysis Code">
          <ChildrenDefinitions>
            <Directory Name="output-folder" Label="Output Folder">
            </Directory>
            <File Name="output-file" Label="Output File"
              FileFilters="Input files (*.inp);;All files (*.*)" Version="0">
              <BriefDescription>Truchas file to be generated</BriefDescription>
            </File>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value>Genre</Value>
              <Items>
                <Item>output-folder</Item>
              </Items>
            </Structure>
            <Structure>
              <Value>Truchas</Value>
              <Items>
                <Item>output-file</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Resource Name="model" Label="Model" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::model::Resource" />
          </Accepts>
        </Resource>
        <Resource Name="attributes" Label="Attributes" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" />
          </Accepts>
        </Resource>
        <File Name="mesh-file" Label="Mesh File" ShouldExist="true"
          FileFilters="Exodus files (*.exo *.ex2 *.gen);;All files (*.*)" Version="0">
          <BriefDescription>Mesh file used by all physics solvers other than induction heating.</BriefDescription>
        </File>
        <File Name="alt-mesh-file" Label="Alt Mesh File" Optional="true" IsEnabledByDefault="false"
          ShouildExist="true"
          FileFilters="Exodus files (*.exo *.ex2 *.gen);;All files (*.*)" Version="0">
          <BriefDescription>Alternate mesh file used by the induction heating solver.</BriefDescription>
        </File>
        <Void Name="test-mode" Label="Test Mode" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>For internal use</BriefDescription>
        </Void>
      </ItemDefinitions>
     </AttDef>
   </Definitions>
  <Views>
    <View Type="Instanced" Title="Export Settings" TopLevel="true"
      FilterByCategory="false" FilterByAdvanceLevel="true">
      <InstancedAttributes>
        <Att Name="Options" Type="export" />
      </InstancedAttributes>
    </View>
  </Views>
  </SMTK_AttributeResource>
