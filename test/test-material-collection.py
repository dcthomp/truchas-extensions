import argparse
import glob
import json
import os
import shutil
import sys


def save_attributes(att_resource, defn_types, sbi_path):
    """Writes attribute collection to sbi file.

    Inputs:
        att_resource
        defn_types: list of strings specifying attribute definitions to write
        sbi_path: filesystem path of output file
    """
    writer = smtk.io.AttributeWriter()
    logger = smtk.io.Logger()

    # Get list of definitions to export
    defns = list()
    for defn_type in defn_types:
        defn = att_resource.findDefinition(defn_type)
        assert defn is not None, 'ERROR - missing definition type {}'.format(
            defn_type)
        defns.append(defn)
    writer.treatAsLibrary(defns)
    err = writer.write(att_resource, sbi_path, logger)
    assert err == 0, 'Error writing sbi file {}: {}'.format(
        sbi_path, logger.convertToString())
    assert not logger.hasErrors(), 'AttributeReader had errors: {}'.format(
        ogger.convertToString())
    print('Wrote', sbi_path)


def load_materials(att_resource, sbi_path):
    """Reads contents of sbi file (attribute collection) into attribute resource."""
    reader = smtk.io.AttributeReader()
    logger = smtk.io.Logger()
    include_path = False  # collections are single files
    err = reader.read(att_resource, sbi_path, include_path, logger)
    assert err == 0, 'Error loading sbi file {}: {}'.format(
        sbi_path, logger.convertToString())
    assert not logger.hasErrors(), 'AttributeReader had errors: {}'.format(
        ogger.convertToString())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate material collection from yml description')
    # Required arguments
    parser.add_argument('template_filepath',
                        help='Attribute template filename/path (.sbt)')
    parser.add_argument('yml_filepaths', nargs='+',
                        help='YAML file(s) specifying the attributes to generate')
    parser.add_argument('-o', '--output_folder',
                        help='output folder for writing resource files', required=True)
    # Optional arguments
    parser.add_argument('-s', '--smtktools_path',
                        help='path to smtk_tools module')

    args = parser.parse_args()
    # print(args)

    # Check filesystem paths
    if not os.path.exists(args.template_filepath):
        raise RuntimeError(
            'template file not found: {}'.format(args.template_filepath))
    yml_filepath = args.yml_filepaths[0]
    if not os.path.exists(yml_filepath):
        raise RuntimeError('yml spec not found: {}'.format(yml_filepath))
    if args.smtktools_path is not None and not os.path.exists(args.smtktools_path):
        raise RuntimeError(
            'smtk_tools directory not found: {}'.format(args.smtktools_path))
    if os.path.exists(args.output_folder):
        print('Deleting existing output folder', args.output_folder)
        shutil.rmtree(args.output_folder)
    os.makedirs(args.output_folder)

    # Put smtk imports here
    import smtk
    import smtksimulationtruchas

    # Add path to smtk_tools
    tools_path = args.smtktools_path
    if tools_path is None:
        # Default path from the repository
        source_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(source_path, 'smtk-tools')
        tools_path = os.path.normpath(path)

    assert os.path.exists(
        tools_path), 'smtk-tools path not found at {}'.format(tools_path)
    sys.path.insert(0, tools_path)

    import smtk_tools
    from smtk_tools.resource_io import ResourceIO
    from smtk_tools.attribute_builder import AttributeBuilder

    # Load yml file as the attribute specification
    import yaml
    spec = None
    print('Loading yaml file:', yml_filepath)
    with open(yml_filepath) as fp:
        content = fp.read()
        spec = yaml.safe_load(content)
    assert spec is not None

    print('Importing attribute template', args.template_filepath)
    res_io = ResourceIO()
    att_resource = res_io.import_resource(args.template_filepath)
    assert att_resource is not None, 'failed to import attribute template from {}'.format(
        args.template_filepath)

    # Initialize builder and populate the attributes
    builder = AttributeBuilder()
    builder.build_attributes(att_resource, spec)

    # Load any additional yml input files.
    # Note that yml_filepath is updated to the last spec file.
    if len(args.yml_filepaths) > 1:
        more_yml = args.yml_filepaths[1:]
        for yml_filepath in more_yml:
            print('Loading yaml file:', yml_filepath)
            with open(yml_filepath) as fp:
                content = fp.read()
                spec = yaml.safe_load(content)
            assert spec is not None
            builder.process_spec(spec)

    # Write material attributes to sbi file
    materials_filename = 'materials.sbi'
    materials_path = os.path.join(args.output_folder, materials_filename)
    defn_types = ['material.real', 'fn.material', 'fn.solid-fraction']
    save_attributes(att_resource, defn_types, materials_path)

    # Create new attribute resource
    test_resource = res_io.import_resource(args.template_filepath)
    load_materials(test_resource, materials_path)

    # Check materials
    material_atts = test_resource.findAttributes('material.real')
    assert len(material_atts) == 2, 'Wrong number of material atts, should be 2 not {}'.format(
        len(material_atts))

    # To check material validity, you *must* use the custom validation logic
    mat_utils = smtk.simulation.truchas.MaterialAttributeUtils()
    categories = set()
    for att in material_atts:
        is_valid, reason = mat_utils.isValidReason(att, categories)
        assert is_valid, reason

    # finis
