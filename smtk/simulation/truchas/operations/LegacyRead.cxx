//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/truchas/operations/LegacyRead.h"

// Local includes
#include "smtk/simulation/truchas/Metadata.h"

#include "smtk/simulation/truchas/operations/LegacyRead_xml.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/operation/operators/ReadResource.h"
#include "smtk/project/Manager.h"
#include "smtk/resource/PropertyType.h"

#include "nlohmann/json.hpp"

#include "boost/filesystem.hpp"

#include <fstream>

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

// Macro for returning on error
#define errorMacro(msg)                                                                            \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), msg);                                                              \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)
}

namespace smtk
{
namespace simulation
{
namespace truchas
{

smtk::operation::Operation::Result LegacyRead::operateInternal()
{
  auto& logger = this->log();
  std::string filename = this->parameters()->findFile("filename")->value();

  std::ifstream file(filename);
  if (!file.good())
  {
    file.close();
    errorMacro("Cannot read file \"" << filename << "\".");
  }

  nlohmann::json j;
  try
  {
    j = nlohmann::json::parse(file);
  }
  catch (...)
  {
    file.close();
    errorMacro("Cannot parse file \"" << filename << "\".");
  }
  file.close();

  // Check fileVersion and simulationCode fields
  auto jFileVersion = j.find("fileVersion");
  if (jFileVersion == j.end())
  {
    errorMacro("Input fileVersion field is missing.");
  }
  if (*jFileVersion != 1)
  {
    errorMacro("Expected fileVersion 1 not " << *jFileVersion);
  }

  auto jSimCode = j.find("simulationCode");
  if (jSimCode == j.end())
  {
    errorMacro("Input simulationCode field is missing.");
  }
  if (*jSimCode != "truchas")
  {
    errorMacro("Expected simulationCode \"truchas\" not " << *jSimCode);
  }

  if (j.count("resources") == 0)
  {
    errorMacro("Input \"resources\" field is missing.");
  }

  Metadata mdata;

  // Start with a new/empty project
  auto project = this->projectManager()->create(Metadata::PROJECT_TYPENAME);
  if (!project)
  {
    errorMacro("Cannot create project type \"" << Metadata::PROJECT_TYPENAME << "\"");
  }

  // Set the project name and location
  boost::filesystem::path projectFilePath(filename);
  boost::filesystem::path projectPath = projectFilePath.parent_path();
  std::string projectName = projectPath.filename().string();
  project->setName(projectName);
  std::string projectFilename = projectPath.string() + "/" + projectName + ".project.smtk";
  project->setLocation(projectFilename);

  // Must set project operations manager to avoid seg fault writing project.
  // Todo is this a workaround for something missing elsewhere?
  project->operations().setManager(this->manager());

  // Read input resources
  auto readOp = this->manager()->create<smtk::operation::ReadResource>();
  if (!readOp)
  {
    errorMacro("Internal error: failed to create ReadResource operator");
  }
  auto jResources = j["resources"];
  for (nlohmann::json::iterator it = jResources.begin(); it != jResources.end(); ++it)
  {
    auto jDescriptor = *it;
    std::string filename = jDescriptor["filename"];
    std::string identifier = jDescriptor["identifier"];
    std::string typeName = jDescriptor["typeName"];

    boost::filesystem::path resourcePath = projectPath / filename;
    readOp->parameters()->findFile("filename")->setValue(resourcePath.string());
    auto readResult = readOp->operate();
    int outcome = readResult->findInt("outcome")->value();
    if (outcome != OP_SUCCEEDED)
    {
      errorMacro("Failed to read resource file " << resourcePath.string());
    }

    auto resource = readResult->findResource("resource")->value();
    std::string role = "unknown";
    if ((identifier == "default") && (typeName == "smtk::attribute::Resource"))
    {
      role = resource->name();
    }
    else if ((identifier == "default") && (typeName == "smtk::session::vtk::Resource"))
    {
      role = Metadata::HEAT_TRANSFER_MESH_ROLE;
      mdata.NativeHeatTransferMeshLocation = jDescriptor["importLocation"].get<std::string>();
    }
    else if ((identifier == "second") && (typeName == "smtk::session::vtk::Resource"))
    {
      role = Metadata::INDUCTION_HEATING_MESH_ROLE;
      mdata.NativeInductionHeatingMeshLocation = jDescriptor["importLocation"].get<std::string>();
    }
    std::cout << "Adding resource type " << resource->typeName() << " with role " << role
              << std::endl;
    project->resources().add(resource, role);
  }
  mdata.putToResource(project);

  smtk::operation::Operation::Result result =
    this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  smtk::attribute::ResourceItem::Ptr created = result->findResource("resource");
  created->setValue(project);
  result->find("legacy")->setIsEnabled(true);
  return result;
}

const char* LegacyRead::xmlDescription() const
{
  return LegacyRead_xml;
}

smtk::resource::ResourcePtr read(const std::string& filename)
{
  LegacyRead::Ptr read = LegacyRead::create();
  read->parameters()->findFile("filename")->setValue(filename);
  smtk::operation::Operation::Result result = read->operate();
  if (result->findInt("outcome")->value() !=
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    return smtk::resource::ResourcePtr();
  }
  return result->findResource("resource")->value();
}
}
}
}
