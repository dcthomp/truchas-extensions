//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/utility/MaterialAttributeUtils.h"

#include "smtk/attribute/Analyses.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/io/AttributeReader.h"
#include "smtk/io/AttributeWriter.h"
#include "smtk/io/Logger.h"
#include "smtk/view/Configuration.h"

#include "smtk/common/testing/cxx/helpers.h"

#include <boost/filesystem.hpp>

#include <sstream>
#include <string>

int TestAddRemovePhase(int, char**)
{
  auto utils = smtk::simulation::truchas::MaterialAttributeUtils();
  int outputFileNumber = 0;

  // Remove output folder
  std::stringstream ss;
  ss << SCRATCH_DIR << "/"
     << "TestAddRemovePhase";
  std::string outputFolder = ss.str();
  if (boost::filesystem::is_directory(outputFolder))
  {
    boost::filesystem::remove_all(outputFolder);
  }
  boost::filesystem::create_directory(outputFolder);

  // Load attribute template
  auto attResource = smtk::attribute::Resource::create();
  smtk::io::AttributeReader reader;
  if (reader.read(attResource, TEMPLATE_PATH, true, smtk::io::Logger::instance()))
  {
    smtkTest(false, "error loading template file " << TEMPLATE_PATH);
  }

  // Create analysis att
  smtk::view::ConfigurationPtr view = attResource->findViewByType("Analysis");
  smtkTest(view != nullptr, "no analysis view found");
  smtk::view::Configuration::Component viewComp = view->details();
  std::string attName, attType;
  smtkTest(viewComp.attribute("AnalysisAttributeName", attName), "failed to get analysis att name");
  smtkTest(viewComp.attribute("AnalysisAttributeType", attType), "failed to get analysis att type");
  auto analysisDefn = attResource->analyses().buildAnalysesDefinition(attResource, attType);
  auto analysisAtt = attResource->createAttribute(attName, analysisDefn);

  // Initialize analysis att
  auto analysisItem = analysisAtt->findString("Analysis");
  analysisItem->setValue("Truchas");
  smtk::attribute::ItemPtr item =
    analysisItem->findChild("Truchas", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
  auto truchasItem = std::dynamic_pointer_cast<smtk::attribute::GroupItem>(item);
  auto htItem = truchasItem->findAs<smtk::attribute::GroupItem>("Heat Transfer");
  htItem->setIsEnabled(true);
  auto erItem = htItem->find("Enclosure Radiation");
  erItem->setIsEnabled(true);
  auto ihItem = htItem->find("Induction Heating");
  ihItem->setIsEnabled(true);

  // Create material attribute
  auto defn = attResource->findDefinition("material.real");
  smtkTest(defn != nullptr, "failed to find material.real definition");

  auto att = attResource->createAttribute("material1", defn);
  smtkTest(att != nullptr, "failed to create material attribute");
  smtkTest(!utils.isValid(att), "failed to detect attribute is NOT invalid");

  // Populate first phase
  auto invariantItem = att->findGroup("invariant-properties");
  smtkTest(invariantItem != nullptr, "failed to find invariant-properties group");
  auto phasesItem = att->findGroup("phases");
  smtkTest(phasesItem != nullptr, "failed to find phases item");
  smtkTest(phasesItem->numberOfGroups() == 1, "wrong number of phases");
  auto sharedItem = att->findGroup("shared-properties");
  smtkTest(sharedItem != nullptr, "failed to find shared-properties item");

  auto densityItem = invariantItem->findAs<smtk::attribute::DoubleItem>("density");
  smtkTest(densityItem != nullptr, "failed to find density item");
  smtkTest(densityItem->setValue(1750.0), "failed to set density");

  auto conductivityItem = phasesItem->findAs<smtk::attribute::DoubleItem>("conductivity");
  smtkTest(conductivityItem != nullptr, "failed to find conductivity item");
  smtkTest(conductivityItem->setValue(100.0), "failed to set conductivity item");

  auto ecItem = phasesItem->findAs<smtk::attribute::DoubleItem>("electrical-conductivity");
  smtkTest(ecItem != nullptr, "failed to find elecrical-conductivity item");
  smtkTest(ecItem->setValue(3.14159e-5), "failed to set elecrical-conductivity item");

  smtkTest(!utils.isValid(att), "failed to detect attribute IS valid");

  // Export to sbi file
  smtk::io::AttributeWriter writer;
  std::string outputFile;

  ss.str("");
  ss << outputFolder << "/"
     << "TestAddRemovePhase" << ++outputFileNumber << ".sbi";
  outputFile = ss.str();
  if (writer.write(attResource, outputFile, smtk::io::Logger::instance()))
  {
    smtkTest(false, "error writing attribute file " << outputFile);
  }
  std::cout << "Wrote " << outputFile << " single phase" << std::endl;

  // Create second phase, export to another sbi file
  smtkTest(utils.addPhase(att), "failed to add material phase");
  smtkTest(!utils.isValid(att), "failed to detect attribute is NOT invalid");

  smtkTest(phasesItem->numberOfGroups() == 2, "wrong number of phases, should be 2");

  auto transitionsItem = att->findGroup("transitions");
  smtkTest(transitionsItem != nullptr, "failed to find transitions group");
  smtkTest(transitionsItem->numberOfGroups() == 1, "wrong number of phases, should be 1");

  ss.str("");
  ss << outputFolder << "/"
     << "TestAddRemovePhase" << ++outputFileNumber << ".sbi";
  outputFile = ss.str();
  writer.write(attResource, outputFile, smtk::io::Logger::instance());
  std::cout << "Wrote " << outputFile << " add phase" << std::endl;

  // Set conductivity in Phase 2, export to another sbi file
  auto condItem2 = phasesItem->findAs<smtk::attribute::DoubleItem>(1, "conductivity");
  condItem2->setIsEnabled(true);
  condItem2->setValue(234.5);
  ss.str("");
  ss << outputFolder << "/"
     << "TestAddRemovePhase" << ++outputFileNumber << ".sbi";
  outputFile = ss.str();
  writer.write(attResource, outputFile, smtk::io::Logger::instance());
  std::cout << "Wrote " << outputFile << " set second phase conductivity" << std::endl;

  // Disable electrical-conductivity in shared-properties
  auto sharedECItem = sharedItem->find("electrical-conductivity");
  utils.enableSharedItem(sharedECItem, false);
  ss.str("");
  ss << outputFolder << "/"
     << "TestAddRemovePhase" << ++outputFileNumber << ".sbi";
  outputFile = ss.str();
  writer.write(attResource, outputFile, smtk::io::Logger::instance());
  std::cout << "Wrote " << outputFile << " unselect shared electrical-conductivity" << std::endl;

  // Remove first phase, export to another sbi file
  utils.removePhase(att, 0);
  ss.str("");
  ss << outputFolder << "/"
     << "TestAddRemovePhase" << ++outputFileNumber << ".sbi";
  outputFile = ss.str();
  writer.write(attResource, outputFile, smtk::io::Logger::instance());
  std::cout << "Wrote " << outputFile << " remove first phase" << std::endl;

  smtkTest(!utils.isValid(att), "failed to detect attribute is NOT invalid");

  attResource.reset();
  return 0;
}
