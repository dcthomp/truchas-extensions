# Truchas end-to-end tests

This folder is for end-to-end tests, mainly to verify the simulation templates and export operation logic. There are also unit tests elsewhere, inside the source folders.

* The end-to-end tests implement examples that were developed by LANL, and are captured in the cmb-target-problems submodule.
* The end-to-end tests also make use of the attribute builder utility in the smtk-tools submodule. Each test builds an attribute resource from a yml-formatted specification, runs the Truchas export operation, and compares the resulting input file with a baseline file.

To include the test when building the project:

* Load the git submodules (`git submodule init; git submodule update`)
* Set the ENABLE_TESTING flag when running cmake.
