mixin flow-solver(abs_tol, rel_tol)
  //- Common content for flow-pressure-solver and flow-viscous-solver
  String(Name="krylov_method" Label="Krylov Method" AdvanceLevel="1")
    ChildrenDefinitions
      Int(Name="krylov_dim" Label="Krylov Dimension" AdvanceLevel="1")
        DefaultValue 5
        RangeInfo
          Min(Inclusive="true") 1
    DiscreteInfo(DefaultIndex="0")
      Value cg
      Structure
        Value gmres
        Items
          Item krylov_dim
      Value bicgstab
  Double(Name="conv_rate_tol" Label="Convergence Rate Tolerance" AdvanceLevel="1")
    DefaultValue 0.9
    RangeInfo
      Min(Inclusive="false") 0.0
      Max(Inclusive="true") 1.0
  Double(Name="abs_tol" Label="Absolute Tolerance")
    DefaultValue #{abs_tol}
    RangeInfo
      Min(Inclusive="true") 0.0
  Double(Name="rel_tol" Label="Relative Tolerance")
    DefaultValue #{rel_tol}
    RangeInfo
      Min(Inclusive="false") 0.0
  Int(Name="max_ds_iter", Label="Max diagonally scaled Krylov iterations")
    DefaultValue 100
  Int(Name="max_pcg_iter", Label="Max preconditioned Krylov iterations")
    DefaultValue 10
  Void(Name="verbose-output" Label="Verbose Output" Optional="true" IsEnabledByDefault="false")
    BriefDescription Sets HYPRE print level to 2 for debugging.
  Group(Name="hypre" Label="HYPRE Parameters" AdvanceLevel="1")
    ItemDefinitions
      // Hide cg_use_two_norm (Oct 2020)
      Void(Name="cg_use_two_norm" Optional="true" IsEnabledByDefault="false" AdvanceLevel=99)
      Double(Name="amg_strong_threshold" Optional="true" IsEnabledByDefault="false")
      Int(Name="amg_max_levels" Optional="true" IsEnabledByDefault="false")
      Int(Name="amg_coarsen_method" Optional="true" IsEnabledByDefault="false")
      Int(Name="amg_smoothing_sweeps" Optional="true" IsEnabledByDefault="false")
      Int(Name="amg_smoothing_method" Optional="true" IsEnabledByDefault="false")
      Int(Name="amg_interp_method" Optional="true" IsEnabledByDefault="false")

doctype xml
SMTK_AttributeResource(Version="4")
  Categories
    Cat Fluid Flow
    Cat Viscous Flow

  Definitions
    // Flow Numerics
    AttDef(Type="flow-numerics" Label="Numerics")
      Categories #[Cat Fluid Flow]
      ItemDefinitions
        Double(Name="courant_number" Label="Courant Number")
          BriefDescription Sets an upper bound on the time step that is associated with stability of the explicit fluid advection algorithm.
          DefaultValue 0.5
          RangeInfo
            Min(Inclusive="false") 0.0
            Max(Inclusive="true") 1.0
        Double(Name="viscous_number" Label="Viscous Number" AdvanceLevel="1")
          BriefDescription Sets an upper bound on the time step that is associated with stability of an explicit treatment of viscous flow stress tensor.
          DefaultValue 0.0
          RangeInfo
            Min(Inclusive="true") 0.0
        Int(Name="vol_track_subcycles" Label="Volume Tracker Subcycles")
          BriefDescription Number of sub-time steps n taken by the volume tracker for every time step of the flow algorithm.
          DefaultValue 2
          RangeInfo
            Min(Inclusive="true") 1
        Void(Name="nested_dissection" Label="Nested Dissection" Optional="true" IsEnabledByDefault="true" AdvanceLevel="1")
          BriefDescription Enables use of the nested dissection algorithm to reconstruct material interfaces in cells containing 3 or more materials.
        Double(Name="vol_frac_cutoff" Label="Volume Fraction Cutoff" AdvanceLevel="1")
          BriefDescription The smallest volume fraction allowed.
          DefaultValue 1e-8
          RangeInfo
            Min(Inclusive="false") 0.0
            Max(Inclusive="true") 1.0
        Int(Name="fischer_dim" Label="Fischer's Method Dimension" AdvancedLevel="1")
          BriefDescription The dimension of the subspace used in Fischer’s projection method for computing an initial guess for pressure projection system based on previous solutions.
          DefaultValue 6
        Double(Name="fluid_frac_threshold" Label="Fluid Fraction Threshold" AdvanceLevel="1")
          BriefDescription Cells with a total fluid volume fraction less than this threshold are ignored by the flow solver.
          DefaultValue 1e-2
          RangeInfo
            Min(Inclusive="false") 0.0
            Max(Inclusive="true") 1.0
        Double(Name="min_face_fraction" Label="Min Face Fraction" AdvanceLevel="1")
          BriefDescription The minimum value of the fluid density associated with a face for the pressure projection system.
          DefaultValue 1e-3
          RangeInfo
            Min(Inclusive="false") 0.0
            Max(Inclusive="true") 1.0
        Group(Name="void_collapse" Label="Void Collapse" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1")
          ItemDefinitions
            Double(Name="void_collapse_relaxation" Label="Relaxation Parameter")
              BriefDescription The relaxation parameter in the void collapse model.
              DefaultValue 0.1
              RangeInfo
                Min(Inclusive="true") 0.0
                Min(Inclusive="true") 1.0
        Group(Name="wisp_redistribution" Label="Wisp Redistribution" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1")
          ItemDefinitions
            Double(Name="wisp_cutoff" Label="Cutoff")
              BriefDescription Fluid cells with a fluid volume fraction below this value may be treated as wisps and havetheir fluid material moved around to other fluid cells.
              DefaultValue 0.05
              RangeInfo
                Min(Inclusive="true") 0.0
            Double(Name="wisp_neighbor_cutoff" Label="Neighbor Cutoff")
              BriefDescription Fluid cells with a fluid volume fraction belowwisp_cutoffcan only be considered a wisp ifthe amount of fluid in the neighboring cells is also "small"
              DefaultValue 0.25
              RangeInfo
                Min(Inclusive="true") 0.0

    AttDef(Type="material-priority" Label="Material Priority")
      Categories #[Cat Fluid Flow]
      ItemDefinitions
        Group(Name="track_interfaces" Label="Track Interfaces" Optional="true" IsEnabledByDefault="true")
          BriefDescription Enables the tracking of material interfaces.
          ItemDefinitions
            Group(Name="material_priority" Label="Material Priority" Extensible="true" NumberOfRequiredGroups="0")
              //- Using group because no UI for extensible ComponentItem
              BriefDescription Priority order in which material interfaces are reconstructed within a cell for volume tracking.
              ItemDefinitions
                Component(Name="material" Label="Material" NumberOfRequiredValues="1")
                  Accepts
                    Resource(Name="smtk::attribute::Resource" Filter="attribute[type='material']")
            // Void item used by UI to track changes which require user confirmation
            Void(Name="confirmed" AdvanceLevel="99" Optional="true" IsEnabledByDefault="true")

    AttDef(Type="pressure-linear-solver" Label="Pressure Linear Solver")
      Categories #[Cat Fluid Flow]
      ItemDefinitions
        +flow-solver(1e-9, 0.0)
    AttDef(Type="viscous-linear-solver" Label="Viscous Linear Solver")
      Categories #[Cat Viscous Flow]
      ItemDefinitions
        Group(Name="implicit-time-stepping" Label="Implicit Time Stepping" Optional="true" IsEnabledByDefault="true")
          ItemDefinitions
            Double(Name="viscous_implicitness" Label="Viscous Implicitness" AdvanceLevel="1")
              BriefDescription The degree of time implicitness used for the velocity field in the discretization of the viscous flow stress tensor in the fluid momentum conservation equation.
              DefaultValue 1.0
              RangeInfo
                Min(Inclusive="false") 0.0
                Max(Inclusive="true") 1.0
            +flow-solver(0.0, 1e-6)
