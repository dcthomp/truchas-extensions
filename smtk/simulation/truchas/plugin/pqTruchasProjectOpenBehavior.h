//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqTruchasProjectOpenBehavior_h
#define pqTruchasProjectOpenBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqTruchasProjectOpenReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqTruchasProjectOpenReaction(QAction* parent);

  void openProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->openProject(); }

private:
  Q_DISABLE_COPY(pqTruchasProjectOpenReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqTruchasProjectOpenBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqTruchasProjectOpenBehavior* instance(QObject* parent = nullptr);
  ~pqTruchasProjectOpenBehavior() override;

protected:
  pqTruchasProjectOpenBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqTruchasProjectOpenBehavior);
};

#endif
