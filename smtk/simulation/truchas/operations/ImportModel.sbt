<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the Project "Add" Operation -->
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="import-model" Label="Truchas Project - Import Model" BaseType="operation">
      <BriefDescription>
        Imports a model to use for the induction-heating mesh.
      </BriefDescription>

      <AssociationsDef Name="project" NumberOfRequiredValues="1"
                       Extensible="false" OnlyResources="true">
        <Accepts><Resource Name="smtk::project::Project"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <File Name="induction-heating-mesh" Label="Induction Heating Mesh" Optional="true" IsEnabledByDefault="false" ShouldExist="true" FileFilters="Exodus Files (*.ex? *.gen);;NetCDF Files (*.ncdf);;All Files (*)">
          <BriefDescription>The input file to be used as the induction heat (alt) mesh.</BriefDescription>
        </File>
        <Void Name="copy-file" Label="Copy Geometry File(s) Into Project Folder" AdvanceLevel="1" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>If enabled, store a copy of the native file(s) in the project directory.</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(add)" BaseType="result"/>

  </Definitions>
</SMTK_AttributeResource>
