//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/truchas/utility/ModelUtils.h"

#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/utility/ProjectUtils.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/common/UUID.h"
#include "smtk/model/Resource.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/operators/AssignColors.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Properties.h"

#include <iomanip>
#include <iostream>
#include <sstream>

namespace smtk
{
namespace simulation
{
namespace truchas
{

bool ModelUtils::assignColors(smtk::model::ResourcePtr modelResource, smtk::model::BitFlags entType,
  const std::vector<std::string>& palette) const
{
  auto colorOp = smtk::operation::AssignColors::create();

  // Associate model entities
  auto uuidSet = modelResource->entitiesMatchingFlags(entType, true);
  for (auto uuid : uuidSet)
  {
    auto modelEnt = modelResource->findEntity(uuid);
    colorOp->parameters()->associate(modelEnt);
  }

  smtk::attribute::StringItemPtr colorsItem = colorOp->parameters()->findString("colors");
  colorsItem->setIsEnabled(true);
  colorsItem->setNumberOfValues(palette.size());
  colorsItem->setValues(palette.begin(), palette.end());

  auto result = colorOp->operate();
  return true;
}

void ModelUtils::renameModelEntities(smtk::model::ResourcePtr modelResource) const
{
  std::cout << "Renaming model volumes" << std::endl;
  this->renameModelEntitiesInternal(modelResource, smtk::model::VOLUME, "block ");

  std::cout << "Renaming model faces" << std::endl;
  this->renameModelEntitiesInternal(modelResource, smtk::model::FACE, "side set ");

  // Mark the resource as modified.
  modelResource->setClean(false);
}

void ModelUtils::tagResources(smtk::project::ProjectPtr project) const
{
  ProjectUtils projectUtils;
  auto htModelRes =
    projectUtils.getByRole<smtk::model::Resource>(project, Metadata::HEAT_TRANSFER_MESH_ROLE);
  if (htModelRes)
  {
    htModelRes->properties().get<std::string>()["project.use"] = Metadata::HEAT_TRANSFER_MESH_ROLE;
  }

  auto emModelRes =
    projectUtils.getByRole<smtk::model::Resource>(project, Metadata::INDUCTION_HEATING_MESH_ROLE);
  if (emModelRes)
  {
    emModelRes->properties().get<std::string>()["project.use"] =
      Metadata::INDUCTION_HEATING_MESH_ROLE;
  }
}

void ModelUtils::renameModelEntitiesInternal(smtk::model::ResourcePtr modelResource,
  smtk::model::EntityTypeBits entType, const std::string& prefix) const
{
  auto entRefs = modelResource->findEntitiesOfType(entType, true);

  // First pass - find the max id
  int maxId = -1;
  for (auto entRef : entRefs)
  {
    auto uuid = entRef.entity();
    auto propList = modelResource->integerProperty(uuid, "pedigree id");
    if (propList.size() == 1)
    {
      int id = propList[0];
      maxId = id > maxId ? id : maxId;
    }
  } // for

  // Sanity check
  if (maxId < 0)
  {
    std::cerr << "No pedigree ids found" << std::endl;
    return;
  }

  // Get number of digits
  int number = maxId;
  int digits = 0;
  while (number)
  {
    number /= 10;
    digits++;
  }

  // Second pass - update entity names
  std::stringstream ss;
  for (auto entRef : entRefs)
  {
    auto uuid = entRef.entity();
    auto propList = modelResource->integerProperty(uuid, "pedigree id");
    if (propList.size() == 1)
    {
      int id = propList[0];
      ss.str("");
      ss << prefix << std::setfill('0') << std::setw(digits) << id;

      // If vtk found no name, it assigns "Unnamed ..."
      if (!entRef.name().empty() && entRef.name().find("Unnamed") != 0)
      {
        ss << " - " << entRef.name();
      }

      entRef.setName(ss.str());
    } // if
  }   // for (entRefs)
}

// end namespace smtk::simulation::truchas
}
}
}
