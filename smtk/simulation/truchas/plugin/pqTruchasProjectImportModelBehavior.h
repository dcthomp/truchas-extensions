//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqTruchasProjectImportModelBehavior_h
#define pqTruchasProjectImportModelBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for importing a model into a project.
/// This class is HARD-CODED to only support importing a model for the
/// INDUCTION HEATING role.

class pqTruchasProjectImportModelReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqTruchasProjectImportModelReaction(QAction* parent);

  void importModel();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->importModel(); }

private:
  Q_DISABLE_COPY(pqTruchasProjectImportModelReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqTruchasProjectImportModelBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqTruchasProjectImportModelBehavior* instance(QObject* parent = nullptr);
  ~pqTruchasProjectImportModelBehavior() override;

protected:
  pqTruchasProjectImportModelBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqTruchasProjectImportModelBehavior);
};

#endif
