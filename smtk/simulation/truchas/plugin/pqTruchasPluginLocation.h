//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_plugin_pqTruchasPluginLocation_h
#define smtk_simulation_truchas_plugin_pqTruchasPluginLocation_h

#include <QObject>

class pqTruchasPluginLocation : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqTruchasPluginLocation(QObject* parent = nullptr);
  ~pqTruchasPluginLocation() override;

  void StoreLocation(const char* location);

private:
  Q_DISABLE_COPY(pqTruchasPluginLocation);
};

#endif
