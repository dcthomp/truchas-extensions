//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/plugin/pqTruchasExtensionsAutoStart.h"

// Plugin includes
#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/Registrar.h"
#include "smtk/simulation/truchas/qt/qtProjectRuntime.h"
#include "smtk/simulation/truchas/qt/qtTruchasViewRegistrar.h"

// SMTK includes
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResourcePanel.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMTKSettings.h"
#include "smtk/extension/qt/qtSMTKUtilities.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Resource.h"
#include "smtk/view/ResourcePhraseModel.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMIntVectorProperty.h"

#include <QApplication>
#include <QDebug>
#include <QMainWindow>
#include <QTimer>
#include <QWidget>

namespace
{
// Configure resource panel to omit displaying projects
// Code is copied from smtk::project::AutoStart
void setView()
{
  for (QWidget* w : QApplication::topLevelWidgets())
  {
    QMainWindow* mainWindow = dynamic_cast<QMainWindow*>(w);
    if (mainWindow)
    {
      pqSMTKResourcePanel* dock = mainWindow->findChild<pqSMTKResourcePanel*>();
      // If the dock is not there, just try it again.
      if (dock)
      {
        auto phraseModel = std::dynamic_pointer_cast<smtk::view::ResourcePhraseModel>(
          dock->resourceBrowser()->phraseModel());
        if (phraseModel)
        {
          phraseModel->setFilter([](const smtk::resource::Resource& resource) {
            return !resource.isOfType(smtk::common::typeName<smtk::project::Project>());
          });
        }
      }
      else
      {
        QTimer::singleShot(10, []() { setView(); });
      }
    }
  }
}
} // namespace

pqTruchasExtensionsAutoStart::pqTruchasExtensionsAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqTruchasExtensionsAutoStart::~pqTruchasExtensionsAutoStart()
{
}

void pqTruchasExtensionsAutoStart::startup()
{
  // Instantiate project runtime
  qtProjectRuntime::instance(pqCoreUtilities::mainWidget());

  // Check for current/active pqServer
  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server != nullptr)
  {
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
    this->resourceManagerAdded(wrapper, server);
  }

  // Listen for server connections
  auto smtkBehavior = pqSMTKBehavior::instance();
  QObject::connect(smtkBehavior, static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
                                   &pqSMTKBehavior::addedManagerOnServer),
    this, &pqTruchasExtensionsAutoStart::resourceManagerAdded);
  QObject::connect(smtkBehavior, static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
                                   &pqSMTKBehavior::removingManagerFromServer),
    this, &pqTruchasExtensionsAutoStart::resourceManagerRemoved);

  // Since the loading order of smtk plugins is indeterminate, a spinning
  // function call is used here to set up the custom view.
  QTimer::singleShot(10, []() { setView(); });
}

void pqTruchasExtensionsAutoStart::shutdown()
{
}

void pqTruchasExtensionsAutoStart::resourceManagerAdded(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::resource::ManagerPtr resManager = wrapper->smtkResourceManager();
  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  smtk::operation::ManagerPtr opManager = wrapper->smtkOperationManager();
  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  if (!resManager || !viewManager || !opManager || !projManager)
  {
    return;
  }

  smtk::simulation::truchas::Registrar::registerTo(projManager);
  smtk::extension::qtTruchasViewRegistrar::registerTo(viewManager);

  // Disable the smtk save-on-close dialog (superseded in our close behavior)
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    vtkSMProperty* ssProp = proxy->GetProperty("ShowSaveResourceOnClose");
    vtkSMIntVectorProperty* ssIntProp = vtkSMIntVectorProperty::SafeDownCast(ssProp);
    if (ssIntProp != nullptr)
    {
      ssIntProp->SetElement(0, vtkSMTKSettings::DontShowAndDiscard);
      proxy->UpdateVTKObjects();
    } // if (ssIntProp)
  } // if (proxy)
}

void pqTruchasExtensionsAutoStart::resourceManagerRemoved(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  if (viewManager != nullptr)
  {
    smtk::extension::qtTruchasViewRegistrar::unregisterFrom(viewManager);
  }

  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  if (projManager != nullptr)
  {
    smtk::simulation::truchas::Registrar::unregisterFrom(projManager);
  }
}
