//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_plugin_pqTruchasProjectExportBehavior_h
#define smtk_simulation_truchas_plugin_pqTruchasProjectExportBehavior_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Operation.h"

#include "pqReaction.h"

#include <QObject>

#include <string>

/// A reaction for writing a resource manager's state to disk.
class pqTruchasProjectExportReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqTruchasProjectExportReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqTruchasProjectExportReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqTruchasProjectExportBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqTruchasProjectExportBehavior* instance(QObject* parent = nullptr);
  ~pqTruchasProjectExportBehavior() override;

  void exportProject();

protected slots:
  void onOperationExecuted(const smtk::operation::Operation::Result& result);

protected:
  pqTruchasProjectExportBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqTruchasProjectExportBehavior);
};

#endif
