//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_truchas_utility_ProjectUtils_h
#define smtk_simulation_truchas_utility_ProjectUtils_h

#include "smtk/simulation/truchas/Exports.h"

#include "smtk/project/Project.h"
#include "smtk/project/ResourceContainer.h"
#include "smtk/resource/Resource.h"

#include <sstream>
#include <string>

namespace smtk
{
namespace simulation
{
namespace truchas
{

class SMTKTRUCHAS_EXPORT ProjectUtils
{
public:
  // Finds single resource by role
  smtk::resource::ResourcePtr getByRole(smtk::project::ProjectPtr project,
    const std::string& role,
    std::string& errMessage) const;

  template<typename ResourceType>
  std::shared_ptr<ResourceType> getByRole(smtk::project::ProjectPtr project,
    const std::string& role,
    std::string& errMessage);

  template<typename ResourceType>
  std::shared_ptr<ResourceType> getByRole(smtk::project::ProjectPtr project,
    const std::string& role);
};

template<typename ResourceType>
std::shared_ptr<ResourceType> ProjectUtils::getByRole(smtk::project::ProjectPtr project,
  const std::string& role,
  std::string& errMessage)
{
  errMessage.clear();
  auto resourceSet = project->resources().findByRole(role);
  if (resourceSet.empty())
  {
    std::stringstream ss;
    ss << "Project is missing \"" << role << "\" resource.";
    errMessage = ss.str();
    return std::shared_ptr<ResourceType>(nullptr);
  }
  else if (resourceSet.size() == 1)
  {
    smtk::resource::ResourcePtr res = *(resourceSet.begin());
    std::shared_ptr<ResourceType> typedRes = std::dynamic_pointer_cast<ResourceType>(res);
    if (typedRes == nullptr)
    {
      std::stringstream ss;
      ss << "Project is missing \"" << role << "\" resource.";
      errMessage = ss.str();
    }
    return typedRes;
  }

  // (else)
  // Technically we should pruce the set by ResourceType, but Truchas projects do not
  // currently support multiple resources with the same role.
  std::stringstream ss;
  ss << "Project has multiple \"" << role << "\" resources.";
  errMessage = ss.str();
  return std::shared_ptr<ResourceType>(nullptr);
}

template<typename ResourceType>
std::shared_ptr<ResourceType> ProjectUtils::getByRole(smtk::project::ProjectPtr project,
  const std::string& role)
{
  std::string errMessage;
  return this->getByRole<ResourceType>(project, role, errMessage);
}

}
}
}
#endif
