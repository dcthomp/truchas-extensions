<?xml version="1.0"?>
<!--Created by XmlV3StringWriter-->
<SMTK_AttributeResource Version="3">
  <Includes>
    <!-- Note: order is important, e.g., put expressions first -->
    <File>functions.sbt</File>
    <File>material.sbt</File>
  </Includes>

  <Views>
    <View Type="Group" Name="Materials (Experimental)" FilterByAdvanceLevel="false" FilterByCategory="false" TabPosition="North" TopLevel="true">
      <Views>
        <View Title="Custom" />
<!--         <View Title="Default" /> -->
      </Views>
    </View>
    <View Type="smtkTruchasMaterialsView" Name="Custom" FilterByAdvanceLevel="false" FilterByCategory="false">
      <AttributeTypes>
        <Att Type="material.real" />
      </AttributeTypes>
    </View>
<!--     <View Type="Attribute" Name="Default" HideAssociations="true">
      <AttributeTypes>
        <Att Type="material" />
      </AttributeTypes>
    </View> -->
  </Views>
</SMTK_AttributeResource>
