//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtMaterialPriorityModel.h - Qt model for material priority list

#ifndef smtk_simulation_truchas_qt_qtMaterialPriorityModel_h
#define smtk_simulation_truchas_qt_qtMaterialPriorityModel_h

#include "smtk/PublicPointerDefs.h"

#include "smtk/simulation/truchas/qt/Exports.h"

#include <QAbstractTableModel>

#include <set>
#include <vector>

/* **
  Qt table model for material priority, used in flow analysis.

  The data are internally represented by an extensible "material_priority"
  GroupItem that contains a list of "material" ComponentItems. The GroupItem
  is updated when material attributes are referenced or dereferenced to
  "body" attributes and the "inflow_material" property of "flow_bc"
  attributes.
 */

class SMTKTRUCHASQTEXT_EXPORT qtMaterialPriorityModel : public QAbstractTableModel
{
  Q_OBJECT
  typedef QAbstractTableModel Superclass;

public:
  qtMaterialPriorityModel(QObject* parent, smtk::attribute::GroupItemPtr groupItem);
  virtual ~qtMaterialPriorityModel();

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  QVariant headerData(
    int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  // Use QMimeData for drag & drop logic
  bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column,
    const QModelIndex& parent) override;
  QMimeData* mimeData(const QModelIndexList& indexes) const override;

  // Check that set of material attributes matches current model
  bool isValid(const std::set<smtk::attribute::AttributePtr>& materialAtts) const;

  // Updates refItem and identifies material attributes to be assigned
  void update(const std::set<smtk::attribute::AttributePtr>& materialAtts);

signals:
  void attributeAssigned(int dragRow);
  void modified(smtk::attribute::ItemPtr, bool needsConfirm);

protected:
  smtk::attribute::WeakGroupItemPtr m_groupItem;
};

#endif
