//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqTruchasProjectImportModelBehavior.h"

// Truchas Extension includes
#include "smtk/simulation/truchas/operations/ImportModel.h"
#include "smtk/simulation/truchas/qt/qtProjectRuntime.h"
#include "smtk/simulation/truchas/utility/ModelUtils.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/operation/operators/ImportResource.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"
#include "pqWaitCursor.h"

// Qt includes
#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QMessageBox>
#include <QString>

namespace
{
const int OP_SUCCESS = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
} // namespace

//-----------------------------------------------------------------------------
pqTruchasProjectImportModelReaction::pqTruchasProjectImportModelReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqTruchasProjectImportModelReaction::importModel()
{
  // Note that we only support importing induction-heating model
  const std::string projectRole = "induction-heating";

  auto project = qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Cannot import model because no project currently loaded.";
    return;
  }

  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Construct a file dialog for the user to select the model file
  QString filter("Exodus Files (*.ex? *.gen);;NetCDF Files (*.ncdf);;All Files (*)");
  pqFileDialog fileDialog(
    server, pqCoreUtilities::mainWidget(), tr("Select Model File:"), QString(), filter);
  fileDialog.setObjectName("FileOpenDialog");
  fileDialog.setFileMode(pqFileDialog::ExistingFile);
  fileDialog.setShowHidden(true);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString location = fileDialog.getSelectedFiles()[0];

  // Import the model
  smtk::resource::ResourcePtr newResource;
  auto importOp = opManager->create<smtk::simulation::truchas::ImportModel>();
  importOp->parameters()->associate(project);
  importOp->parameters()->findFile("induction-heating-mesh")->setValue(location.toStdString());
  auto result = importOp->operate();
  int outcome = result->findInt("outcome")->value();
  if (outcome != OP_SUCCESS)
  {
    qWarning() << importOp->log().convertToString().c_str();
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error",
      "Unable to import model file; see \"Output Messages\" for more info.");
    return;
  }

  // Set resource "tags" (string properties used by toolbar)
  smtk::simulation::truchas::ModelUtils modelUtils;
  modelUtils.tagResources(project);
} // importModel()

//-----------------------------------------------------------------------------
static pqTruchasProjectImportModelBehavior* g_instance = nullptr;

pqTruchasProjectImportModelBehavior::pqTruchasProjectImportModelBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqTruchasProjectImportModelBehavior* pqTruchasProjectImportModelBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqTruchasProjectImportModelBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqTruchasProjectImportModelBehavior::~pqTruchasProjectImportModelBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
