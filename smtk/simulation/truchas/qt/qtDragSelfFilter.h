//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtDragSelfFilter.h - filters drag events from other sources

#ifndef __smtk_simulation_truchas_qt_qtDragSelfFilter_h
#define __smtk_simulation_truchas_qt_qtDragSelfFilter_h

#include "smtk/simulation/truchas/qt/Exports.h"

#include <QObject>

/* **
  * Provides logic to disable drop events from sources
  * other than the parent object.
 */

class QEvent;

class SMTKTRUCHASQTEXT_EXPORT qtDragSelfFilter : public QObject
{
public:
  qtDragSelfFilter(QObject* parent = nullptr);

protected:
  bool eventFilter(QObject* obj, QEvent* event) override;
};

#endif // __smtk_simulation_truchas_qt_qtDragSelfFilter_h
