//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtkTruchasMaterialsView.h"

// Truchas extension includes
#include "smtk/simulation/truchas/qt/qtAttributeListWidget.h"
#include "smtk/simulation/truchas/qt/qtMaterialAttribute.h"
#include "smtk/simulation/truchas/qt/qtProjectRuntime.h"
#include "smtk/simulation/truchas/qt/qtValidators.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/operators/Export.h"
#include "smtk/attribute/operators/Import.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtAttribute.h"
#include "smtk/extension/qt/qtItem.h"
#include "smtk/extension/qt/qtTableWidget.h"
#include "smtk/extension/qt/qtTypeDeclarations.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/view/Configuration.h"

#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QFrame>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPointer>
#include <QSplitter>
#include <QString>
#include <QVBoxLayout>

#include <set>
// #include <string>
#include <vector>

class smtkTruchasMaterialsView::Internal
{
public:
  // Internal member data
  qtAttributeListWidget* ListFrame; // upper panel displaying list of attributes
  QPointer<QFrame> EditorFrame;     // lower panel displaying attribute editor
  QPointer<qtMaterialAttribute> CurrentAtt;

  smtk::view::Configuration::Component AttComponent;
  std::string AttributeType;

  Internal()
    : ListFrame(nullptr)
  {
  }
}; // Internal

qtBaseView* smtkTruchasMaterialsView::createViewWidget(const smtk::view::Information& info)
{
  smtkTruchasMaterialsView* view = new smtkTruchasMaterialsView(info);
  view->buildUI();
  return view;
}

smtkTruchasMaterialsView::smtkTruchasMaterialsView(const smtk::view::Information& info)
  : qtBaseAttributeView(info)
{
  m_internal = new smtkTruchasMaterialsView::Internal();

  smtk::view::ConfigurationPtr view = this->configuration();
  int typesIndex = view->details().findChild("AttributeTypes");
  if (typesIndex < 0)
  {
    qWarning() << "ERROR: smtkTruchasMaterialsView missing AttributeTypes element";
    return;
  }
  smtk::view::Configuration::Component typesComp = view->details().child(typesIndex);
  std::size_t i, n = typesComp.numberOfChildren();
  for (i = 0; i < n; i++)
  {
    smtk::view::Configuration::Component& attComp = typesComp.child(i);
    if (attComp.name() == "Att")
    {
      m_internal->AttComponent = attComp;
      break;
    }
  } // for (i)

  // Check that attribute Type is specified
  if (!m_internal->AttComponent.attribute("Type", m_internal->AttributeType))
  {
    qWarning()
      << "ERROR: smtkTruchasMaterialsView/AttributeTypes missing Att element or Type attribute";
  }
}

smtkTruchasMaterialsView::~smtkTruchasMaterialsView()
{
  delete m_internal;
}

bool smtkTruchasMaterialsView::isEmpty() const
{
  // Returns true when categories are *not* enabled

  // Sanity check
  if (m_internal->AttributeType.empty())
  {
    qWarning() << "Missing material attribute type in view specification";
    return true;
  }

  auto attResource = this->attributeResource();
  auto attDef = attResource->findDefinition(m_internal->AttributeType);
  auto& categories = attDef->categories();
  bool pass = attResource->passActiveCategoryCheck(categories);
  return !pass;
}
bool smtkTruchasMaterialsView::isValid() const
{
  // For now, use brute force to compute validity
  // Todo consider adding dictionary of <att, valid>
  auto attResource = this->attributeResource();
  auto attVector = attResource->findAttributes(m_internal->AttributeType);
  for (auto att : attVector)
  {
    if (!isAttributeValid(att))
    {
      return false;
    }
  }

  // (else)
  return true;
}

void smtkTruchasMaterialsView::loadFromSbiFile()
{
  qtProjectRuntime* runtime = qtProjectRuntime::instance();
  auto project = runtime->project();

  // Run dialog to get input file
  QFileInfo fileInfo(project->location().c_str());
  QString filter("sbi files (*.sbi);;All files (*)");
  QFileDialog fileDialog(
    this->parentWidget(), "Select File", fileInfo.absoluteDir().absolutePath(), filter);
  fileDialog.setObjectName("FileSaveDialog");
  fileDialog.setFileMode(QFileDialog::ExistingFile);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString filePath = fileDialog.selectedFiles()[0];

  // Setup import operation
  // Note: getting operation manager from project manager requires const cast
  const smtk::project::Manager* constManager = project->manager();
  auto projManager = const_cast<smtk::project::Manager*>(constManager);
  smtk::operation::ManagerPtr opManager = projManager->operationManager();

  smtk::operation::OperationPtr importOp = opManager->create<smtk::attribute::Import>();
  if (!importOp)
  {
    QString msg("Could not create \"import attribute operation\"");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(this->parentWidget(), "ERROR reading attributes", msg);
    return;
  }

  std::shared_ptr<smtk::attribute::Attribute> paramsAtt = importOp->parameters();

  auto fileItem = paramsAtt->findFile("filename");
  fileItem->setValue(filePath.toStdString());

  // Load sbi file into current resource
  auto attResource = this->attributeResource();
  paramsAtt->associate(attResource);

  QString msg;
  QTextStream qs(&msg);
  try
  {
    auto result = importOp->operate();
    int outcome = result->findInt("outcome")->value();
    if (outcome == static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      qs << "Loaded " << filePath;
      QMessageBox::information(this->parentWidget(), "Success", msg);
    }
    else
    {
      qs << "\"import attribute operation\" failed, outcome " << outcome;
      QMessageBox::critical(this->parentWidget(), "FAILED", msg);
      smtkErrorMacro(importOp->log(), msg.toStdString());
    }
  }
  catch (const std::exception& e)
  {
    QMessageBox::critical(this->parentWidget(), "ERROR", e.what());
    smtkErrorMacro(importOp->log(), e.what());
  }

  this->updateUI();
}

void smtkTruchasMaterialsView::saveToSbiFile()
{
  qtProjectRuntime* runtime = qtProjectRuntime::instance();
  auto project = runtime->project();

  // Run dialog to get input file
  QFileInfo fileInfo(project->location().c_str());
  QString filter("sbi files (*.sbi);;All files (*.*)");
  QFileDialog fileDialog(
    this->parentWidget(), "Select File", fileInfo.absoluteDir().absolutePath(), filter);
  fileDialog.setObjectName("FileSaveDialog");
  fileDialog.setAcceptMode(QFileDialog::AcceptSave);
  fileDialog.setFileMode(QFileDialog::AnyFile);
  fileDialog.setDefaultSuffix("sbi");
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString filePath = fileDialog.selectedFiles()[0];

  // Setup export operation
  // Note: getting operation manager from project manager requires const cast
  const smtk::project::Manager* constManager = project->manager();
  auto projManager = const_cast<smtk::project::Manager*>(constManager);
  smtk::operation::ManagerPtr opManager = projManager->operationManager();

  // smtk::operation::OperationPtr exportOp = opManager->create<smtk::attribute::Export>();
  smtk::operation::OperationPtr exportOp = opManager->create<smtk::attribute::Export>();
  if (!exportOp)
  {
    QString msg("Could not create \"export attribute operation\"");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(this->parentWidget(), "ERROR writing attributes", msg);
    return;
  }

  auto attResource = this->attributeResource();
  std::shared_ptr<smtk::attribute::Attribute> paramsAtt = exportOp->parameters();
  paramsAtt->associate(attResource);
  auto fileItem = paramsAtt->findFile("filename");
  fileItem->setValue(filePath.toStdString());

  // Set option to only write material attributes and functions
  auto groupItem = exportOp->parameters()->findGroup("attribute-collection");
  if (groupItem == nullptr)
  {
    QString msg("Did not find \"attribute-collection\" item");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(this->parentWidget(), "ERROR reading attributes", msg);
    return;
  }
  groupItem->setIsEnabled(true);
  auto typesItem = groupItem->findAs<smtk::attribute::StringItem>("types");
  if (typesItem == nullptr)
  {
    QString msg("Did not find \"attribute-collection/types\" item");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(this->parentWidget(), "ERROR reading attributes", msg);
    return;
  }
  typesItem->setNumberOfValues(3);
  typesItem->setValue(0, "material.real");
  typesItem->setValue(1, "fn.material");
  typesItem->setValue(2, "fn.solid-fraction");

  QString msg;
  QTextStream qs(&msg);
  try
  {
    auto result = exportOp->operate();
    int outcome = result->findInt("outcome")->value();
    if (outcome == static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      qs << "Wrote " << filePath;
      QMessageBox::information(this->parentWidget(), "Success", msg);
    }
    else
    {
      qs << "\"export attribute operation\" failed, outcome " << outcome;
      QMessageBox::critical(this->parentWidget(), "FAILED", msg);
      smtkErrorMacro(exportOp->log(), msg.toStdString());
    }
  }
  catch (const std::exception& e)
  {
    QMessageBox::critical(this->parentWidget(), "ERROR", e.what());
    smtkErrorMacro(exportOp->log(), e.what());
  }
}

void smtkTruchasMaterialsView::onShowCategory()
{
  this->updateUI();
}

void smtkTruchasMaterialsView::updateModelAssociation()
{
  this->updateUI();
}

void smtkTruchasMaterialsView::updateUI()
{
  // Call ListFrame's onShowCategory() to trigger this event chain:
  //   - ListFrame re-selects the current attribute (if any)
  //   - ListFrame emits onAttributeSelectionChanged signal
  //   - signal is connected to smtkTruchasMaterials::onAttributeSelectionChanged()
  //   - which redraws the EditorFrame
  m_internal->ListFrame->onShowCategory();
}

void smtkTruchasMaterialsView::createWidget()
{
  // Display a simplified version of qtAttributeView top frame
  auto attResource = this->attributeResource();
  auto materialDefinition = attResource->findDefinition(m_internal->AttributeType);

  QSplitter* frame = new QSplitter(this->parentWidget());
  frame->setOrientation(Qt::Vertical);

  m_internal->ListFrame =
    new qtAttributeListWidget(frame, this, materialDefinition, isAttributeValid);
  // m_internal->ListFrame->setAttributeValidator(isAttributeValid);
  m_internal->ListFrame->showLoadSaveButtons(true);
  QObject::connect(m_internal->ListFrame, &qtAttributeListWidget::loadButtonClicked, this,
    &smtkTruchasMaterialsView::loadFromSbiFile);
  QObject::connect(m_internal->ListFrame, &qtAttributeListWidget::saveButtonClicked, this,
    &smtkTruchasMaterialsView::saveToSbiFile);

  // Attribute frame
  m_internal->EditorFrame = new QFrame(frame);
  new QVBoxLayout(m_internal->EditorFrame);
  //  m_internal->EditorFrame->setVisible(0);

  frame->addWidget(m_internal->ListFrame);
  frame->addWidget(m_internal->EditorFrame);

  QObject::connect(m_internal->ListFrame, &qtAttributeListWidget::attributeCreated, this,
    &qtBaseAttributeView::attributeCreated);
  QObject::connect(m_internal->ListFrame, &qtAttributeListWidget::attributeRemoved, this,
    &qtBaseAttributeView::attributeRemoved);

  // We want the signals that may change the attribute to be displayed Queued instead of
  // Direct so that QLineEdit::editingFinished signals are processed prior to these.
  QObject::connect(m_internal->ListFrame, &qtAttributeListWidget::attributeSelectionChanged, this,
    &smtkTruchasMaterialsView::onAttributeSelectionChanged, Qt::QueuedConnection);
  QObject::connect(m_internal->ListFrame, &qtAttributeListWidget::attributeNameChanged, this,
    &qtBaseAttributeView::attributeChanged, Qt::QueuedConnection);

  this->Widget = frame;
} // createWidget()

void smtkTruchasMaterialsView::onAttributeChanged()
{
  auto* qAtt = dynamic_cast<qtMaterialAttribute*>(this->sender());
  assert(qAtt);
  auto att = qAtt->attribute();
  m_internal->ListFrame->checkStatus(att);
  emit qtBaseView::modified();
}

void smtkTruchasMaterialsView::onAttributeSelectionChanged(smtk::attribute::AttributePtr att)
{
  m_internal->EditorFrame->setVisible(true);
  if (m_internal->CurrentAtt != nullptr)
  {
    m_internal->EditorFrame->layout()->removeWidget(m_internal->CurrentAtt->widget());
  }
  delete m_internal->CurrentAtt;
  m_internal->CurrentAtt = nullptr;

  if (att != nullptr)
  {
    m_internal->CurrentAtt =
      new qtMaterialAttribute(att, m_internal->AttComponent, m_internal->EditorFrame, this);

    m_internal->CurrentAtt->createBasicLayout(false);
    if (m_internal->CurrentAtt->widget())
    {
      m_internal->EditorFrame->layout()->addWidget(m_internal->CurrentAtt->widget());
      if (this->advanceLevelVisible())
      {
        m_internal->CurrentAtt->showAdvanceLevelOverlay(true);
      }
    }

    QObject::connect(m_internal->CurrentAtt, &qtMaterialAttribute::itemModified, this,
      &smtkTruchasMaterialsView::onItemChanged, Qt::QueuedConnection);
    QObject::connect(m_internal->CurrentAtt, &qtMaterialAttribute::modified, this,
      &smtkTruchasMaterialsView::onAttributeChanged, Qt::QueuedConnection);
  } // if (att)
}

void smtkTruchasMaterialsView::onItemChanged(smtk::extension::qtItem* qitem)
{
  // qDebug() << "onItemChanged";
  if (qitem == nullptr)
  {
    return;
  }
  auto item = qitem->item();
  if (item == nullptr)
  {
    return;
  }
  auto attPtr = item->attribute();
  if (attPtr == nullptr)
  {
    return;
  }

  m_internal->ListFrame->checkStatus(attPtr);
  this->valueChanged(item);
  std::vector<std::string> itemNames{ item->name() };
  this->attributeChanged(attPtr, itemNames);
}
