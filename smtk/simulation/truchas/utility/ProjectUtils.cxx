//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/truchas/utility/ProjectUtils.h"

namespace smtk
{
namespace simulation
{
namespace truchas
{

smtk::resource::ResourcePtr ProjectUtils::getByRole(smtk::project::ProjectPtr project,
  const std::string& role,
  std::string& errMessage) const
{
  errMessage.clear();
  auto resourceSet = project->resources().findByRole(role);
  if (resourceSet.empty())
  {
    std::stringstream ss;
    ss << "Project is missing \"" << role << "\" resource.";
    errMessage = ss.str();
    return smtk::resource::ResourcePtr(nullptr);
  }
  else if (resourceSet.size() > 1)
  {
    std::stringstream ss;
    ss << "Project has multiple \"" << role << "\" resources.";
    errMessage = ss.str();
    return smtk::resource::ResourcePtr(nullptr);
  }
  else
  {
    return *(resourceSet.begin());
  }
}

}
}
}
