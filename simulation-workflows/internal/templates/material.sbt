<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Viscous Flow</Cat>
    <Cat>Void Material</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="background-material" Label="Default Material" BaseType="" Version="0" Unique="true">
      <Categories>
        <Cat>Fluid Flow</Cat>
        <Cat>Heat Transfer</Cat>
        <Cat>Induction Heating</Cat>
        <Cat>Viscous Flow</Cat>
        <Cat>Void Material</Cat>
      </Categories>
      <BriefDescription>Material to use to fill any portion of the mesh not explicitlydefined in the "Body" namelists.</BriefDescription>
      <ItemDefinitions>
        <Component Name="background-material" Label="Select" NumberOfRequiredValues="1" Unique="true" Version="1">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material']"/>
          </Accepts>
        </Component>
      </ItemDefinitions>
    </AttDef>
    <!-- Common base class for "real" and "void" materials-->
    <AttDef Type="material" BaseType="" Abstract="true" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="material.void" BaseType="material" Label="VOID" Version="0">
      <Categories>
        <Cat>Void Material</Cat>
      </Categories>
      <ItemDefinitions>
        <Double Name="void-temperature" Label="Temperature">
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="sound-speed" Label="Speed of Sound" AdvanceLevel="1">
          <BriefDescription>| The adiabatic sound speed that is used in computing thecompressibility of each cell containing the material.
This is not a real sound speed, but a numerical artifice
used to permit collapse of small void bubbles.</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- material.solid is a placeholder for the fluid material_priority attribute-->
    <AttDef Type="material.solid" BaseType="material" Label="SOLID">
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
    </AttDef>
    <AttDef Type="material.real" BaseType="material" Label="Material" RootName="Material" Version="1">
      <ItemDefinitions>
        <!-- Invariant properties are the same for all phases-->
        <Group Name="invariant-properties" Label=" ">
          <ItemDefinitions>
            <Double Name="density" Label="Density (rho)">
              <Categories>
                <Cat>Fluid Flow</Cat>
                <Cat>Heat Transfer</Cat>
                <Cat>Induction Heating</Cat>
                <Cat>Viscous Flow</Cat>
                <Cat>Void Material</Cat>
              </Categories>
              <BriefDescription>Mass density of the material phase</BriefDescription>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="reference-enthalpy" Label="Reference Enthalpy" AdvanceLevel="1">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="reference-temperature" Label="Reference Temperature" AdvanceLevel="1">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <BriefDescription>Reference Temperature for Enthalpy calculations</BriefDescription>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="shared-properties" Label="Shared Properties" Version="0">
          <ItemDefinitions>
            <String Name="enthalpy" Label="Enthalpy" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Double Name="specific-enthalpy" Label="Specific Enthalpy Function (h)">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <BriefDescription>Specific enthalpy must be specified as a function.</BriefDescription>
                  <ExpressionType>fn.material.specific-enthalpy</ExpressionType>
                </Double>
                <Double Name="specific-heat" Label="Specific Heat (Cp)">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <BriefDescription>Specific heat of the material phase</BriefDescription>
                  <ExpressionType>fn.material.specific-heat</ExpressionType>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                  </RangeInfo>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo>
                <Structure>
                  <Value Enum="Specific Enthalpy Function">specific-enthalpy</Value>
                  <Items>
                    <Item>specific-enthalpy</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Specific Heat">specific-heat</Value>
                  <Items>
                    <Item>specific-heat</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <Double Name="conductivity" Label="Conductivity (K)" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <BriefDescription>Thermal conductivity of the material phase</BriefDescription>
              <ExpressionType>fn.material.conductivity</ExpressionType>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="electrical-conductivity" Label="Electrical Conductivity" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <ExpressionType>fn.material.electrical-conductivity</ExpressionType>
            </Double>
            <Double Name="electric-susceptibility" Label="Electric Susceptibility" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="magnetic-susceptibility" Label="Magnetic Susceptibility" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="phases" Label="Phases" Extensible="true" NumberOfRequiredGroups="1" Version="1">
          <ItemDefinitions>
            <String Name="name" Label="Name">
              <Categories>
                <Cat>Fluid Flow</Cat>
                <Cat>Heat Transfer</Cat>
                <Cat>Induction Heating</Cat>
                <Cat>Viscous Flow</Cat>
                <Cat>Void Material</Cat>
              </Categories>
            </String>
            <String Name="enthalpy" Label="Enthalpy" Optional="true" IsEnabledByDefault="true">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Double Name="specific-enthalpy" Label="Specific Enthalpy Function (h)">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <BriefDescription>Specific enthalpy must be specified as a function.</BriefDescription>
                  <ExpressionType>fn.material.specific-enthalpy</ExpressionType>
                </Double>
                <Double Name="specific-heat" Label="Specific Heat (Cp)">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <BriefDescription>Specific heat of the material phase</BriefDescription>
                  <ExpressionType>fn.material.specific-heat</ExpressionType>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                  </RangeInfo>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo>
                <Structure>
                  <Value Enum="Specific Enthalpy Function">specific-enthalpy</Value>
                  <Items>
                    <Item>specific-enthalpy</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Specific Heat">specific-heat</Value>
                  <Items>
                    <Item>specific-heat</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <Double Name="conductivity" Label="Conductivity (K)" Optional="true" IsEnabledByDefault="true">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <BriefDescription>Thermal conductivity of the material phase</BriefDescription>
              <ExpressionType>fn.material.conductivity</ExpressionType>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="electrical-conductivity" Label="Electrical Conductivity" Optional="true" IsEnabledByDefault="true">
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <ExpressionType>fn.material.electrical-conductivity</ExpressionType>
            </Double>
            <Double Name="electric-susceptibility" Label="Electric Susceptibility" AdvanceLevel="1" Optional="true" IsEnabledByDefault="true">
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="magnetic-susceptibility" Label="Magnetic Susceptibility" AdvanceLevel="1" Optional="true" IsEnabledByDefault="true">
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Group Name="fluid" Label="Fluid" Optional="true" IsEnabledByDefault="false">
              <ItemDefinitions>
                <Double Name="density-delta" Label="Density Delta Function" Optional="true">
                  <Categories>
                    <Cat>Fluid Flow</Cat>
                  </Categories>
                  <BriefDescription>The relative deviation of the true temperature-dependent
density from the reference density</BriefDescription>
                  <ExpressionType>fn.material.density-delta</ExpressionType>
                </Double>
                <Double Name="viscosity" Label="Viscosity (nu)">
                  <BriefDescription>The dynamic viscosity of a fluid phase</BriefDescription>
                  <Categories>
                    <Cat>Viscous Flow</Cat>
                  </Categories>
                  <ExpressionType>fn.material.viscosity</ExpressionType>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
        <Group Name="transitions" Label="Transitions" Extensible="true" NumberOfRequiredGroups="0" Version="1">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Viscous Flow</Cat>
            <Cat>Void Material</Cat>
          </Categories>
          <ItemDefinitions>
            <Double Name="latent-heat" Label="Latent Heat (Lf)">
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <String Name="transition-spec" Label="Transition Temperatures">
              <ChildrenDefinitions>
                <Double Name="lower-transition-temperature" Label="Low Transition Temperature (Ts)"/>
                <Double Name="upper-transition-temperature" Label="High Transition Temperature (Tl)"/>
                <Double Name="solid-fraction-table" Label="Table">
                  <ExpressionType>fn.solid-fraction</ExpressionType>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="Low/High">two-temp</Value>
                  <Items>
                    <Item>lower-transition-temperature</Item>
                    <Item>upper-transition-temperature</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Table">table</Value>
                  <Items>
                    <Item>solid-fraction-table</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
