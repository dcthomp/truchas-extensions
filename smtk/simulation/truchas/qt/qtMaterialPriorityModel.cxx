//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/truchas/qt/qtMaterialPriorityModel.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/StringItem.h"

#include <QByteArray>
#include <QDataStream>
#include <QDebug>
#include <QMimeData>
#include <QModelIndex>
#include <QString>
#include <QTextStream>
#include <QVariant>

#include <cassert>
#include <string>

qtMaterialPriorityModel::qtMaterialPriorityModel(
  QObject* parent, smtk::attribute::GroupItemPtr groupItem)
  : QAbstractTableModel(parent)
  , m_groupItem(groupItem)
{
}

qtMaterialPriorityModel::~qtMaterialPriorityModel()
{
}

int qtMaterialPriorityModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
  {
    return 0;
  }
  auto groupItem = m_groupItem.lock();
  return groupItem == nullptr ? 0 : static_cast<int>(groupItem->numberOfGroups());
}

int qtMaterialPriorityModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid())
  {
    return 0;
  }
  return 3; // material, phase, material:phase
}

QVariant qtMaterialPriorityModel::data(const QModelIndex& index, int role) const
{
  int row = index.row();
  int col = index.column();
  QVariant qValue; // return value

  assert(index.model() == this);
  if (!index.isValid())
  {
    return qValue;
  }

  // Skip everything else except display role for now
  if (role != Qt::DisplayRole && role != Qt::EditRole)
  {
    // qWarning() << "Unsupported role:" << role;
    return QVariant();
  }

  auto groupItem = m_groupItem.lock();
  if (groupItem == nullptr)
  {
    return qValue;
  }

  if (row >= groupItem->numberOfGroups())
  {
    return qValue;
  }

  auto refItem = groupItem->findAs<smtk::attribute::ComponentItem>(row, "material");
  auto compItem = refItem->value();
  if (compItem == nullptr)
  {
    return qValue;
  }
  auto matItem = std::dynamic_pointer_cast<smtk::attribute::Attribute>(compItem);

  // SOLID is special case
  if (matItem->type() == "material.solid")
  {
    QString s;
    switch (col)
    {
      case 0:
        s = "SOLID";
        break;
      case 1:
        break;
      case 2:
        s = "SOLID";
        break;
    }
    qValue = s;
    return qValue;
  }

  // VOID is special case
  if (matItem->type() == "material.void")
  {
    QString s;
    switch (col)
    {
      case 0:
        s = "VOID";
        break;
      case 1:
        break;
      case 2:
        s = "VOID";
        break;
    }
    qValue = s;
    return qValue;
  }

  // Locate fluid phase item (code currently presumes there is only one)
  std::string fluidPhaseName;
  auto phasesItem = matItem->findGroup("phases");
  std::size_t numPhases = phasesItem->numberOfGroups();
  bool isFluid = false;
  for (std::size_t i = 0; i < numPhases; ++i)
  {
    auto fluidItem = phasesItem->find(i, "fluid");
    if (fluidItem->isEnabled())
    {
      if (numPhases == 1)
      {
        fluidPhaseName = matItem->name();
      }
      else
      {
        auto nameItem = phasesItem->findAs<smtk::attribute::StringItem>(i, "name");
        fluidPhaseName = nameItem->value();
      }
      isFluid = true;
      break;
    }
  }

  // Get DisplayRole reply
  switch (col)
  {
    case 0: // material name
    {
      qValue.setValue(QString::fromStdString(matItem->name()));
    }
    break;

    case 1: // fluid phase name
    {
      QString phaseName =
        fluidPhaseName.empty() ? "(not found)" : QString::fromStdString(fluidPhaseName);
      qValue.setValue(phaseName);
    }
    break;

    case 2: // material:phase
    {
      // One of: (i) material, (ii) material INVALID (not fluid), or (iii) material:fluid-phase
      QString combinedName;
      QTextStream qs(&combinedName);
      if (!isFluid)
      {
        qs << matItem->name().c_str() << ": INVALID (NOT FLUID)";
      }
      else if (numPhases == 1)
      {
        combinedName = matItem->name().c_str();
      }
      else
      {
        qs << matItem->name().c_str() << ":" << fluidPhaseName.c_str();
      }
      qValue.setValue(combinedName);
    }
    break;

    default:
      qWarning() << "Unrecognized column" << col;
      break;
  } // switch

  return qValue;
}

Qt::ItemFlags qtMaterialPriorityModel::flags(const QModelIndex& index) const
{
  if (!index.isValid())
  {
    return Qt::ItemIsDropEnabled;
  }

  auto f = Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
  return f;
}

QVariant qtMaterialPriorityModel::headerData(
  int section, Qt::Orientation orientation, int role) const
{
  if ((role != Qt::DisplayRole) || (orientation == Qt::Vertical))
  {
    return Superclass::headerData(section, orientation, role);
  }

  // Qt::Horizontal
  switch (section)
  {
    case 0:
      return QVariant("Material");
      break;
    case 1:
      return QVariant("Phase");
      break;
    case 2:
      return QVariant("Material Priority");
      break;

    default:
      qWarning() << __FILE__ << ":" << __LINE__ << "Unrecognized section" << section;
      break;
  }

  return QVariant();
}

bool qtMaterialPriorityModel::dropMimeData(
  const QMimeData* mimeData, Qt::DropAction action, int row, int column, const QModelIndex& parent)
{
  // qDebug() << "dropMimeData row" << row << "column" << column << "Parent" << parent << "isValid?" << parent.isValid();
  auto priorityGroup = m_groupItem.lock();
  if (priorityGroup == nullptr)
  {
    return false;
  }

  // Validate source
  const QString MODEL_FORMAT("application/x-qabstractitemmodeldatalist");
  if (parent.isValid() && parent.model() != this)
  {
    qWarning() << "Unexpected drop source:" << parent << "set to row" << parent.row();
    //return false;
  }
  if (!mimeData->hasFormat(MODEL_FORMAT))
  {
    qWarning() << "unexpected drop source: mimedata missing format" << MODEL_FORMAT;
    return false;
  }
  if (!mimeData->hasText() || mimeData->text() != "qtMaterialPriorityModel")
  {
    qWarning() << "unexpected drop source: mimedata missing text \"qtMaterialPriorityModel\"";
    return false;
  }

  // Handle unexpected/undocumented case when parent is valid
  if (parent.isValid())
  {
    row = parent.row();
    // qDebug() << "parent is valid; changing row to" << row;
  }

  // Parse mime data to obtain the row that was dragged.
  QByteArray encoded = mimeData->data(MODEL_FORMAT);
  QDataStream stream(&encoded, QIODevice::ReadOnly);
  int dragRow;
  stream >> dragRow;

  int dropRow = row >= 0 ? row : this->rowCount();
  // qDebug() << "Assign from row" << dragRow << "to" << dropRow << "rowCount" << this->rowCount();

  QModelIndex parentIndex;
  int dropGroupPosition = dropRow;
  int rowCount = this->rowCount();
  if (row < 0)
  {
    dropRow = rowCount;
    dropGroupPosition = dropRow - 1;
  }

  // Moving rows down requires special logic
  if (dropRow > dragRow)
  {
    dropRow = dropRow >= rowCount ? rowCount : dropRow + 1;
  }

  bool valid = this->beginMoveRows(parentIndex, dragRow, dragRow, parentIndex, dropRow);
  qWarning() << "beginMoveRows() valid?" << (bool)valid;
  if (valid)
  {
    priorityGroup->rotate(dragRow, dropGroupPosition);
    this->endMoveRows();
    emit this->modified(priorityGroup, false);
  }

  return valid;
}

QMimeData* qtMaterialPriorityModel::mimeData(const QModelIndexList& indexes) const
{
  // Insert text to indicate the source
  auto mimeData = Superclass::mimeData(indexes);
  mimeData->setText("qtMaterialPriorityModel");
  return mimeData;
}

bool qtMaterialPriorityModel::isValid(
  const std::set<smtk::attribute::AttributePtr>& materialAtts) const
{
  auto priorityGroup = m_groupItem.lock();
  if (priorityGroup == nullptr)
  {
    return false;
  }

  // Copy input set to a working set
  auto workingSet = materialAtts;
  for (std::size_t i = 0; i < priorityGroup->numberOfGroups(); ++i)
  {
    auto refItem = priorityGroup->findAs<smtk::attribute::ComponentItem>(i, "material");
    auto matItem = std::dynamic_pointer_cast<smtk::attribute::Attribute>(refItem->value());
    if ((matItem == nullptr) || (materialAtts.find(matItem) == materialAtts.end()))
    {
      // If matItem is null, then the material item is no longer in the attribute resource.
      // If matItem is not in materialAtts, then it is no longer referenced by a body or inflow.
      return false;
    }
    workingSet.erase(matItem); // remove from the working set
  }

  // If we reach here, the working set should be empty.
  bool valid = workingSet.empty();
  return valid;
}

void qtMaterialPriorityModel::update(const std::set<smtk::attribute::AttributePtr>& materialAtts)
{
  auto priorityGroup = m_groupItem.lock();
  if (priorityGroup == nullptr)
  {
    return;
  }

  // Traverse priorityGroup item and prune any items not in the
  // materialAtts set.
  // This is when a material attribute is deleted, or when a
  // material attribute is modified to remove its fluid phase.

  // Copy material atts into a working set, which will be used to
  // detect new/unassigned material attributes.
  auto workingSet = materialAtts;

  // Keep list of rows to delete
  std::vector<int> rowsToDelete;
  bool priorityGroupModified = false;
  for (std::size_t i = 0; i < priorityGroup->numberOfGroups(); ++i)
  {
    auto refItem = priorityGroup->findAs<smtk::attribute::ComponentItem>(i, "material");
    auto matItem = std::dynamic_pointer_cast<smtk::attribute::Attribute>(refItem->value());
    if ((matItem == nullptr) || (materialAtts.find(matItem) == materialAtts.end()))
    {
      int row = static_cast<int>(i);
      // Insert at begining so that rows are sorted largest -> smallest
      rowsToDelete.insert(rowsToDelete.begin(), row);
      continue;
    }

    workingSet.erase(matItem); // remove from the working set
  }

  // Delete rows identified in last loop (already ordered last to first)
  QModelIndex parentIndex;
  for (auto row : rowsToDelete)
  {
#ifndef DEBUG
    qDebug() << "Removing material priority item" << row;
#endif
    this->beginRemoveRows(parentIndex, row, row);
    if (!priorityGroup->removeGroup(row))
    {
      qWarning() << "Failed to remove subgroup" << row << "from material_priority attribute";
    }
    priorityGroupModified = true;
    this->endRemoveRows();
  }

  // Add rest of workingSet to priority group
  std::size_t numNew_t = workingSet.size();
  if (numNew_t > 0)
  {
    int numNew = static_cast<int>(numNew_t);
    std::size_t numGroups_t = priorityGroup->numberOfGroups();
    int first = static_cast<int>(numGroups_t);
    int last = first + numNew - 1;
    this->beginInsertRows(parentIndex, first, last);
    if (!priorityGroup->insertGroups(numGroups_t, numNew_t))
    {
      qWarning() << "unable to insert " << numNew << "group(s) at position" << numGroups_t;
    }
    else
    {
      priorityGroupModified = true;
      std::size_t pos = numGroups_t;
      for (auto matAtt : workingSet)
      {
        auto refItem = priorityGroup->findAs<smtk::attribute::ComponentItem>(pos, "material");
        if (!refItem->setValue(0, matAtt))
        {
          qWarning() << "unable to set reference item to material attribute"
                     << matAtt->name().c_str();
        }
        ++pos;
      }
    } // else

    this->endInsertRows();
  }

  if (priorityGroupModified)
  {
    // Todo display alert icon and "Confirm" button
    emit this->modified(priorityGroup, true);
  }
}
