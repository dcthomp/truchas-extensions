//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

// Local incluides
#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/Registrar.h"
#include "smtk/simulation/truchas/operations/Export.h"
#include "smtk/simulation/truchas/utility/AttributeUtils.h"
#include "smtk/simulation/truchas/utility/ProjectUtils.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/model/Registrar.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/project/Registrar.h"
#include "smtk/project/operators/Read.h"
#include "smtk/resource/Manager.h"
#include "smtk/session/vtk/Registrar.h"

#include "smtk/common/testing/cxx/helpers.h"

#include <boost/filesystem.hpp>

#include <iostream>
#include <string>
#include <vector>

const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

int TestExportProject(int /*argc*/, char* /*argv*/[])
{
  // Remove output folder
  std::stringstream ss;
  ss << SCRATCH_DIR << "/cxx"
     << "/export_project";
  std::string outputFolder = ss.str();
  if (boost::filesystem::is_directory(outputFolder))
  {
    boost::filesystem::remove_all(outputFolder);
  }
  boost::filesystem::create_directories(outputFolder);

  // Create sundry managers
  smtk::resource::ManagerPtr resourceManager = smtk::resource::Manager::create();
  smtk::attribute::Registrar::registerTo(resourceManager);
  smtk::model::Registrar::registerTo(resourceManager);
  smtk::session::vtk::Registrar::registerTo(resourceManager);

  smtk::operation::ManagerPtr operationManager = smtk::operation::Manager::create();
  smtk::operation::Registrar::registerTo(operationManager);
  smtk::attribute::Registrar::registerTo(operationManager);
  smtk::session::vtk::Registrar::registerTo(operationManager);

  smtk::project::ManagerPtr projectManager =
    smtk::project::Manager::create(resourceManager, operationManager);
  smtk::project::Registrar::registerTo(projectManager);
  smtk::simulation::truchas::Registrar::registerTo(projectManager);

  // Todo Next line is currently a no-op. Is it needed?
  smtk::project::Registrar::registerTo(operationManager);
  operationManager->registerResourceManager(resourceManager);

  // Application must set workflows directory
  smtk::simulation::truchas::Metadata::WORKFLOWS_DIRECTORY = WORKFLOWS_SOURCE_DIR;

  // Read project from file system
  std::shared_ptr<smtk::project::Project> project;
  {
    boost::filesystem::path scratchFolder(SCRATCH_DIR);
    boost::filesystem::path projectFolder = scratchFolder / "cxx/import_model";
    boost::filesystem::path projectFilename = projectFolder / "import_model.project.smtk";
    std::cout << "Read project file " << projectFilename.string() << std::endl;

    auto readOp = operationManager->create<smtk::project::Read>();
    test(readOp != nullptr);
    test(readOp->parameters()->findFile("filename")->setValue(projectFilename.string()));
    auto result = readOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Read Outcome: " << outcome << std::endl;
    if (outcome != OP_SUCCEEDED)
    {
      std::cout << readOp->log().convertToString() << std::endl;
    }
    test(outcome == OP_SUCCEEDED);

    smtk::attribute::ResourceItemPtr projectItem = result->findResource("resource");
    smtk::resource::ResourcePtr resource = projectItem->value();
    test(resource != nullptr);
    project = std::dynamic_pointer_cast<smtk::project::Project>(resource);
    test(project != nullptr);
  }

  smtk::simulation::truchas::ProjectUtils projectUtils;

  // Run export operator
  {
    auto exportOp = operationManager->create<smtk::simulation::truchas::Export>();
    test(exportOp->parameters()->associate(project));

    // Set the "analysis" ResourceItem
    std::string analysisRole("Analysis1");
    auto attResource = projectUtils.getByRole<smtk::attribute::Resource>(project, analysisRole);
    test(attResource != nullptr);
    test(exportOp->parameters()->findResource("analysis")->setValue(attResource));

    // Set the "analysis-code" StringItem
    smtk::simulation::truchas::AttributeUtils attUtils;
    auto analysisAtt = attUtils.getAnalysisAtt(attResource);
    test(analysisAtt != nullptr);
    auto analysisItem = analysisAtt->findString("Analysis");
    // exportOp->parameters()->find("analysis-code", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE)->assign(analysisItem);
    std::string analysisCode = analysisItem->value();
    exportOp->parameters()->findString("analysis-code")->setValue(analysisCode);

    // Set the "output-file" FileItem
    std::string outputFile = outputFolder + "/heat-transfer.inp";
    std::cout << "outputFile: " << outputFile << std::endl;
    auto fileItem = exportOp->parameters()->findAs<smtk::attribute::FileItem>(
      "output-file", smtk::attribute::SearchStyle::RECURSIVE_ACTIVE);
    test(fileItem != nullptr);
    test(fileItem->setValue(outputFile));

    // Set the "test-mode" VoidItem
    exportOp->parameters()->find("test-mode")->setIsEnabled(true);

    auto result = exportOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Export Outcome: " << outcome << std::endl;
    if (outcome != OP_SUCCEEDED)
    {
      std::cout << exportOp->log().convertToString() << std::endl;
    }
    test(outcome == OP_SUCCEEDED);

    // Verify that output file was created
    test(boost::filesystem::exists(outputFile));
  }

  // Run the export operator again
  // Note: This second run fails (hangs) when built with the default smtk, which
  // is configured to run operations in separate threads.
  {
    // Also enable induction-heating

    std::cout << "START SECOND EXPORT" << std::endl;
    auto exportOp = operationManager->create<smtk::simulation::truchas::Export>();
    test(exportOp->parameters()->associate(project));

    // Set the "analysis" ResourceItem
    std::string analysisRole("Analysis1");
    auto attResource = projectUtils.getByRole<smtk::attribute::Resource>(project, analysisRole);
    test(attResource != nullptr);
    test(exportOp->parameters()->findResource("analysis")->setValue(attResource));

    // Set the "analysis-code" StringItem
    smtk::simulation::truchas::AttributeUtils attUtils;
    auto analysisAtt = attUtils.getAnalysisAtt(attResource);
    test(analysisAtt != nullptr);
    auto analysisItem = analysisAtt->findString("Analysis");
    std::string analysisCode = analysisItem->value();
    exportOp->parameters()->findString("analysis-code")->setValue(analysisCode);

    auto item = analysisItem->findChild("Truchas", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    auto truchasItem = std::dynamic_pointer_cast<smtk::attribute::GroupItem>(item);
    test(truchasItem != nullptr);

    auto htItem = truchasItem->findAs<smtk::attribute::GroupItem>(0, "Heat Transfer");
    test(htItem != nullptr);
    htItem->setIsEnabled(true);

    auto ihItem = htItem->find(0, "Induction Heating");
    test(ihItem != nullptr);
    ihItem->setIsEnabled(true);

    // When IH is set, should also set the domain type
    // (not required, but would otherwise set off a warning during export)
    auto emAtts = attResource->findAttributes("electromagnetics");
    test(emAtts.size() == 1);
    auto emAtt = emAtts[0];
    auto domainTypeItem = emAtt->findString("em-domain-type");
    test(domainTypeItem != nullptr);
    test(domainTypeItem->setValue("quarter_cylinder"));

    // Set the "output-file" FileItem
    std::string outputFile = outputFolder + "/induction-heating.inp";
    std::cout << "outputFile: " << outputFile << std::endl;
    auto fileItem = exportOp->parameters()->findAs<smtk::attribute::FileItem>(
      "output-file", smtk::attribute::SearchStyle::RECURSIVE_ACTIVE);
    test(fileItem != nullptr);
    test(fileItem->setValue(outputFile));

    // Set the "test-mode" VoidItem
    exportOp->parameters()->find("test-mode")->setIsEnabled(true);

    auto result = exportOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Export Outcome: " << outcome << std::endl;
    if (outcome != OP_SUCCEEDED)
    {
      std::cout << exportOp->log().convertToString() << std::endl;
    }
    test(outcome == OP_SUCCEEDED);

    // Verify that output file was created
    test(boost::filesystem::exists(outputFile));
  }

  return 0;
}
