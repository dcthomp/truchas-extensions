<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="thermal-bc" Label="Thermal BC" RootName="ThermalBC">
      <ItemDefinitions>
        <Double Name="Temperature">
          <ExpressionType>fn.ht.temp</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.ht" Abstract="true"></AttDef>
    <AttDef Type="fn.ht.temp" Label="Temperature" BaseType="fn.ht" RootName="temp-func">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <String Name="independent-variable" Label="Independent Variable" NumberOfRequiredValues="1">
              <DiscreteInfo>
                <Value Enum="t (time)">t</Value>
                <Value>x</Value>
                <Value>y</Value>
                <Value>z</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Indep. Variable" NumberOfRequiredValues="1"></Double>
                <Double Name="Value" Label="Temperature" NumberOfRequiredValues="1"></Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Coordinates (t,x,y,z)" NumberOfRequiredValues="4">
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(t,x,y,z))" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1"></Double>
                <Int Name="Exponent" Label="Exponents t,x,y,z)" NumberOfRequiredValues="4">
                  <ComponentLabels>
                    <Label>t</Label>
                    <Label>x</Label>
                    <Label>y</Label>
                    <Label>z</Label>
                  </ComponentLabels>
                  <DefaultValue>0</DefaultValue>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="1">
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>independent-variable</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Group" Title="HT Functions" TopLevel="true" TabPosition="North" FilterByAdvanceLevel="false" FilterByCategory="false">
      <Views>
        <View Title="Boundary Conditions"/>
        <View Title="Functions"/>
      </Views>
    </View>
    <View Type="Attribute" Title="Boundary Conditions">
      <AttributeTypes>
        <Att Type="thermal-bc"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Functions">
      <AttributeTypes>
        <Att Type="fn.ht"></Att>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>