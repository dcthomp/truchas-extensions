//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqTruchasProjectCloseBehavior_h
#define pqTruchasProjectCloseBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqTruchasProjectCloseReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqTruchasProjectCloseReaction(QAction* parent);

  void closeProject();

signals:
  void projectClosed();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->closeProject(); }

private:
  Q_DISABLE_COPY(pqTruchasProjectCloseReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqTruchasProjectCloseBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqTruchasProjectCloseBehavior* instance(QObject* parent = nullptr);
  ~pqTruchasProjectCloseBehavior() override;

protected:
  pqTruchasProjectCloseBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqTruchasProjectCloseBehavior);
};

#endif
