//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqTruchasProjectExportBehavior.h"

#include "smtk/simulation/truchas/operations/Export.h"
#include "smtk/simulation/truchas/qt/qtProjectRuntime.h"
#include "smtk/simulation/truchas/utility/AttributeUtils.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtOperationDialog.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/project/ResourceContainer.h"

// Paraview client
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QGridLayout>
#include <QMessageBox>
#include <QSpacerItem>
#include <QString>
#include <QTextStream>
#include <QtGlobal>

#include "nlohmann/json.hpp"

#include "boost/filesystem.hpp"

#include <algorithm> // std::replace
#include <cassert>

using json = nlohmann::json;

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

//-----------------------------------------------------------------------------
pqTruchasProjectExportReaction::pqTruchasProjectExportReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqTruchasProjectExportReaction::onTriggered()
{
  pqTruchasProjectExportBehavior::instance()->exportProject();
}

//-----------------------------------------------------------------------------
void pqTruchasProjectExportBehavior::exportProject()
{
  auto project = qtProjectRuntime::instance()->project();
  assert(project != nullptr);

  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

// This version only works with a single attribute resource in the project
#if 0
  // Todo smtk::project::ResourceContainer::find() won't compile:
  auto attResourceSet = project->resources().find<smtk::attribute::Resource>();
#else
  // So do it brute force instead
  std::set<smtk::attribute::ResourcePtr> attResourceSet;
  for (auto iter = project->resources().begin(); iter != project->resources().end(); ++iter)
  {
    auto resource = *iter;
    if (resource->isOfType<smtk::attribute::Resource>())
    {
      auto attResource = std::dynamic_pointer_cast<smtk::attribute::Resource>(resource);
      attResourceSet.insert(attResource);
    }
  }
#endif

  QString errMsg;
  if (attResourceSet.empty())
  {
    errMsg = "Internal error - project missing analysis (attribute) resource.";
  }
  else if (attResourceSet.size() > 1)
  {
    // Future: add dialog to select the analysis.
    // For now:
    errMsg = "Internal error - project has mulitple attribute resources"
             ", which is not currently supported.";
  }
  if (!errMsg.isEmpty())
  {
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "Error", errMsg);
    return;
  }

  const auto attResource = *attResourceSet.begin();

  // Get the analysis type
  smtk::simulation::truchas::AttributeUtils attUtils;
  auto analysisAtt = attUtils.getAnalysisAtt(attResource);
  assert(analysisAtt != nullptr);
  auto analysisItem = analysisAtt->findString("Analysis");
  assert(analysisItem != nullptr);
  if (!analysisItem->isSet())
  {
    errMsg = "You must first select an analysis type in the simulation attributes.";
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "Error", errMsg);
    return;
  }
  std::string analysis = analysisItem->value();

  // Set export parameters
  auto exportOp = opManager->create<smtk::simulation::truchas::Export>();
  assert(exportOp != nullptr);
  if (!exportOp->parameters()->associate(project))
  {
    errMsg = "Failed to associate project to export operation.";
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "Error", errMsg);
    return;
  }

  auto resourceItem = exportOp->parameters()->findResource("analysis");
  resourceItem->setValue(attResource);

  auto analysisCodeItem = exportOp->parameters()->findString("analysis-code");
  analysisCodeItem->setValue(analysis);

  // Set path to project export folder
  std::string projectLocation = project->location();
  assert(!projectLocation.empty());

  boost::filesystem::path projectFilePath(projectLocation);
  boost::filesystem::path projectPath = projectFilePath.parent_path();
  boost::filesystem::path exportPath = projectPath / "export";

  if (analysis == "Truchas")
  {
    boost::filesystem::path inpFilePath = exportPath / "truchas.inp";
    auto fileItem = exportOp->parameters()->findFile("output-file");
    if (fileItem != nullptr)
    {
      fileItem->setValue(inpFilePath.string());
    }
  }
  else if (analysis == "Genre")
  {
    auto dirItem = exportOp->parameters()->findDirectory("output-folder");
    if (dirItem != nullptr)
    {
      dirItem->setValue(exportPath.string());
    }
  }

  // Construct a modal dialog for the operation spec
  QSharedPointer<smtk::extension::qtOperationDialog> dialog =
    QSharedPointer<smtk::extension::qtOperationDialog>(
      new smtk::extension::qtOperationDialog(exportOp, wrapper->smtkResourceManager(),
        wrapper->smtkViewManager(), pqCoreUtilities::mainWidget()));
  dialog->setObjectName("ExportProjectDialog");
  dialog->setWindowTitle("Generate Genre/Truchas Input File");
  dialog->resize(640, 480);

  QObject::connect(dialog.get(), &smtk::extension::qtOperationDialog::operationExecuted, this, &pqTruchasProjectExportBehavior::onOperationExecuted);
  dialog->exec();
} // exportProject()

//-----------------------------------------------------------------------------
void pqTruchasProjectExportBehavior::onOperationExecuted(
  const smtk::operation::Operation::Result& result)
{
  if (result->findInt("outcome")->value() != OP_SUCCEEDED)
  {
    QMessageBox msgBox(pqCoreUtilities::mainWidget());
    msgBox.setStandardButtons(QMessageBox::Ok);
    // Create a spacer so it doesn't look weird
    QSpacerItem* horizontalSpacer =
      new QSpacerItem(300, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
    msgBox.setText("Export failed. See the \"Output Messages\" view for more details.");
    QGridLayout* layout = (QGridLayout*)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();
    return;
  }

  qInfo() << "Export operation completed";

  // Check for warnings - some scripts use a string item
  std::size_t numWarnings = 0;
  auto warningsItem = result->findString("warnings");
  if (warningsItem && warningsItem->numberOfValues() > 0)
  {
    numWarnings = warningsItem->numberOfValues();
    for (std::size_t i = 0; i < numWarnings; ++i)
    {
      qWarning() << QString::fromStdString(warningsItem->value(i));
    }
  }

  // Alternatively check log item
  if (warningsItem == nullptr)
  {
    auto logItem = result->findString("log");
    std::string logString = logItem->value(0);
    if (!logString.empty())
    {
      auto jsonLog = json::parse(logString);
      assert(jsonLog.is_array());
      for (json::iterator it = jsonLog.begin(); it != jsonLog.end(); ++it)
      {
        auto jsRecord = *it;
        assert(jsRecord.is_object());
        if (jsRecord["severity"] >= static_cast<int>(smtk::io::Logger::Warning))
        {
          numWarnings++;
          std::string content = jsRecord["message"];
          // Escape any quote signs (formats better)
          std::replace(content.begin(), content.end(), '"', '\"');
          qWarning("%zu. %s", numWarnings, content.c_str());
        } // if (>= warning)
      }   // for (it)
    }     // if (logString)
  }       // if (warningsItem)
  qDebug() << "Number of warnings:" << numWarnings;

  if (numWarnings > 0)
  {
    QWidget* parentWidget = pqCoreUtilities::mainWidget();
    QString text;
    QTextStream qs(&text);
    qs << "WARNING: The generated file is INCOMPLETE or INVALID."
       << " You can find more details in the Output Messages view."
       << " You will generally need to correct all invalid input fields"
       << " in the Attribute Editor in order to generate a valid"
       << " Truchas input file."
       << " Also look in the Attribute Editor panel for red \"alert\""
       << " icons and input fields with red background."
       << "\n\nNumber of errors: " << numWarnings;
    QMessageBox msgBox(
      QMessageBox::NoIcon, "Export Warnings", text, QMessageBox::NoButton, parentWidget);
    // Todo connect alert icon
    // msgBox.setIconPixmap(QIcon(ALERT_ICON_PATH).pixmap(32, 32));
    msgBox.exec();
  }
}

//-----------------------------------------------------------------------------
static pqTruchasProjectExportBehavior* g_instance = nullptr;

pqTruchasProjectExportBehavior::pqTruchasProjectExportBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqTruchasProjectExportBehavior* pqTruchasProjectExportBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqTruchasProjectExportBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqTruchasProjectExportBehavior::~pqTruchasProjectExportBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
