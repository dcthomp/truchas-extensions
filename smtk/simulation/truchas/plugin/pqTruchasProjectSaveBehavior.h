//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqTruchasProjectSaveBehavior_h
#define pqTruchasProjectSaveBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqTruchasProjectSaveReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqTruchasProjectSaveReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqTruchasProjectSaveReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqTruchasProjectSaveBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqTruchasProjectSaveBehavior* instance(QObject* parent = nullptr);
  ~pqTruchasProjectSaveBehavior() override;

  // Return value indicates whether or not save succeeded
  bool saveProject();

protected:
  pqTruchasProjectSaveBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqTruchasProjectSaveBehavior);
};

#endif
