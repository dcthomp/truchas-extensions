//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/truchas/qt/qtAttributeListWidget.h"

#include "smtk/simulation/truchas/utility/MaterialAttributeUtils.h"

#include "smtk/attribute/Definition.h"
#include "smtk/attribute/Resource.h"
#include "smtk/extension/qt/qtBaseView.h"
#include "smtk/extension/qt/qtTableWidget.h"
#include "smtk/extension/qt/qtTypeDeclarations.h"
#include "smtk/extension/qt/qtUIManager.h"

#include <QComboBox>
#include <QDebug>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QIcon>
#include <QLabel>
#include <QMessageBox>
#include <QPointer>
#include <QPushButton>
#include <QSizePolicy>
#include <QSpacerItem>
#include <QStringList>
#include <QVBoxLayout>
#include <QVariant>
#include <Qt>

#include <set>

#define ALERT_ICON_PATH ":/icons/attribute/errorAlert.png"

namespace
{
const int status_column = 0;
const int name_column = 1;
const int type_column = 2;
const int color_column = 3;
};

using namespace smtk::attribute;
using namespace smtk::extension;

class qtAttributeListWidgetInternals
{
public:
  qtTableWidget* ListTable;
  QPushButton* AddButton;
  QComboBox* DefsCombo;
  QLabel* DefLabel;
  QPushButton* DeleteButton;
  QPushButton* CopyButton;

  // For loading/saving .sbi files with designated attribute type
  QPushButton* LoadButton;
  QPushButton* SaveButton;

  QFrame* ButtonsFrame;
  QFrame* ListFrame;

  // smtk::attribute::ResourcePtr AttributeResource;
  DefinitionPtr AttDef;
  AttributePtr CurrentAtt;
  QPointer<qtBaseView> View;
  std::function<bool(smtk::attribute::AttributePtr)> IsValid;
  smtk::simulation::truchas::MaterialAttributeUtils m_utils;

  qtAttributeListWidgetInternals() {}
  ~qtAttributeListWidgetInternals() {}
};

qtAttributeListWidget::qtAttributeListWidget(QWidget* _parent, qtBaseView* view,
  smtk::attribute::DefinitionPtr attDefinition, AttributeValidator fn)
  : QWidget(_parent)
  , m_attributeValidator(fn)
{
  // Current implementation only supports 1 concrete definition
  std::vector<smtk::attribute::DefinitionPtr> derivedDefns;
  attDefinition->resource()->findAllDerivedDefinitions(attDefinition, true, derivedDefns);
  assert(derivedDefns.size() == 1);

  this->Internals = new qtAttributeListWidgetInternals;
  this->Internals->View = view;
  this->Internals->AttDef = attDefinition;
  this->initWidget();
}

qtAttributeListWidget::~qtAttributeListWidget()
{
  delete this->Internals;
}

void qtAttributeListWidget::checkStatus(smtk::attribute::AttributePtr attPtr)
{
  // qDebug() << "Checking status for att " << attPtr->name().c_str();
  // Find corresponding row in ListTable and update status column
  for (int row = 0; row < this->Internals->ListTable->rowCount(); ++row)
  {
    QTableWidgetItem* item = this->Internals->ListTable->item(row, name_column);
    smtk::attribute::Attribute* listAtt = item
      ? static_cast<smtk::attribute::Attribute*>(item->data(Qt::UserRole).value<void*>())
      : nullptr;
    if (listAtt == attPtr.get())
    {
      bool isValid = m_attributeValidator != nullptr
        ? m_attributeValidator(attPtr)
        : attPtr->isValid();
      QTableWidgetItem* statusItem =
        isValid ? nullptr : new QTableWidgetItem(QIcon(ALERT_ICON_PATH), "", smtk_USER_DATA_TYPE);
      this->Internals->ListTable->setItem(row, status_column, statusItem);
      break;
    } // if (listAtt == att)
  }   // for (i)
}

void qtAttributeListWidget::onShowCategory()
{
  int selectedRow = -1;

  // Clear the table
  this->Internals->ListTable->blockSignals(true);
  this->Internals->ListTable->clearContents();
  this->Internals->ListTable->setRowCount(0);

  // Rebuild the table contents
  const ResourcePtr attResource = this->Internals->AttDef->resource();
  std::vector<smtk::attribute::AttributePtr> result;
  attResource->findAttributes(this->Internals->AttDef, result);
  if (result.size())
  {
    selectedRow = -1;
    int row = 0;
    for (auto it = result.begin(); it != result.end(); ++it, ++row)
    {
      QTableWidgetItem* item = this->addAttributeListItem(*it);
      if ((*it)->definition()->advanceLevel())
      {
        item->setFont(this->Internals->View->uiManager()->advancedFont());
      }

      // Check if this is the current (selected) attribute
      if ((*it) == this->Internals->CurrentAtt)
      {
        selectedRow = row;
      }
    } // for (it)
  }   // if (result.size()

  if (selectedRow < 0 && result.size())
  {
    selectedRow = 0;
    this->Internals->CurrentAtt = result[0];
  }

  if (selectedRow >= 0)
  {
    this->Internals->ListTable->selectRow(selectedRow);
  }
  this->Internals->ListTable->blockSignals(false);
  this->Internals->SaveButton->setDisabled(result.empty());
  emit this->attributeSelectionChanged(this->Internals->CurrentAtt);
}

void qtAttributeListWidget::showLoadSaveButtons(bool visible)
{
  this->Internals->LoadButton->setVisible(visible);
  this->Internals->SaveButton->setVisible(visible);
}

void qtAttributeListWidget::initWidget()
{
  auto topLayout = new QVBoxLayout(this);
  topLayout->setMargin(0);
  QSizePolicy fixedSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  QSizePolicy expandingSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  // Table widget
  this->Internals->ListTable = new smtk::extension::qtTableWidget(this);
  this->Internals->ListTable->setSelectionMode(QAbstractItemView::SingleSelection);
  this->Internals->ListTable->setSelectionBehavior(QAbstractItemView::SelectRows);
  this->Internals->ListTable->setSizePolicy(expandingSizePolicy);
  this->Internals->ListTable->setColumnCount(2); // columns: status, name

  QStringList headerLabels;
  headerLabels << ""
               << "Name";
  this->Internals->ListTable->setHorizontalHeaderLabels(headerLabels);
  this->Internals->ListTable->horizontalHeader()->setSectionResizeMode(
    QHeaderView::ResizeToContents);
  this->Internals->ListTable->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);

  // Buttons frame
  this->Internals->ButtonsFrame = new QFrame(this);
  QHBoxLayout* buttonLayout = new QHBoxLayout(this->Internals->ButtonsFrame);
  buttonLayout->setMargin(0);
  this->Internals->ButtonsFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

  this->Internals->AddButton = new QPushButton("New", this->Internals->ButtonsFrame);
  this->Internals->AddButton->setSizePolicy(fixedSizePolicy);
  this->Internals->DeleteButton = new QPushButton("Delete", this->Internals->ButtonsFrame);
  this->Internals->DeleteButton->setSizePolicy(fixedSizePolicy);
  this->Internals->CopyButton = new QPushButton("Copy", this->Internals->ButtonsFrame);
  this->Internals->CopyButton->setSizePolicy(fixedSizePolicy);

  this->Internals->LoadButton = new QPushButton("Load File...", this->Internals->ButtonsFrame);
  this->Internals->LoadButton->setSizePolicy(fixedSizePolicy);
  this->Internals->SaveButton = new QPushButton("Save File...", this->Internals->ButtonsFrame);
  this->Internals->SaveButton->setSizePolicy(fixedSizePolicy);

  buttonLayout->addWidget(this->Internals->AddButton);
  buttonLayout->addWidget(this->Internals->CopyButton);
  buttonLayout->addWidget(this->Internals->DeleteButton);

  auto spacer = new QSpacerItem(20, 20, QSizePolicy::Expanding);
  buttonLayout->addSpacerItem(spacer);
  buttonLayout->addWidget(this->Internals->LoadButton);
  buttonLayout->addWidget(this->Internals->SaveButton);

  // Connect load/save buttons to public signals
  QObject::connect(this->Internals->LoadButton, &QPushButton::clicked, this,
    &qtAttributeListWidget::loadButtonClicked);
  QObject::connect(this->Internals->SaveButton, &QPushButton::clicked, this,
    &qtAttributeListWidget::saveButtonClicked);

  // Hide load/save buttons by default
  this->Internals->LoadButton->setVisible(false);
  this->Internals->SaveButton->setVisible(false);

  topLayout->addWidget(this->Internals->ButtonsFrame);
  topLayout->addWidget(this->Internals->ListTable);

  // We want the signals that may change the attribute to be displayed Queued instead of
  // Direct so that QLineEdit::editingFinished signals are processed prior to these.
  QObject::connect(this->Internals->ListTable,
    &smtk::extension::qtTableWidget::itemSelectionChanged, this,
    &qtAttributeListWidget::onAttributeSelectionChanged, Qt::QueuedConnection);
  QObject::connect(this->Internals->ListTable, &smtk::extension::qtTableWidget::itemChanged, this,
    &qtAttributeListWidget::onAttributeNameChanged);
  // We need this so that the attribute name will also be changed
  // when a recorded test is play back, which is using setText
  // on the underline QLineEdit of the cell.
  QObject::connect(this->Internals->ListTable, &qtTableWidget::cellChanged, this,
    &qtAttributeListWidget::onAttributeCellChanged, Qt::QueuedConnection);

  QObject::connect(this->Internals->AddButton, &QPushButton::clicked, this,
    &qtAttributeListWidget::onCreateAttribute);
  QObject::connect(this->Internals->CopyButton, &QPushButton::clicked, this,
    &qtAttributeListWidget::onCopySelected);
  QObject::connect(this->Internals->DeleteButton, &QPushButton::clicked, this,
    &qtAttributeListWidget::onDeleteSelected);

  // Initialize the list contents
  const ResourcePtr attResource = this->Internals->AttDef->resource();
  std::vector<smtk::attribute::AttributePtr> result;
  attResource->findAttributes(this->Internals->AttDef, result);
  if (result.size())
  {
    for (auto it = result.begin(); it != result.end(); ++it)
    {
      QTableWidgetItem* item = this->addAttributeListItem(*it);
      if ((*it)->definition()->advanceLevel())
      {
        item->setFont(this->Internals->View->uiManager()->advancedFont());
      }
    } // for (it)
    this->Internals->ListTable->selectRow(0);
  } // if (result.size())

  this->Internals->SaveButton->setDisabled(result.empty());
}

QTableWidgetItem* qtAttributeListWidget::addAttributeListItem(
  smtk::attribute::AttributePtr attribute)
{
  // Use MaterialAttUtils to make sure att is configured correctly
  this->Internals->m_utils.configureCopiedAttribute(attribute);

  int numRows = this->Internals->ListTable->rowCount();
  this->Internals->ListTable->setRowCount(++numRows);

  QTableWidgetItem* item =
    new QTableWidgetItem(QString::fromUtf8(attribute->name().c_str()), smtk_USER_DATA_TYPE);
  Qt::ItemFlags nonEditableFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
  //qDebug() << "Item text" << item->text();

  QVariant vdata;
  vdata.setValue(static_cast<void*>(attribute.get()));
  item->setData(Qt::UserRole, vdata);
  item->setFlags(nonEditableFlags | Qt::ItemIsEditable);
  this->Internals->ListTable->setItem(numRows - 1, name_column, item);

  // If validator provided, use it; otherwise use attribute's isValid method
  bool isValid = m_attributeValidator
    ? m_attributeValidator(attribute)
    : attribute->isValid();
  if (!isValid)
  {
    QIcon alertIcon(ALERT_ICON_PATH);
    QTableWidgetItem* statusItem = new QTableWidgetItem(alertIcon, "", smtk_USER_DATA_TYPE);
    this->Internals->ListTable->setItem(numRows - 1, status_column, statusItem);
  }

  this->Internals->SaveButton->setEnabled(true);
  return item;
}

smtk::attribute::AttributePtr qtAttributeListWidget::getAttributeFromItem(QTableWidgetItem* item)
{
  smtk::attribute::Attribute* rawPtr = item
    ? static_cast<smtk::attribute::Attribute*>(item->data(Qt::UserRole).value<void*>())
    : nullptr;
  return rawPtr ? rawPtr->shared_from_this() : smtk::attribute::AttributePtr();
}

smtk::attribute::AttributePtr qtAttributeListWidget::getSelectedAttribute()
{
  return this->getAttributeFromItem(this->getSelectedItem());
}

QTableWidgetItem* qtAttributeListWidget::getSelectedItem()
{
  auto selectedItems = this->Internals->ListTable->selectedItems();
  int n = selectedItems.count();
  for (int i = 0; i < n; i++)
  {
    if (selectedItems.value(i)->column() == name_column)
    {
      return selectedItems.value(i);
    }
  }
  return nullptr;
}

void qtAttributeListWidget::onAttributeCellChanged(int row, int col)
{
  if (col == name_column)
  {
    QTableWidgetItem* qitem = this->Internals->ListTable->item(row, col);
    this->onAttributeNameChanged(qitem);
  }
}

void qtAttributeListWidget::onAttributeNameChanged(QTableWidgetItem* qitem)
{
  auto aAttribute = this->getAttributeFromItem(qitem);
  if (aAttribute && qitem->text().toStdString() != aAttribute->name())
  {
    auto attResource = aAttribute->definition()->resource();
    // Lets see if the name is in use
    auto att = attResource->findAttribute(qitem->text().toStdString());
    if (att != nullptr)
    {
      std::string s;
      s = "Can't rename " + aAttribute->type() + ":" + aAttribute->name() +
        ".  There already exists an attribute of type: " + att->type() + " named " + att->name() +
        ".";

      QMessageBox::warning(this->parentWidget(), "Attribute Can't be Renamed", s.c_str());
      qitem->setText(aAttribute->name().c_str());
      return;
    }
    attResource->rename(aAttribute, qitem->text().toStdString());
    emit this->attributeNameChanged(aAttribute, std::vector<std::string>());
  }
}

void qtAttributeListWidget::onAttributeSelectionChanged()
{
  auto selectedItem = getSelectedItem();
  auto att = this->getAttributeFromItem(selectedItem);
  this->Internals->CurrentAtt = att;
  emit this->attributeSelectionChanged(att);
}

void qtAttributeListWidget::onCopySelected()
{
  auto selObject = this->getSelectedAttribute();
  if (!selObject)
  {
    return;
  }

  const ResourcePtr attResource = this->Internals->AttDef->resource();
  auto newObject = attResource->copyAttribute(selObject);
  this->Internals->m_utils.configureCopiedAttribute(newObject);
  if (newObject)
  {
    QTableWidgetItem* item = this->addAttributeListItem(newObject);
    if (item)
    {
      this->Internals->ListTable->selectRow(item->row());
    }
    emit this->numOfAttributesChanged();
  }
}

void qtAttributeListWidget::onCreateAttribute()
{
  const ResourcePtr attResource = this->Internals->AttDef->resource();

  smtk::attribute::AttributePtr newAtt = attResource->createAttribute(this->Internals->AttDef);
  assert(newAtt);

  smtk::simulation::truchas::MaterialAttributeUtils utils;
  this->Internals->m_utils.configureNewAttribute(newAtt);

  emit this->attributeCreated(newAtt);
  QTableWidgetItem* item = this->addAttributeListItem(newAtt);
  if (item)
  {
    this->Internals->ListTable->selectRow(item->row());
  }
  emit this->attributeSelectionChanged(newAtt);
  this->Internals->SaveButton->setEnabled(true);
}

void qtAttributeListWidget::onDeleteSelected()
{
  smtk::attribute::AttributePtr selObject = this->getSelectedAttribute();
  if (selObject != nullptr)
  {
    QTableWidgetItem* selItem = this->getSelectedItem();
    int row = selItem->row();
    auto attResource = selObject->attributeResource();
    if (attResource->removeAttribute(selObject))
    {
      emit this->attributeRemoved(selObject);

      this->Internals->ListTable->removeRow(row);
      emit this->numOfAttributesChanged();
      this->Internals->SaveButton->setEnabled(this->Internals->ListTable->rowCount() > 0);
    }
    else
    {
      QString msg;
      QTextStream qs(&msg);
      qs << "Can't remove attribute " << selObject->name().c_str()
         << " - Might be used as a prerequisite!";
      QMessageBox::warning(this->parentWidget(), tr("Failure to Remove Attribute"), msg);
    }
  }
}

void qtAttributeListWidget::onItemChanged(smtk::extension::qtItem* qitem)
{
  // qDebug() << "onItemChanged";
}

#undef ALERT_ICON_PATH
