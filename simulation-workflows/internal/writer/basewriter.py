"""
Abstract base class for Genre and Truchas export writers

"""
import warnings

from .cardformat import CardFormat
from .namelist import Namelist

import smtk
import smtk.attribute
import smtk.model


class BaseWriter:
    """Common base class for export writers"""
# ---------------------------------------------------------------------

    def __init__(self, export_params, mesh_filename, altmesh_filename):
        self.altmesh_file = altmesh_filename
        self.categories = set()
        self.checked_attributes = set()  # attributes that have been validated
        self.material_att_utils = None   # set in materialwriter.py
        self.mesh_file = mesh_filename
        self.model_resource = None
        self.out = None
        self.sim_atts = None
        self.warning_messages = list()

        # For now, some of these member data must be initialized in the subclass

        self.sim_atts = smtk.attribute.Resource.CastTo(
            export_params.find('attributes').value())
        # print('sim_atts', self.sim_atts)
        if self.sim_atts is None:
            msg = 'ERROR - No simulation attributes'
            print(msg)
            raise RuntimeError(msg)

        analysis_att = self.sim_atts.findAttribute('analysis')
        if not analysis_att:
            raise RuntimeError('Internal Error -- missing analysis att')

        self.categories = self.sim_atts.analyses(
        ).getAnalysisAttributeCategories(analysis_att)
        print('Writing categories: {}'.format(self.categories))

        resource = export_params.find('model').value()
        self.model_resource = smtk.model.Resource.CastTo(resource)
        if self.model_resource is None:
            msg = 'ERROR - No model resource'
            print(msg)
            raise RuntimeError(msg)

        CardFormat.ModelResource = self.model_resource

# ---------------------------------------------------------------------
    def write(self, path):
        """[pure virtual] Entry method for writing output"""
        raise RuntimeError('BaseWriter.write() must be overridden by subclass')

# ---------------------------------------------------------------------
    def _check_attribute(self, att):
        """Checks attribute validity"""
        if att in self.checked_attributes:  # avoid redundancy
            return

        self.checked_attributes.add(att)
        if not att.isValid(self.categories):
            msg = 'WARNING: Invalid Attribute \"{}\" (type \"{}\").'.format(
                att.name(), att.type())
            warnings.warn(msg)
            self.warning_messages.append(msg)

# ---------------------------------------------------------------------
    def _find_instanced_attribute(self, att_type):
        """At some point, migrate this logic to CardFormat"""
        att_list = self.sim_atts.findAttributes(att_type)
        if len(att_list) == 1:
            return att_list[0]

        elif not att_list:
            msg = 'Missing instanced attribute type \"{}\"'.format(att_type)
            raise RuntimeError(msg)
        else:
            msg = 'Attribute type \"{}\" not instanced; found {} instead of 1'.format(
                att_type, len(card_att_list))
            raise RuntimeError(msg)

# ---------------------------------------------------------------------
    def _finish_namelist(self):
        self.out.write('/\n')

# ---------------------------------------------------------------------
    def _passes_categories(self, item, categories):
        """[DEPRECATED ] Backward- and list/set-compatible category check

        Use item.categories().passes(categories)

        The isMemberOf() method was deleted early Jan 2020.
        There is also some uncertainty whether the API argument
        should be a list or set.
        """
        if hasattr(item, 'isMemberOf'):
            if isinstance(categories, list):
                return item.isMemberOf(categories)
            # (else)
            return item.isMemberOf(list(categories))
        elif isinstance(categories, set):
            return item.categories().passes(categories)
        else:
            return item.categories().passes(set(categories))

# ---------------------------------------------------------------------
    def _start_namelist(self, namelist_or_title):
        if isinstance(namelist_or_title, Namelist):
            title = namelist_or_title.title
        else:
            title = namelist_or_title
        self.out.write('\n&%s\n' % title)

# ---------------------------------------------------------------------
    def _write_instanced_attribute(self, card):
        """Lookup attribute and write card

        At some point, migrate this logic to CardFormat
        """
        card_att = self._find_instanced_attribute(card.att_type)
        if self._passes_categories(card_att, self.categories):
            self._check_attribute(card_att)
            card.write(self.out, card_att)
        else:
            msg = 'Ignoring attribute type {}. List length is {} instead of 1'.format(
                cart.att_type, len(card_att_list))
            print(msg)
            self.warning_messages.append(msg)

# ---------------------------------------------------------------------
    def _write_separator(self, separator):
        """"""
        self.out.write('\n')
        label = '### {} '.format(separator)
        line = label.ljust(80, '#')
        self.out.write(line)
        self.out.write('\n')
        return None
