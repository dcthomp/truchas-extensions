//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/truchas/qt/qtInductionCoilsModel.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"

#include <QByteArray>
#include <QDebug>
#include <QMimeData>
#include <QModelIndex>
#include <QString>
#include <QTextStream>
#include <QVariant>
#include <QWidget>

#include <cassert>

qtInductionCoilsModel::qtInductionCoilsModel(
  QWidget* parent, smtk::attribute::AttributePtr attribute)
  : QAbstractTableModel(parent)
  , m_attribute(attribute)
  , m_coilsItem(nullptr)
{
  m_coilsItem = attribute->findGroup("coils");
  assert(m_coilsItem);
}

qtInductionCoilsModel::~qtInductionCoilsModel()
{
}

int qtInductionCoilsModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
  {
    return 0;
  }
  return static_cast<int>(m_coilsItem->numberOfGroups());
}

int qtInductionCoilsModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid())
  {
    return 0;
  }
  // NTurns, Radius, Length, Center (3 cols)
  return 6;
}

QVariant qtInductionCoilsModel::data(const QModelIndex& index, int role) const
{
  int row = index.row();
  int col = index.column();
  QVariant qValue; // return value

  assert(index.model() == this);
  if (!index.isValid())
  {
    return qValue;
  }

  // Right-align all data
  if (role == Qt::TextAlignmentRole)
  {
    return QVariant(Qt::AlignRight | Qt::AlignVCenter);
  }

  if (role == Qt::BackgroundRole)
  {
    // If item not enabled, return the default
    if ((this->flags(index) & Qt::ItemIsEnabled) == 0x0)
    {
      return qValue;
    }

    // Check the "set" and "valid" condition for the corresponding ValueItem
    smtk::attribute::ValueItemPtr valItem;
    std::size_t index = -1;
    if (col < 3) // nturns, radius, length
    {
      auto item = m_coilsItem->item(row, col);
      valItem = smtk::dynamic_pointer_cast<smtk::attribute::ValueItem>(item);
      index = 0;
    }
    else // center x, y, z
    {
      valItem = m_coilsItem->findAs<smtk::attribute::ValueItem>(row, "center");
      index = col - 3;
    }
    // Find the "set" condition for the corresponding value item
    if (valItem->isSet(index) && valItem->isValid())
    {
      return qValue;
    }

    // (else) return invalid color
    QColor invalidColor(255, 128, 128);
    return QVariant(QBrush(invalidColor));
  } // if (Qt::BackgroundRole)

  // Skip everything else except display role for now
  if (role != Qt::DisplayRole && role != Qt::EditRole)
  {
    // qWarning() << "Unsupported role:" << role;
    return QVariant();
  }

  // Special case - if disabled, return "n/a"
  if ((this->flags(index) & Qt::ItemIsEnabled) == 0x0)
  {
    return QVariant("n/a");
  }

  // Get DisplayRole reply
  switch (col)
  {
    case 0: // NTurns
    {
      auto valItem = m_coilsItem->findAs<smtk::attribute::IntItem>(row, "nturns");
      if (valItem->isSet())
      {
        auto qString = QString::number(valItem->value());
        qValue.setValue(qString);
      }
    }
    break;

    case 1: // Radius
    {
      auto valItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(row, "radius");
      if (valItem->isSet())
      {
        auto qString = QString::number(valItem->value());
        qValue.setValue(qString);
      }
    }
    break;

    case 2: // Length (only applies for nturns > 1)
    {
      auto valItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(row, "length");
      if (valItem->isSet())
      {
        auto qString = QString::number(valItem->value());
        qValue.setValue(qString);
      }
    }
    break;

    case 3: // x-center
    case 4: // y-center
    case 5: // z-center
    {
      std::size_t index = col - 3;
      auto valItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(row, "center");
      if (valItem->isSet(index))
      {
        auto qString = QString::number(valItem->value(index));
        qValue.setValue(qString);
      }
    }
    break;

    default:
      qWarning() << "Unrecognized column" << col;
      break;
  } // switch

  return qValue;
}

Qt::ItemFlags qtInductionCoilsModel::flags(const QModelIndex& index) const
{
  if (!index.isValid())
  {
    return Qt::ItemIsDropEnabled;
  }

  // One special case: if NTurns is 1, disable length
  if (index.column() == 2)
  {
    int row = index.row();
    auto nturnsItem = m_coilsItem->findAs<smtk::attribute::IntItem>(row, "nturns");
    if (!nturnsItem->isSet() || (nturnsItem->value() < 2))
    {
      return QAbstractTableModel::flags(index) & ~Qt::ItemIsEnabled;
    }
  }

  auto f = Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsDragEnabled |
    Qt::ItemIsDropEnabled;

  return f;
}

QVariant qtInductionCoilsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role != Qt::DisplayRole)
  {
    return Superclass::headerData(section, orientation, role);
  }

  if (orientation == Qt::Vertical)
  {
    QString label;
    QTextStream qs(&label);
    qs << "Coil " << (section + 1);
    return label;
  }

  // (else)
  // Qt::Horizontal
  switch (section)
  {
    case 0:
      return QVariant("# Turns");
      break;
    case 1:
      return QVariant("Radius");
      break;
    case 2:
      return QVariant("Length");
      break;
    case 3:
      return QVariant("X Center");
      break;
    case 4:
      return QVariant("Y Center");
      break;
    case 5:
      return QVariant("Z Center");
      break;

    default:
      qWarning() << __FILE__ << ":" << __LINE__ << "Unrecognized section" << section;
      break;
  }

  return QVariant();
}

bool qtInductionCoilsModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
  if (!index.isValid() || role != Qt::EditRole)
  {
    return false;
  }

  int row = index.row();
  int col = index.column();
  bool modified = false;

  // Unset the item if input is null
  if (value.toString().isEmpty())
  {
    auto item = m_coilsItem->item(row, col);
    auto valItem = smtk::dynamic_pointer_cast<smtk::attribute::ValueItem>(item);
    valItem->unset();
    return true;
  }

  if (0 == col) // nturns
  {
    auto item = m_coilsItem->findAs<smtk::attribute::IntItem>(row, "nturns");
    if (value.isValid())
    {
      modified = item->setValue(value.toInt());
    }
    else
    {
      item->unset();
    }
  }
  else if (1 == col) // radius
  {
    auto item = m_coilsItem->findAs<smtk::attribute::DoubleItem>(row, "radius");
    modified = item->setValue(value.toDouble());
  }
  else if (2 == col) // length
  {
    auto item = m_coilsItem->findAs<smtk::attribute::DoubleItem>(row, "length");
    modified = item->setValue(value.toDouble());
  }
  else
  { // center
    auto item = m_coilsItem->findAs<smtk::attribute::DoubleItem>(row, "center");
    int i = col - 3;
    modified = item->setValue(i, value.toDouble());
  }

  if (true)
  {
    emit this->dataChanged(index, index);
  }
  return true;
}

bool qtInductionCoilsModel::dropMimeData(const QMimeData* mimeData, Qt::DropAction action,
  int dropRow, int dropColumn, const QModelIndex& parent)
{
  if (parent.isValid())
  {
    dropRow = parent.row();
  }

  // Parse mime data to obtain the row that was dragged.
  QByteArray encoded = mimeData->data("application/x-qabstractitemmodeldatalist");
  QDataStream stream(&encoded, QIODevice::ReadOnly);
  QMap<int, QVariant> roleDataMap;
  int dragRow;
  stream >> dragRow;

  // Emit signal so that smtkTruchasCoilsView can update this model
  // and the source model concurrently.
  emit this->requestRotate(dragRow, dropRow);
  return true;
}

void qtInductionCoilsModel::beginAddCoil()
{
  // External logic will be adding one row to my model
  int nrows = this->rowCount();
  this->beginInsertRows(QModelIndex(), nrows, nrows);
}

void qtInductionCoilsModel::endAddCoil()
{
  this->endInsertRows();
}

void qtInductionCoilsModel::beginRemoveCoil(int row)
{
  // External logic will be removing one row to my model
  this->beginRemoveRows(QModelIndex(), row, row);
}

void qtInductionCoilsModel::endRemoveCoil()
{
  this->endRemoveRows();
}

bool qtInductionCoilsModel::beginRotateCoil(int fromRow, int toRow)
{
  // External logic will be rotating items in m_coilsGroup
  QModelIndex index;
  return this->beginMoveRows(index, fromRow, fromRow, index, toRow);
}

void qtInductionCoilsModel::endRotateCoil()
{
  this->endMoveRows();
}

void qtInductionCoilsModel::beginAddTime()
{
  // Coils model not affected by this change
}

void qtInductionCoilsModel::endAddTime()
{
  // Coils model not affected by this change
}

void qtInductionCoilsModel::beginRemoveTime(int row)
{
  (void)row;
  // Coils model not affected by this change
}

void qtInductionCoilsModel::endRemoveTime()
{
  // Coils model not affected by this change
}
