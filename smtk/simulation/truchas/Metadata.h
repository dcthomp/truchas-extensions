//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_truchas_Metadata_h
#define __smtk_simulation_truchas_Metadata_h

#include "smtk/simulation/truchas/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/resource/PropertyType.h"

#include "nlohmann/json.hpp"

#include <string>

namespace
{
const smtk::resource::Integer METADATA_FORMAT_NUMBER = 1;
}

namespace smtk
{
namespace simulation
{
namespace truchas
{

/**\brief Metadata specific to truchas projects.
 *
 * Includes both static constants used to across the code base and
 * instance data that are stored in the resource properties on the
 * project instance.
 */

struct SMTKTRUCHAS_EXPORT Metadata
{
  // Static constants - initialized in the cxx file
  static const std::string PROJECT_TYPENAME;            // "truchas"
  static const std::string PROJECT_FILE_EXTENSION;      // ".project.smtk";
  static const std::string PROPERTY_PREFIX;             // "truchas.";
  static const std::string HEAT_TRANSFER_MESH_ROLE;     // "heat-transfer-mesh"
  static const std::string INDUCTION_HEATING_MESH_ROLE; // "induction-heating-mesh"

  // Directory containing Truchas.sbt and Truchas.py
  // Applications must set this!
  static std::string WORKFLOWS_DIRECTORY;

  // Interim methods to add and retrieve instance data from project
  //   getFromResource() returns boolean indicating whether or not the resource
  //   is formatted with Truchas metadata.
  bool getFromResource(smtk::resource::ConstResourcePtr resource);
  void putToResource(smtk::resource::ResourcePtr resource);

  // Instance data - stored in the project resource properties
  smtk::resource::Integer Format = METADATA_FORMAT_NUMBER;
  smtk::resource::String NativeHeatTransferMeshLocation;
  smtk::resource::String NativeInductionHeatingMeshLocation;
};

SMTKTRUCHAS_EXPORT void to_json(nlohmann::json& j, const Metadata& mdata);
SMTKTRUCHAS_EXPORT void from_json(const nlohmann::json& j, Metadata& mdata);
}
}
}

#endif
