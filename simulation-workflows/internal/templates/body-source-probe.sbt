<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="body" Label="Body" Unique="true" Version="2">
      <AssociationsDef Name="BodyAssociations" NumberOfRequiredValues="1" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Component Name="material" Label="Material" NumberOfRequiredValues="1" EnforceCategories="true">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material.real']"></Resource>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material.void']"></Resource>
          </Accepts>
          <ChildrenDefinitions>
            <Double Name="temperature" Label="Initial Temperature" NumberOfRequiredValues="1">
              <BriefDescription>Initial temperature of the material body</BriefDescription>
              <ExpressionType>fn.initial-condition.temperature</ExpressionType>
            </Double>
            <Double Name="velocity" Label="Initial Velocity" NumberOfRequiredValues="3" OkToInheritCategories="false" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>Initial velocity of the material body</BriefDescription>
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <ConditionalInfo>
            <Condition Resource="smtk::attribute::Resource" Component="attribute[type='material.real']">
              <Items>
                <Item>temperature</Item>
                <Item>velocity</Item>
              </Items>
            </Condition>
          </ConditionalInfo>
        </Component>
      </ItemDefinitions>
    </AttDef>
    <!-- DS_SOURCE-->
    <AttDef Type="ht.source" Label="Volumetric Heat Source" BaseType="" Version="0">
      <AssociationsDef Name="DSSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="source" Label="Temperature">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- PROBE-->
    <AttDef Type="probe" Label="Probe" BaseType="" Version="0">
      <ItemDefinitions>
        <Double Name="coord" Label="Location" NumberOfRequiredValues="3">
          <BriefDescription>Coordinates of the probe location</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x=</Label>
            <Label>y=</Label>
            <Label>z=</Label>
          </ComponentLabels>
        </Double>
        <Double Name="coord-scale-factor" Label="Coordinate scale factor">
          <BriefDescription>Multiplicative scaling factor applied to the coordinates</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <String Name="data" Label="Simulation data">
          <BriefDescription>Simulation data to record at the probe location</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value>temperature</Value>
            <Structure>
              <Value>velocity</Value>
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
            </Structure>
            <Structure>
              <Value>pressure</Value>
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
            </Structure>
          </DiscreteInfo>
        </String>
        <String Name="data-file" Label="Filename">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <BriefDescription>Each probe must write to a different file</BriefDescription>
        </String>
        <String Name="description" Label="Description" Optional="true">
          <BriefDescription>Arbitrary comment string to write to output file</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
        </String>
        <Int Name="digits" Label="Output digits">
          <BriefDescription>Number of significant digits in output data</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">2</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
