//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtAttributeListWidget - the Attribute list widget
// .SECTION Description
// .SECTION See Also
// qtSection

#ifndef __smtk_simulation_truchas_qt_qtAttributeListWidget_h
#define __smtk_simulation_truchas_qt_qtAttributeListWidget_h

#include "smtk/simulation/truchas/qt/qtTypeDeclarations.h"

#include "smtk/simulation/truchas/qt/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include <QWidget>

#include <functional>
#include <string>
#include <vector>

class QTableWidgetItem;
class qtAttributeListWidgetInternals;

namespace smtk
{
namespace extension
{
class qtBaseView;
class qtItem;
class qtUIManager;
}
}

/* **
  Displays a list of attributes of a specified type, with buttons
  to create, copy, and delete instances. This code is adapted from
  the "TopFrame" in qtAttributeView, however, it does not fully implement
  the functionality there. Known differences are:
    * This class *only* supports a single concrete definition. In other words,
      it won't display a combobox for selected which attribute type to create.
    * This class does *not* support "property" display mode (which is
      deprecated).
    * This class adds supports for an optional validator function, which is
      needed for Truchas materials (to check monotonicity of transition
      temperatures).
 */
class SMTKTRUCHASQTEXT_EXPORT qtAttributeListWidget : public QWidget
{
  Q_OBJECT

public:
  typedef std::function<bool(smtk::attribute::AttributePtr)> AttributeValidator;

  qtAttributeListWidget(QWidget* parent, smtk::extension::qtBaseView* view,
    smtk::attribute::DefinitionPtr attDefinition, AttributeValidator fn = nullptr);
  virtual ~qtAttributeListWidget();
  void checkStatus(smtk::attribute::AttributePtr att);

  // Rebuild list - should be called after category changes
  void onShowCategory();

  // Set visibility of load/save buttons
  void showLoadSaveButtons(bool visible);

public slots:

signals:
  void attributeCreated(const smtk::attribute::AttributePtr);
  void attributeRemoved(const smtk::attribute::AttributePtr);
  void attributeNameChanged(const smtk::attribute::AttributePtr, std::vector<std::string> notUsed);
  void attributeSelectionChanged(const smtk::attribute::AttributePtr);
  void numOfAttributesChanged();
  void loadButtonClicked();
  void saveButtonClicked();

protected slots:
  void onAttributeCellChanged(int row, int col);
  void onAttributeNameChanged(QTableWidgetItem*);
  void onAttributeSelectionChanged();
  void onCopySelected();
  void onCreateAttribute();
  void onDeleteSelected();
  void onItemChanged(smtk::extension::qtItem*);

protected:
  virtual void initWidget();
  QTableWidgetItem* addAttributeListItem(smtk::attribute::AttributePtr attribute);
  smtk::attribute::AttributePtr getAttributeFromItem(QTableWidgetItem* item);
  smtk::attribute::AttributePtr getSelectedAttribute();
  QTableWidgetItem* getSelectedItem();

  AttributeValidator m_attributeValidator;

private:
  qtAttributeListWidgetInternals* Internals;

}; // class

#endif
