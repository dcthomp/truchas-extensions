import argparse
import glob
import json
import os
import shutil
import sys


def check_file(baseline_filepath, test_filepath):
    """"""
    assert os.path.exists(
        baseline_filepath), 'baseline file not found: {}'.format(baseline_filepath)
    assert os.path.exists(
        test_filepath), 'test file not found: {}'.format(test_filepath)
    with open(baseline_filepath) as bf:
        baseline_lines = bf.readlines()
    with open(test_filepath) as tf:
        test_lines = tf.readlines()
    test_filename = os.path.basename(test_filepath)

    import difflib
    diff = difflib.unified_diff(
        baseline_lines, test_lines, fromfile='baseline', tofile=test_filename, n=0)
    delta = [l for l in diff]
    if not delta:
        print('Output File "{}" MATCHES Baseline'.format(test_filename))
    else:
        print('Output file does not match baseline')
        print('Baseline file', baseline_filepath)
        print('Output file:', test_filepath)
        sys.stderr.write('========== diff baseline output ==========\n')
        sys.stderr.writelines(delta)
        sys.stderr.write('==========================================\n')
        raise RuntimeError('Test file does NOT match baseline')


def write_legacy_projectfile(args, simulation_code, att_resource, model_resource):
    """Writes legacy .smtkproject file for debug/test."""
    project_dict = dict(
        fileVersion=1,
        projectDirectory=args.output_folder,
        projectName=os.path.basename(args.output_folder),
        resources=[],
        simulationCode=simulation_code
    )

    att_path = att_resource.location()
    att_resource_dict = dict(
        filename=os.path.basename(att_path),
        identifier='default',
        importLocation=args.template_filepath,
        typeName=att_resource.typeName(),
        uuid=str(att_resource.id())
    )
    project_dict['resources'].append(att_resource_dict)

    if model_resource is not None:
        model_path = model_resource.location()
        model_resource_dict = dict(
            filename=os.path.basename(model_path),
            identifier='default',
            importLocation=args.model_filepath,
            typeName=model_resource.typeName(),
            uuid=str(model_resource.id())
        )
    project_dict['resources'].append(model_resource_dict)

    project_filepath = os.path.join(args.output_folder, '.smtkproject')
    with open(project_filepath, 'w') as proj_fp:
        json.dump(project_dict, proj_fp, indent=2)
        proj_fp.write('\n')
        print(f'Wrote {project_filepath}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate attribute resource from yml description')
    # Required arguments
    parser.add_argument('template_filepath',
                        help='Attribute template filename/path (.sbt)')
    parser.add_argument(
        'model_filepath', help='path to model file (.gen, .ncdf)')
    parser.add_argument('yml_filepaths', nargs='+',
                        help='YAML file(s) specifying the attributes to generate')
    # Optional arguments
    parser.add_argument('-i', '--induction_heating_model',
                        help='path to induction heating model file (.gen, .ncdf')
    parser.add_argument('-o', '--output_folder',
                        help='output folder for writing resource files')
    parser.add_argument('-s', '--smtktools_path',
                        help='path to smtk_tools module')
    # Todo path to validation spec file
    # parser.add_argument('-s', '--skip_instances', action='store_true', help='skip initializing instanced attributes')

    args = parser.parse_args()
    # print(args)

    # Check filesystem paths
    if not os.path.exists(args.template_filepath):
        raise RuntimeError(
            'template file not found: {}'.format(args.template_filepath))
    if not os.path.exists(args.model_filepath):
        raise RuntimeError(
            'model file not found: {}'.format(args.model_filepath))
    yml_filepath = args.yml_filepaths[0]
    if not os.path.exists(yml_filepath):
        raise RuntimeError('yml spec not found: {}'.format(yml_filepath))
    if args.smtktools_path is not None and not os.path.exists(args.smtktools_path):
        raise RuntimeError(
            'smtk_tools directory not found: {}'.format(args.smtktools_path))
    if args.output_folder is not None:
        if os.path.exists(args.output_folder):
            print('Deleting existing output folder', args.output_folder)
            shutil.rmtree(args.output_folder)
        os.makedirs(args.output_folder)
    if args.induction_heating_model is not None and not os.path.exists(args.induction_heating_model):
        raise RuntimeError(
            'induction-heating model not found: {}'.format(args.induction_heating_model))

    # Put smtk imports here
    import smtk
    import smtksimulationtruchas

    # Add path to smtk_tools
    tools_path = args.smtktools_path
    if tools_path is None:
        # Default path from the repository
        source_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(source_path, 'smtk-tools')
        tools_path = os.path.normpath(path)

    assert os.path.exists(
        tools_path), 'smtk-tools path not found at {}'.format(tools_path)
    sys.path.insert(0, tools_path)

    import smtk_tools
    from smtk_tools.resource_io import ResourceIO
    from smtk_tools.attribute_builder import AttributeBuilder

    # Load yml file as the attribute specification
    import yaml
    spec = None
    print('Loading yaml file:', yml_filepath)
    with open(yml_filepath) as fp:
        content = fp.read()
        spec = yaml.safe_load(content)
    assert spec is not None

    # Initialize ResourceIO and load resources
    model_resource = None
    res_io = ResourceIO()
    if args.model_filepath:
        # Support model or resource input
        basename, ext = os.path.splitext(args.model_filepath)
        if ext == '.smtk':
            print('Loading model resource:', args.model_filepath)
            model_resource = res_io.read_resource(args.model_filepath)
        else:
            print('Importing model file:', args.model_filepath)
            model_resource = res_io.import_resource(args.model_filepath)
        assert model_resource is not None, 'failed to load model file {}'.format(
            args.model_filepath)

        # Update model entity names with prefix including pedigree id
        model_utils = smtk.simulation.truchas.ModelUtils()
        model_utils.renameModelEntities(model_resource)

    print('Importing attribute template', args.template_filepath)
    att_resource = res_io.import_resource(args.template_filepath)
    assert att_resource is not None, 'failed to import attribute template from {}'.format(
        args.template_filepath)

    # Associate the model resource
    if model_resource is not None:
        att_resource.associate(model_resource)

    # Initialize builder and populate the attributes
    builder = AttributeBuilder()
    builder.build_attributes(att_resource, spec, model_resource=model_resource)

    # Load any additional yml input files.
    # Note that yml_filepath is updated to the last spec file.
    if len(args.yml_filepaths) > 1:
        more_yml = args.yml_filepaths[1:]
        for yml_filepath in more_yml:
            print('Loading yaml file:', yml_filepath)
            with open(yml_filepath) as fp:
                content = fp.read()
                spec = yaml.safe_load(content)
            assert spec is not None
            builder.process_spec(spec)

    # Write the resource files
    if args.output_folder:
        att_path = os.path.join(args.output_folder, 'attributes.smtk')
        res_io.write_resource(att_resource, att_path)
        print('Wrote', att_path)

        # Only write model resource if it was imported
        basename, ext = os.path.splitext(args.model_filepath)
        if ext != '.smtk':
            model_path = os.path.join(args.output_folder, 'model.smtk')
            res_io.write_resource(model_resource, model_path)
            print('Wrote', model_path)

        # Write legacy project file for debug/test
        write_legacy_projectfile(args, 'truchas', att_resource, model_resource)

        # Export the Truchas input file
        basepath, ext = os.path.splitext(args.template_filepath)
        op_path = '{}.py'.format(basepath)
        if not os.path.exists(op_path):
            print('Export operation not found at {}'.format(op_path))
        export_op = res_io.import_python_operation(op_path)

        params_att = export_op.parameters()
        test_mode_item = params_att.findVoid('test-mode')
        if test_mode_item is not None:
            test_mode_item.setIsEnabled(True)

        # Configure analysis (Genre or Truchas)
        analysis_att = att_resource.findAttribute('analysis')
        analysis_item = analysis_att.findString('Analysis')
        analysis_name = analysis_item.value()
        # print(Analysis name', analysis_name)

        params_att.findString('analysis-code').setValue(analysis_name)
        params_att.findResource('attributes').setValue(att_resource)
        params_att.findResource('model').setValue(model_resource)
        params_att.findFile('mesh-file').setValue(args.model_filepath)
        if args.induction_heating_model is not None:
            ih_item = params_att.findFile('alt-mesh-file')
            ih_item.setIsEnabled(True)
            ih_item.setValue(args.induction_heating_model)

        # Use the yml file base as the output file prefix
        yml_filename = os.path.basename(yml_filepath)
        yml_prefix, ext = os.path.splitext(yml_filename)
        export_folder = os.path.join(args.output_folder, 'export')
        if analysis_name == 'Truchas':
            output_filename = '{}.inp'.format(yml_prefix)
            output_filepath = os.path.join(export_folder, output_filename)
            params_att.findFile('output-file').setValue(output_filepath)
        elif analysis_name == 'Genre':
            params_att.findDirectory('output-folder').setValue(export_folder)
        else:
            raise RuntimeError(
                'Unrecognized analysis name {}'.format(analysis_name))

        result = export_op.operate()
        outcome = result.findInt('outcome')
        OPERATION_SUCCEEDED = 3  # defined in smtk/operation/Operation.h
        assert outcome.value() == OPERATION_SUCCEEDED, \
            'export operation failed, outcome: {}'.format(outcome.value())

        # Check that output filesystem items were created
        assert os.path.exists(
            export_folder), 'ouptput folder not found at {}'.format(export_folder)
        if analysis_name == 'Truchas':
            assert os.path.exists(
                output_filepath), 'Output file not found at {}'.format(output_filepath)

        # Dump out any warnings
        num_records = export_op.log().numberOfRecords()
        if num_records:
            print('EXPORT LOG RECORDS ({}):'.format(num_records))
            print(export_op.log().convertToString())
        assert not export_op.log().hasErrors(), 'export log has errors'

        # Check for baseline file(s) to compare
        # Must be in "baseline" folder beneath yml file
        yml_path = os.path.dirname(yml_filepath)
        if analysis_name == 'Truchas':
            # Truchas baseline is single file
            baseline_filepath = os.path.join(
                yml_path, 'baseline', output_filename)
            if os.path.exists(baseline_filepath):
                check_file(baseline_filepath, output_filepath)
        elif analysis_name == 'Genre':
            # Genre baseline is folder with 1 or more files
            baseline_path = os.path.join(yml_path, 'baseline', yml_prefix)
            if os.path.exists(baseline_path) and os.path.isdir(baseline_path):
                pattern = '{}/*.inp'.format(baseline_path)
                for baseline_filepath in glob.iglob(pattern):
                    # print('baseline file:', baseline_filepath)
                    basename = os.path.basename(baseline_filepath)
                    output_filepath = os.path.join(export_folder, basename)
                    check_file(baseline_filepath, output_filepath)
    # finis
